<div class="row xloading">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
		  		<div class="row justify-content-md-center">
				    <div class="col col-5">
                <h4 class="card-title">
                	<?php
                	if($tipe=='tambah')
                		echo 'Tambah Data Evaluasi';
                	else if($tipe=='edit')
                		echo 'Edit Data Evaluasi';
                	?>
                </h4>
                <p class="card-description">
                  Yang bertanda (*) harus diisi!
                </p>
                <?php
                	if($tipe=='tambah')
                		$act = base_url('dosen/evaluasi/simpan');
                	else if($tipe=='edit')
                		$act = base_url('dosen/evaluasi/update/'.$kode);
                ?>

                <form class="forms-sample" action="<?php echo $act; ?>" method="post">

                  <div class="form-group">
                    <label>* Tanggal <?php echo form_error('tgl','<small class="text-danger">','</small>'); ?></label>
                    <input autocomplete="off"  type="text" class="form-control w-50" value="<?php echo $tgl; ?>"  placeholder="Tanggal Kegiatan" name="tgl">
                  </div>

                  <div class="form-group">
                    <label>* Tanggapan Masuarakat <?php echo form_error('tanggapan','<small class="text-danger">','</small>'); ?></label>
                    <textarea name="tanggapan" class="form-control"  placeholder="Tanggapan"><?php echo $tanggapan; ?></textarea>
                  </div>

                  <div class="form-group">
                    <label>* Hasil Survei Mahasiswa <?php echo form_error('survei','<small class="text-danger">','</small>'); ?></label>
                    <textarea name="survei" class="form-control"  placeholder="Survei"><?php echo $survei; ?></textarea>
                  </div>

                  <div class="form-group">
                    <label>* Kendala Yang Dihadapi Mahasiswa <?php echo form_error('kendala','<small class="text-danger">','</small>'); ?></label>
                    <textarea name="kendala" class="form-control"  placeholder="Kendala"><?php echo $kendala; ?></textarea>
                  </div>

                  <div class="form-group">
                    <label>* Saran <?php echo form_error('saran','<small class="text-danger">','</small>'); ?></label>
                    <textarea name="saran" class="form-control"  placeholder="Saran"><?php echo $saran; ?></textarea>
                  </div>

                  <button type="submit" class="btn btn-success mr-2">Simpan</button>
                  <a class="btn btn-light" href="<?php echo $kembali; ?>">Kembali</a>
                </form>
				    </div>
				  </div>
		  	</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function() {
    $('input[name="tgl"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
    		autoApply: true,
    });
});
</script>