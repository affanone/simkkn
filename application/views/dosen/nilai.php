<div class="card">
  <div class="card-body">
    <h4 class="card-title">Data Nilai Mahasiswa KKN</h4>
    <div class="row mb-3">
	    <div class="col offset-8 col-4">
	    	<a class="float-right btn btn-outline-primary" onclick="_cetak(this)" href="<?php echo base_url('dosen/nilai/cetak'); ?>"><i class="fa fa-print"></i>Cetak</a>
	    </div>
	  </div>
	  <div class="alert alert-info" role="alert">
		  <p>
		  	Pengisian nilai berdasarkan nilai angka, akan tetapi yang akan diterima oleh peserta berdasarkan nilai huruf. Berikut merupakan konversi nilai angka ke nilai huruf<br>
		  	<ul>
		  		<li>"E" jikan nilai = 0</li>
		  		<li>"D" jikan nilai < 56</li>
		  		<li>"C" jikan nilai < 63</li>
		  		<li>"BC" jikan nilai < 70</li>
		  		<li>"B" jikan nilai < 77</li>
		  		<li>"AB" jikan nilai < 85</li>
		  		<li>"A" jikan nilai > 84</li>
		  	</ul>
		  </p>
		  <p>
		  	<strong><h4>CATATAN :</h4></strong>
		  	- Pengisian nilai dapat dilakukan sebelum pengarsipan kegiatan KKN dilakukan oleh LPPM.<br>
		  	- Sertifikat peserta KKN diperoleh apabila nilai peserta <strong>"A"</strong>, <strong>"B"</strong> dan <strong>"C"</strong>
		  </p>
		</div>
    <table class="table datatable table-bordered table-striped">
    	<thead>
    		<tr>
    			<th>#</th>
    			<th>NPM</th>
    			<th>NAMA</th>
    			<th>FAK / PRODI</th>
    			<th>NILAI</th>
    			<th>AKSI</th>
    		</tr>
    	</thead>
    	<tbody>
    		<?php
    		$i = 1;
    		foreach ($data as $key => $value) {
    		?>
    			<tr>
	    			<td><?php echo $i++; ?></td>
	    			<td><?php echo $value->NPM; ?></td>
	    			<td><?php echo $value->NAMAMHS; ?></td>
	    			<td><?php echo $value->FAKPRODI.' / '.$value->NAMAPRODI; ?></td>
	    			<td id="nilai<?php echo $value->NPM; ?>"><?php echo $value->NILAIMHS; ?></td>
	    			<td>
	    				<?php if($value->STATUSMHS==1){ ?>
	    					<button type="button" onclick="setNilai(this)" data-npm="<?php echo $value->NPM; ?>" data-nilai="<?php echo $value->NILAIMHS; ?>" data-toggle="modal" data-target="#nilaiModal" class="btn btn-outline-primary btn-sm">Nilai</button>
	    				<?php }else{ ?>
	    					<button type="button" class="btn disabled btn-outline-secondary btn-sm">Nilai</button>
	    				<?php } ?>
	    			</td>
	    		</tr>
    		<?php
    		}
    		?>
    	</tbody>
    </table>
  </div>
</div>


<div class="modal fade" tabindex="-1" id="nilaiModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Set Nilai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center xloading45">
        <p>
        	<div class="form-group">
			    <label for="nilai">Nilai</label>
			    <input autocomplete="off"  type="hidden" name="npm" value="">
			    <input autocomplete="off"  name="nilai" type="text" class="number w-25 m-auto text-center form-control" id="nilai" placeholder="Masukkan nilai">
			    <small class="form-text text-muted">Beri nilai dari range 0 - 100</small>
			  </div>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="simpanNilai()" class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.datatable').DataTable({
			info:false,
			paging:false,
			scrollX:true
		})
	});
	var _this = null;
	function setNilai(t){
		_this = t;
		var nilai = $(t).attr('data-nilai');
		var npm = $(t).attr('data-npm');
		_setValue('nilai',nilai);
		_setValue('npm',npm);
	}
	function simpanNilai(){
		var val = $('[name="nilai"]').val();
		var nilai = parseInt(val);
		var npm = $('[name="npm"]').val();
		if(val==''){
			_alert('Nilai Harus di isi');
		}else if(nilai<0 || nilai>100){
			_alert('Nilai Harus diatas sama dengan 0 dan dibawah sama dengan 100');
		}
		else{
			_ajax({
				url:'<?php echo base_url('dosen/nilai/set') ?>',
				data:{
					npm:npm,
					nilai:nilai
				},
				loading:'.xloading45',
				success:function(){
					$('#nilaiModal').modal('toggle');
					$(_this).attr('data-nilai',nilai);
					$('#nilai'+npm).text(nilai);
				}
			})
		}
	}
</script>