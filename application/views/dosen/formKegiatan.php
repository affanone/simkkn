<div class="row xloading">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
		  		<div class="row justify-content-md-center">
				    <div class="col col-5">
                <h4 class="card-title">
                	<?php
                	if($tipe=='tambah')
                		echo 'Tambah Rencana Kegiatan';
                	else if($tipe=='edit')
                		echo 'Edit Rencana Kegiatan';
                	?>
                </h4>
                <p class="card-description">
                  Yang bertanda (*) harus diisi!
                </p>
                <?php
                	if($tipe=='tambah')
                		$act = base_url('dosen/kegiatan/simpan');
                	else if($tipe=='edit')
                		$act = base_url('dosen/kegiatan/update/'.$kode);
                ?>

                <form class="forms-sample" action="<?php echo $act; ?>" method="post">

                  <div class="form-group">
                    <label>* Tanggal <?php echo form_error('tgl','<small class="text-danger">','</small>'); ?></label>
                    <input autocomplete="off"  type="text" class="form-control w-50" value="<?php echo $tgl; ?>"  placeholder="Tanggal Kegiatan" name="tgl">
                  </div>

                  <div class="form-group">
                    <label>* Judul Kegiatan <?php echo form_error('judul','<small class="text-danger">','</small>'); ?></label>
                    <input autocomplete="off"  type="text" class="form-control" value="<?php echo $judul; ?>"  placeholder="Judul Kegiatan" name="judul">
                  </div>

                  <div class="form-group">
                    <label>* Tempat Kegiatan <?php echo form_error('tempat','<small class="text-danger">','</small>'); ?></label>
                    <input autocomplete="off"  type="text" class="form-control" value="<?php echo $tempat; ?>"  placeholder="Tempat Kegiatan" name="tempat">
                  </div>

                  <div class="form-group">
                    <label>* Anggaran (Rp) <?php echo form_error('anggaran','<small class="text-danger">','</small>'); ?></label>
                    <input autocomplete="off"  type="text" class="form-control currency" value="<?php echo $anggaran; ?>"  placeholder="Anggaran" name="anggaran">
                  </div>

                  <div class="form-group">
                    <label>* Pelaksana <?php echo form_error('pelaksana','<small class="text-danger">','</small>'); ?></label>
                    <input autocomplete="off"  type="text" class="form-control" value="<?php echo $pelaksana; ?>"  placeholder="Pelaksana" name="pelaksana">
                  </div>

                  <div class="form-group">
                    <label for="dpl">* Penanggung Jawab <?php echo form_error('tgjawab','<small class="text-danger">','</small>'); ?></label>

                    <select class="form-control selectpicker" data-live-search="true" id="tgjawab" name="tgjawab">
                      <option disabled="" <?php echo ($tgjawab=='') ? 'selected="':""; ?> value=""> - Penanggung Jawab -</option>
                      <?php

                      foreach ($mhs as $key => $value) {
                        if($tgjawab==$value->NPM){
                          echo '<option data-tokens="'.$value->NAMAMHS.'" selected value="'.$value->NPM.'">('.$value->NPM.') '.$value->NAMAMHS.'</option>';
                        }else{
                          echo '<option data-tokens="'.$value->NAMAMHS.'" value="'.$value->NPM.'">('.$value->NPM.') '.$value->NAMAMHS.'</option>';
                        }
                      }
                      ?>
                    </select>
                  </div>

                  <button type="submit" class="btn btn-success mr-2">Simpan</button>
                  <a class="btn btn-light" href="<?php echo $kembali; ?>">Kembali</a>
                </form>
				    </div>
				  </div>
		  	</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function() {
    $('input[name="tgl"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
    		autoApply: true,
    });
});
</script>