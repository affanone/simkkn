<div class="card">
  <div class="card-header text-right">
    <a href="<?php echo $this->mhistory->back(); ?>" class="btn btn-outline-warning">Kembali</a>
  </div>
  <div class="card-body">
    <h4 class="card-title">Biodata Mahasiswa</h4>
  	<div class="row">
  		<div class="col-md-4 offset-md-8">
  			<img src="<?php echo $foto.'?'.rand(111,999); ?>" alt="NO FOTO" class="img-thumbnail w-100">
  		</div>
  	</div>
    <table class="mt-3 table-striped table" style="font-size: 14px;">
			  <tbody>
			  	<tr>
			  		<td width="3">1.</td>
			  		<td width="350">Nama Mahasiswa</td>
			  		<td width="1">:</td>
			  		<td><?php echo strtoupper($nama); ?></td>
			  	</tr>
			  	<tr>
			  		<td>2.</td>
			  		<td>Nomor Pokok Mahasiswa</td>
			  		<td>:</td>
			  		<td><?php echo $npm; ?></td>
			  	</tr>
			  	<tr>
			  		<td>3.</td>
			  		<td>Fakultas / Prodi</td>
			  		<td>:</td>
			  		<td><?php echo strtoupper($prodi); ?></td>
			  	</tr>
			  	<tr>
			  		<td>4.</td>
			  		<td>Nomor Kontak</td>
			  		<td>:</td>
			  		<td><?php echo $telp; ?></td>
			  	</tr>
			  	<tr>
			  		<td>5.</td>
			  		<td>Pekerjaan</td>
			  		<td>:</td>
			  		<td><?php echo ($kerja=='')?'-':$kerja; ?></td>
			  	</tr>
			  	<tr>
			  		<td>6.</td>
			  		<td>Alamat</td>
			  		<td>:</td>
			  		<td><?php echo $alamat; ?></td>
			  	</tr>
			  	<tr>
			  		<td>7.</td>
			  		<td>Jumlah Kumulatif sks yang diperoleh</td>
			  		<td>:</td>
			  		<td><?php echo $sks; ?></td>
			  	</tr>
			  	<tr>
			  		<td>8.</td>
			  		<td>Ukuran Seragam</td>
			  		<td>:</td>
			  		<td><?php echo strtoupper($seragam); ?></td>
			  	</tr>
			  </tbody>
			</table>
  </div>
</div>