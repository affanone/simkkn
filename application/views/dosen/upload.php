<style type="text/css">
    .body-tab{
        display: none;
    }
</style>
<div class="card">
  <div class="card-body">
    <h4 class="card-title">Data Laporan</h4>


    <ul class="nav nav-tabs card-header-tabs">
      <li class="nav-item">
        <a class="nav-link active tab-menu" href="#laporan-mhs">Data Laporan Peserta <span class="badge badge-pill badge-primary"><?php echo count($data); ?></span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link tab-menu" href="#lembar-monitoring">Upload Lembar Monitoring <span class="badge badge-pill badge-primary"><?php echo count($monitoring); ?></a>
      </li>
    </ul>

    <div class="box-tabs mt-5">
        <div class="body-tab" id="laporan-mhs">
            <div class="alert alert-info" role="alert">
              <b>PENTING!</b> Sebelum diserahkan ke LPPM silahkan dievaluasi terlebih dahulu file yang telah diupload oleh peserta, karena semua file sewaktu-waktu akan dihapus secara otomatis <b>kecuali file yang telah di serahkan ke LPPM yang tidak akan dihapus!</b>
            </div>
            <div class="row" style="overflow:  auto;">
                <div class="col-12">  
                    <table class="table datatable table-hover">
                        <thead>
                            <tr class="text-muted">
                                <th></th>
                                <th>File</th>
                                <th>Upload User</th>
                                <th>Size</th>
                                <th>Date</th>
                                <th>Jenis</th>
                                <th>Deskripsi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        <?php
                        foreach ($data as $key => $value){ 
                            $ext = explode('.', $value->FILEUPL);
                            $ext = end($ext);
                            //if($ext=='pdf'){
                                switch ($value->AJUKANUPL) {
                                    case 0:
                                    case 2:
                                        $del = '<a href="'.base_url('dosen/upload/ajukan/').$value->KDUPL.'" onclick="return ajukan(this);" class="btn-link text-primary">Serahkan ke LPPM</a>';
                                        break;
                                    case 1:
                                        $del = '<a href="'.base_url('dosen/upload/batalajukan/').$value->KDUPL.'" onclick="return ajukan(this);" class="btn-link text-danger">Batalkan penyerahan</a>';
                                    break;
                                    case 3:
                                        if($value->MSGUPL==null)
                                            $msg = 'Mungkin laporan kurang lengkap, silahkan diperbaiki atau langsung mendatangi LPPM';
                                        else
                                            $msg = $value->MSGUPL;
                                        $del = '<a href="#!"  onclick="again(\''.$value->KDUPL.'\',\''.$msg.'\');" class="btn-link text-danger">Ditolak LPPM</a>';
                                    break;
                                    case 4:
                                        $del = '<span class="badge badge-pill badge-success"><i>Diterima LPPM</i></span>';
                                    break;                  
                                    default:
                                        # code...
                                        break;
                                }
                                
                            // }
                            // else
                            //  $del = '';
                                echo '<tr><td width="60px"><img src="'.base_url('assets/images/icons/'.$ext.'.png').'" class="img-fluid rounded"></td>
                                    <td><a '.(($ext=='pdf')? 'onclick="_cetak(this)"' : '' ).' href="'.base_url('dosen/upload/view/').$value->KDUPL.'" class="btn-link" title="'.$value->FILEUPL.'">'.$this->mfungsi->minimalText($value->FILEUPL,15).'</a>&nbsp;.&nbsp;<a href="'.base_url('dosen/upload/download/').$value->KDUPL.'">Download</a></td>
                                    <td><span data-toggle="tooltip" data-placement="bottom" title="'.$value->NPM.'">'.$value->NAMAMHS.'</span></td>
                                    <td>'.$this->mfungsi->koversiSize($value->SIZEUPL).'</td>
                                    <td>'.$this->mfungsi->tgl($value->TGLUPL,true).'</td>
                                    <td>'.ucwords($value->JENISUPL).'</td>
                                    <td>'.$value->DESKUPL.'</td>
                                    <td>'.$del.'</td>
                                    </tr>';
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="body-tab" id="lembar-monitoring">
            <div class="row">
                <div class="col-12">

                    <?php if ($arsip==0) { ?>
                    <div class="alert alert-warning" role="alert">
                      Jenis file yang dapat di proses <b>".pdf"</b>
                    </div>
                    <?php } ?>
                    <h4>Unggah Lembar Monitoring</h4>
                    <?php if ($arsip==0) { ?>
                    <form action="<?php echo base_url('dosen/upload/lembar') ?>" method="post" onsubmit="return upload(this)" enctype="multipart/form-data" id="js-upload-form">
                      <div class="form-row">
                        <div class="form-group col-md-4">
                          <label for="inputEmail4">File</label>
                          <input autocomplete="off"  type="file" required class="form-control" accept="application/pdf" name="file" id="file">
                        </div>
                      </div>
                      <div class="form-row">
                          <button type="submit" oxnclick="upload()" class="btn btn-primary ml-1" id="js-upload-submit">Unggah</button>
                      </div>
                    </form>
                    <?php } ?>
                </div>
                <div>
                    <div class="row" style="overflow:  auto;">
                        <div class="col-12">
                            <table class="table datatable table-hover xloadfile">
                                <thead>
                                    <tr class="text-muted">
                                        <th></th>
                                        <th>File</th>
                                        <th>Size</th>
                                        <th>Tgl</th>
                                        <th>Hapus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php 
                                   foreach ($monitoring as $key => $value) {
                                        $ext = explode('.', $value->FILELM);
                                        $ext = end($ext);
                                            echo '<tr><td width="60px"><img src="'.base_url('assets/images/icons/'.$ext.'.png').'" class="img-fluid rounded"></td>
                                                <td><a '.(($ext=='pdf')? 'onclick="_cetak(this)"' : '' ).' href="'.base_url('dosen/upload/vmonit/').$value->KDLM.'" class="btn-link" title="'.$value->FILELM.'">'.$this->mfungsi->minimalText($value->FILELM,15).'</a>&nbsp;.&nbsp;<a href="'.base_url('mhs/dosen/upload/vdownload/').$value->KDLM.'">Download</a></td>
                                                <td>'.$this->mfungsi->koversiSize($value->SIZELM).'</td>
                                                <td>'.$this->mfungsi->tgl($value->TGLLM,true).'</td>
                                                <td>';
                                                if ($arsip==1) {
                                                    echo '<span class="text-muted">Hapus</span>';
                                                }else{
                                                    echo '<a href="'.base_url('dosen/upload/delete/').$value->KDLM.'" onclick="return hapus(this);" class="btn-link text-danger">Hapus</a></td>
                                                    </tr>';
                                                }
                                   }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    
  </div>
</div>

<div class="modal fade" id="again" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Mau diapakan?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="alert alert-warning" role="alert">
        	<b>Alasan ditolak</b> <span id="pesan-alasan"></span>
				</div>
				<div class="row">
					<div class="col text-center">
						<a href="<?php  echo base_url('dosen/upload/ajukan/').$value->KDUPL; ?>" id="ajukanulang" onclick="return ajukan(this);" class="btn btn-primary">Ajukan Kembali</a>

						<a href="<?php  echo base_url('dosen/upload/batalajukan/').$value->KDUPL; ?>" id="btlajukanulang" onclick="return ajukan(this);" class="btn btn-danger">Batalkan Pengajuan</a>
					</div>
				</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.datatable').DataTable({
            searching:true,
            info:false,
            paging:false
        })

        _tab({
            tab:'.tab-menu',
            body:'.body-tab',
            default:'#laporan-mhs'
        })
    });
	function ajukan(t){
		_alert({
			mode:'confirm',
			title:'Apakah proses akan dilanjutkan?',
			msg:'',
			yes:'Ya!',
			no:'Tidak',
			isConfirm:function(){
				window.location.href=$(t).attr('href');;
			}
		})
		return false;
	}

	function again(k,msg){
        var btlajukan= '<?php  echo base_url('dosen/upload/batalajukanulang/'); ?>'+k;
		var ajukan= '<?php  echo base_url('dosen/upload/ajukan/'); ?>'+k;
		$('#pesan-alasan').text(msg);
		$('#ajukanulang').attr('href', ajukan);
		$('#btlajukanulang').attr('href', btlajukan);
		$('#again').modal('toggle');
	}

	function upload(t){
		$('#progressUpload').remove();
		var htm = '<tr id="progressUpload"><td colspan="5"><div class="progress">'+
				  '<div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>'+
				'</div></td></tr>';
		$('.xloadfile').prepend(htm);
		_upload({
			name:'file',
			progress:function(res){
				$('#progressUpload .progress-bar').css('width', res.percent+'%');
				$('#progressUpload .progress-bar').text(res.percent+'%');
			},
			complete:function(res){
				console.log(res);
				res = JSON.parse(res);
				if(res.status==true){
					$('#progressUpload').remove();
					$('.xloadfile').prepend(res.data);
					$('#file').val('');
				}else{
					$('#progressUpload').html(res.data);
				}
			},
			action:$(t).attr('action')
		})
		return false;
	}

   function hapus(t){
        _alert({
            mode:'confirm',
            title:'Apakah akan dihapus?',
            msg:'',
            yes:'Ya, Hapus!',
            no:'Tidak',
            isConfirm:function(){
                window.location.href=$(t).attr('href');;
            }
        })
        return false;
    }
</script>