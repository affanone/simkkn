<div class="card">
  <div class="card-body">
    <h4 class="card-title">Data Laporan Evaluasi Mingguan</h4>

    <div class="row">
	    <div class="col offset-8 col-4">
	    	<a class="float-right btn btn-outline-primary" onclick="_cetak(this)" href="<?php echo base_url('dosen/evaluasi/cetak'); ?>"><i class="fa fa-print"></i>Cetak</a>
	    	<a class="float-right btn btn-primary  mr-3" href="<?php echo base_url('dosen/evaluasi/tambah'); ?>"><i class="fa fa-plus"></i>Tambah</a>
	    </div>
	    <div class="col col-12 mt-3">
		    <table class="table table-striped table-bordered">
		    	<tdead>
		    		<tr>
		    			<th>#</th>
		    			<th>Tanggal</th>
		    			<th>Tanggapan Masyarakat</th>
		    			<th>Hasil Survei Mahasiswa</th>
		    			<th>Kendala Yang Dihapadi Masyarakat</th>
		    			<th>Saran</th>
		    			<th>Opsi</th>
		    		</tr>
		    	</tdead>
		    	<tbody>
		    		<?php
		    		$i=1;
		    		foreach ($data as $key => $value) {
		    			?>
		    			<tr>
		    				<td><?php echo $i++; ?></td>
		    				<td><?php echo $this->mfungsi->tgl($value->TGLEV); ?></td>
		    				<td><?php echo $value->TANGGAPANEV; ?></td>
		    				<td><?php echo $value->SURVEIEV; ?></td>
		    				<td><?php echo $value->KENDALAEV; ?></td>
		    				<td><?php echo $value->SARANEV; ?></td>
		    				<td>
		    					<div class="btn-group">
									  <a class="btn btn-primary" href="<?php echo base_url('dosen/evaluasi/edit/'.$value->KDEV); ?>">Edit</a>
									  <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    <span class="sr-only">Toggle Dropdown</span>
									  </button>
									  <div class="dropdown-menu"><!-- 
									  	<a class="dropdown-item" href="'.base_url('admin/master/tahun/status/'.$value->KDTAHUN.'/1').'">Aktifkan</a>
									    <div class="dropdown-divider"></div> -->
									    <a class="dropdown-item text-danger" onclick="hapus('<?php echo base_url('dosen/evaluasi/hapus/'.$value->KDEV); ?>')" href="#!">Hapus</a>
									  </div>
									</div>
		    				</td>
		    			</tr>
		    			<?php
		    		}
		    		?>
		    	</tbody>
		    </table>
		  </div>
		</div>
  </div>
</div>
<script type="text/javascript">
	function hapus(url){
			_alert({
				mode:'confirm',
				title:'Apakah akan dihapus?',
				msg:'Semua data yang berhubungan dengan data ini akan ikut terhapus, apakah akan dilanjutkan?',
				yes:'Ya, lanjutkan!',
				no:'Tidak',
				isConfirm:function(){
					window.location = url;
				}
			})
	}
</script>