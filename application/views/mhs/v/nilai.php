<?php
$cek = intval($this->mfungsi->setting('hal_pstkelompok'));
if($cek==0){
?>
<div class="card">
  <div class="card-body">
    <center><h3>NILAI ANDA</h3></center>

    <div class="h1 bg-primary text-white text-center" style="
    width: 170px;
    font-size: 70px;
    font-weight:  bold;
    border-radius:  50%;
    height: 170px;
    margin:  auto;
    margin-top:  30px;
    display:  table;
    margin-bottom:  30px;
">
    	<span style="
    display:  table-cell;
    vertical-align:  middle;
"><?php echo $nilaihuruf = $this->mfungsi->nilaihuruf($nilai); ?></span>
    </div>
        <?php if($nilai==null){ ?>
            <div class="alert alert-info text-center" role="alert">
        	  Nilai anda belum diinputkan oleh DPL
        	</div>
        <?php }else if(($nilaihuruf=='B' || $nilaihuruf=='A') && $status == 2){ ?>
            <div class="alert alert-success text-center" role="alert">
              Kami telah membuat sertifikat elektronik untuk anda! <br><strong>Kami sarankan untuk membukanya Google Chrome<br><br>
              <a class="btn btn-primary btn-lg" href="<?php echo base_url('mhs/ver/nilai/sertifikat') ?>">Lihat Sertifikat</a>
            </div>
        <?php }else{ ?>
            <div class="alert alert-warning text-center" role="alert">
              Apabila nilai di atas yang menurut anda tidak sesuai, silahkan konsultasi ke Dosen Pendamping Lapangan anda!
            </div>
        <?php } ?>
  </div>
</div>
<?php
}
?>