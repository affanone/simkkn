<?php
$disable ='';
$cekdis = intval($this->mfungsi->setting('edit_biodata_mhs'));
if($cekdis==0){
	$disable = ' disabled ';
}
?>
<div class="row justify-content-center">
	<div class="col-lg-8">
		<div class="card">
		  <div class="card-body"> 
		  		<?php
		      if($cekdis==0){
		    		echo '<div class="form-group"><i class="text-muted">BIODATA TIDAK DIIJINKAN UNTUK DIPERBARUI</i></div>';
		    	}
		      ?>
		    <form xonsubmit="return submitform(this)" id="formreg" class="row" action="<?php echo base_url('mhs/ver/bio/update'); ?>" method="post">
		    	<div class="col-md-4 fotoloading">
		    		<img src="<?php echo $this->session->foto.'?'.rand(111,999); ?>" alt="NO FOTO" class="img-thumbnail w-100 fotodisplay">
		    		<div class="w-100 progress-status mb-4 mt-2" style="display: none;">
		    			<div class="progress">
							  <div class="progress-bar" st role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
		    		</div>
		    		<div class="w-100 text-center opsi-foto mb-4 mt-2">
		    			<a href="#!" onclick="return defaultfoto()" class="text-danger">Default</a>&nbsp;.&nbsp;
		    			<label style="cursor:pointer;" class="btn-link">
							    <span class="text-primary">Ganti</span><input autocomplete="off"  type="file" onchange="gantifoto(this)" id="foto" hidden accept="image/jpeg">
							</label>
		    		</div>
		    	</div>
		    	<div class="col-md-8">
			      <div class="form-group">
		        <label for="npm">NPM</label>
			        <input autocomplete="off"  type="text" class="form-control" disabled value="<?php echo $this->session->usern; ?>" id="npm" xplaceholder="NPM">
			      </div>
			      <div class="form-group">
			        <label for="nama">Nama</label>
			        <input autocomplete="off"  type="text" class="form-control" disabled value="<?php echo $this->session->nama; ?>" id="nama" xplaceholder="Nama">
			      </div>
			      <div class="form-group" id="alamatvalid">
			        <label for="alamat">Alamat <small class="font-italic text-warning">Desa, Kecamatan, Kabupaten</small></label>
			        <textarea disabled type="text" class="form-control" placeholder="Alamat"><?php echo $alamat; ?></textarea>
			      </div>
			      <div class="form-group">
			        <label for="prodi">Prodi</label>
			        <input autocomplete="off"  type="text" class="form-control" disabled value="<?php echo $this->session->prodi; ?>" id="prodi" name="prodi" xplaceholder="Prodi">
			      </div>
			      <div class="form-group">
			        <label for="telp">Telp <small class="font-italic text-warning">Gunakan nomor yang dapat kami hubungi</small></label>
			        <input autocomplete="off"  type="text" class="form-control" <?php echo $disable; ?> value="<?php echo $telp; ?>" id="telp" name="telp" xplaceholder="Telp">
			        <?php  echo form_error('telp','<small class="text-danger">','</small>'); ?>
			      </div>
			      <div class="form-group">
			        <label for="kerja">Pekerjaan <small class="font-italic text-warning">Biarkan apabila anda tidak bekerja</small></label>
			        <input autocomplete="off"  type="text" <?php echo $disable; ?> class="form-control" value="<?php echo $kerja; ?>" id="kerja" name="kerja" xplaceholder="Pekerjaan">
			      </div>
			      <div class="form-group">
			        <label for="sks">SKS yang sudah ditempuh <small class="font-italic text-warning">Sesuaikan dengan Transkip terbaru anda</small></label>
			        <input autocomplete="off"  <?php echo $disable; ?> type="text" class="form-control" value="<?php echo $sks; ?>" id="sks" name="sks" xplaceholder="sks">
			        <?php  echo form_error('sks','<small class="text-danger">','</small>'); ?>
			      </div>
			      <div class="form-group">
			        <label for="seragam">Ukuran seragam <small class="font-italic text-warning">Sesuaikan dengan ukuran tubuh anda</small></label>
			        <?php  echo form_error('seragam','<br><small class="text-danger">','</small>'); ?>
			        <div class="col-md-6">
			            <div class="form-group">
			              <div class="form-radio">
			                <label class="form-check-label">
			                  <input autocomplete="off"  type="radio" <?php echo $disable; ?> class="form-check-input" <?php echo ($seragam=='xl') ? 'checked':''; ?> name="seragam" id="xl" value="xl">
			                  XL
			                <i class="input-helper"></i></label>
			              </div>
			              <div class="form-radio">
			                <label class="form-check-label">
			                  <input autocomplete="off"  type="radio" <?php echo $disable; ?> class="form-check-input" <?php echo ($seragam=='l') ? 'checked':''; ?> name="seragam" id="l" value="l">
			                  L
			                <i class="input-helper"></i></label>
			              </div>
			              <div class="form-radio">
			                <label class="form-check-label">
			                  <input autocomplete="off"  type="radio" <?php echo $disable; ?> class="form-check-input" name="seragam" <?php echo ($seragam=='m') ? 'checked':''; ?> id="m" value="m">
			                  M
			                <i class="input-helper"></i></label>
			              </div>
			              <div class="form-radio">
			                <label class="form-check-label">
			                  <input autocomplete="off"  type="radio" <?php echo $disable; ?> class="form-check-input" <?php echo ($seragam=='s') ? 'checked':''; ?> name="seragam" id="s" value="s">
			                  S
			                <i class="input-helper"></i></label>
			              </div>
			            </div>
			          </div>
			      </div>
			    </div>
		      <?php
		      if($cekdis==1){
		      ?>
		      <div class="mt-5 xloading pull-right">
		        <button class="btn btn-primary font-weight-medium" type="submit">Simpan</button>
		      </div>
		      <?php
		    	}
		      ?>
		    </form>
		  </div>
		</div>
		
	</div>
</div>

  <div class="modal fade" tabindex="-1" role="dialog" id="map-google">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Cari Alamat</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
  $(document).ready(function() {
    _ajax({
      url:'<?php echo base_url('map'); ?>',
      success:function(res){
        $('#map-google .modal-body').html(res);
      }
    })
  });
  function findaddress(){
    $('#map-google').modal('show');
  }
</script>

<?php
if(isset($this->session->biodata_update)){
?>
<script type="text/javascript">
	$(document).ready(function() {
      _alert('Data berhasil di ubah');
  });
</script>
<?php
}
$this->session->unset_userdata('biodata_update');
?>
<script type="text/javascript">
		var _alamatValid = false;
  function validasialamat(){
    var alm = $('#alamat').val();
    _ajax({
      url:'<?php echo base_url('mhs/daftar/alamat'); ?>',
      data:{
        alamat:alm
      },
      loading:'#alamatvalid',
      append:true,
      success:function(res){
      	var _alamatValid = false;
        res = JSON.parse(res);
        if(res.status==false){
          _alert('Kesalahan','Alamat tidak tersedia, inputkan dalam cakupan nama kelurahan kecamatan dan kabupaten','error');
        }else{
        	_alamatValid = true; 
          _form({alamat:res.result.text})
          $('.xerralamat').text('');
        }        
      }
    })
  }

  function gantifoto(t){
  	_upload({
  		action:'<?php echo base_url('mhs/ver/bio/foto/ganti') ?>',
  		name:'foto',
  		progress:function(res){
  			$('.opsi-foto').hide();
  			$('.progress-status').show();
  			$('.progress-status .progress-bar').css('width', res.percent+'%');
  		},
  		complete:function(res){
  			res = JSON.parse(res);
  			$('.opsi-foto').show();
  			$('.progress-status').hide();
  			if(res.status==true){
	  			$('.fotodisplay').attr('src', res.foto);
	  			$('.progress-status .progress-bar').css('width', '0%');
	  		}else{
	  			alert(res.foto);
	  		}
  		}
  	})
  }

  function defaultfoto(){
  	_ajax({
  		url:'<?php echo base_url('mhs/ver/bio/foto/default') ?>',
  		loading:'.fotoloading',
  		append:false,
  		success:function(res){
  			res = JSON.parse(res);
  			$('.fotodisplay').attr('src', res.foto);
  		}
  	})
  }
</script>