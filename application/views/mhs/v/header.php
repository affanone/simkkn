<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SIM-KKN | UNIVERSITAS MADURA</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/font-awesome/css/font-awesome.min.css" />

  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/datatable/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/bootstrapselect/css/bootstrap-select.min.css">

  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/preloader.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/mhs-ver.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
  <script src="<?php echo base_url('assets/'); ?>node_modules/jquery/dist/jquery.min.js"></script>
  <noscript>
    Aktifkan javascript pada browser
    <style>div { display:none; }</style>
  </noscript>
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="<?php echo base_url(); ?>"><span style="
    color: #f6f8fa;
">SIM</span><span style="
    color:  red;
    font-weight:  bold;
">KKN</span></a>
        <a class="navbar-brand brand-logo-mini" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/'); ?>images/logobw.png" style="width: 40px;height: 40px;" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex" style="width: 70%;">
          <li class="nav-item">
            <?php
              $menu = $this->uri->segment(3);
              $submenu = $this->uri->segment(4);
              switch ($menu) {
                case 'home':
                  echo profil('aplikasi');
                break;
                case 'kelompok':
                  echo 'DATA PROFIL KELOMPOK KKN';
                break;
                case 'upload':
                  echo 'UNGGAH LAPORAN KKN';
                break;
                case 'nilai':
                  if($submenu=='sertifikat')
                    echo 'SERTIFIKAT ELEKTRONIK';
                  else
                    echo 'NILAI HASIL KKN';
                break;
                case 'album':
                  echo 'FOTO-FOTO';
                break;
                case 'bio':
                  echo 'BIODATA DIRI';
                break;
                default:
                  echo profil('aplikasi');
                  break;
              }
              $s = $this->mfungsi->imgsize($this->session->foto);
              if($s['w']>$s['h']){
                $atr = 'height="100%"';
              }else{
                $atr = 'width="100%"';
              }
            ?>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <div class="img-xs rounded-circle text-center" style="overflow: hidden;background: black;">
              <img <?php echo $atr; ?> class="m-auto  fotodisplay" src="<?php echo $this->session->foto.'?'.rand(111,999); ?>" alt="">
              </div>
              
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
              <a class="dropdown-item preview-item">
                <div class="row" style="margin:  auto;text-align: center;">
                  <div class="col-12">
                    <div style="width:  120px;height:  120px; border-radius: 50%; overflow: hidden;" class="img-xs rounded-circle m-auto">
                      <img class="fotodisplay"  <?php echo $atr; ?>  src="<?php echo $this->session->foto.'?'.rand(111,999); ?>" alt="">
                    </div>                    
                  </div>
                  <div class="col-12 mt-2"><span class="font-weight-bold"><?php echo $this->session->usern; ?></span></div>
                  <div class="col-12"><span><?php echo $this->session->nama; ?></span></div>
                  <div class="col-12">
                    <?php
                    if($this->session->tidak_lulus==true){
                      echo '<span style="margin:0;" class="badge badge-danger badge-pill font-italic">Tidak Lulus</span>';
                    }else if($this->session->mhs_lulus==true){
                      echo '<span style="margin:0;" class="badge badge-primary badge-pill font-italic">Lulus</span>';
                    }else{
                      echo '<span style="margin:0;" class="badge badge-success badge-pill font-italic">Terkelompok</span>';
                    }
                    ?>
                    
                  </div>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item" href="<?php echo base_url('mhs/ver/bio'); ?>">
                Biodata
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item" href="<?php echo base_url('keluar'); ?>">
                Keluar
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="fa fa-bars"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          
          <li class="nav-item <?php echo ($menu=='home') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('mhs/ver/home'); ?>"><i class="menu-icon fa fa-home"></i><span class="menu-title">Dashboard</span></a></li>

          <li class="nav-item <?php echo ($menu=='kelompok') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('mhs/ver/kelompok'); ?>"><i class="menu-icon fa fa-sticky-note"></i><span class="menu-title">Profil KKN</span></a></li>
          <?php
          $cek = intval($this->mfungsi->setting('hal_pstkelompok'));
          if($cek==0){
          ?>          
          <li class="nav-item <?php echo ($menu=='nilai') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('mhs/ver/nilai'); ?>"><i class="menu-icon fa fa-thumbs-up"></i><span class="menu-title">Nilai</span></a></li>

          <li class="nav-item <?php echo ($menu=='upload') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('mhs/ver/upload'); ?>"><i class="menu-icon fa fa-upload"></i><span class="menu-title">Unggah Laporan</span></a></li>
          <?php  } ?>
          <!-- <li class="nav-item <?php echo ($menu=='album') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('mhs/ver/album'); ?>"><i class="menu-icon fa fa-image"></i><span class="menu-title">Album</span></a></li> -->

        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">