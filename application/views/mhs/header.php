<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SIM-KKN | UNIVERSITAS MADURA</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/font-awesome/css/font-awesome.min.css" />

  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/datatable/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/bootstrapselect/css/bootstrap-select.min.css">

  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/preloader.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
  <?php if($this->session->enable_edit_biodata==true){ ?>
    <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/mhs-waiting.css">
  <?php }else{ ?>
    <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/mhs.css">
  <?php } ?>
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
  <script src="<?php echo base_url('assets/'); ?>node_modules/jquery/dist/jquery.min.js"></script>
  <noscript>
    Aktifkan javascript pada browser
    <style>div { display:none; }</style>
  </noscript>
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="<?php echo base_url(); ?>"><span style="
    color: #f6f8fa;
">SIM</span><span style="
    color:  red;
    font-weight:  bold;
">KKN</span></a>
        <a class="navbar-brand brand-logo-mini" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/'); ?>images/logobw.png" style="width: 40px;height: 40px;"  alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex" style="width: 70%;">
          <li class="nav-item">
            <?php
              $menu = $this->uri->segment(2);
              $submenu = $this->uri->segment(3);
              switch ($menu) {
                case 'beranda':
                  if($submenu=='verified')
                    echo 'SYARAT VERIFIKASI PENDAFTARAN KKN';
                  else
                    echo profil('aplikasi');
                break;
                case 'verified':
                  echo 'SYARAT VERIFIKASI PENDAFTARAN KKN';
                break;
                case 'biodata':
                  echo 'BIODATA DIRI ANDA';
                break;
                case 'anggota':
                  echo 'Anggota KKN Tahun Akademik '.$this->mfungsi->tahun()->label;
                break;
                default:
                  echo profil('aplikasi');
                  break;
              }
            ?>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <img class="fotodisplay img-xs rounded-circle" src="<?php echo base_url('assets/'); ?>images/faces/user.png" alt="">
              
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
              <a class="dropdown-item preview-item">
                <div class="row" style="margin:  auto;text-align: center;">
                  <div class="col-12">
                    <img style="width:  100%;height:  100%;" class="img-xs rounded-circle" class="fotodisplay" src="<?php echo base_url('assets/'); ?>images/faces-clipart/pic-5.png" alt="">
                  </div>
                  <div class="col-12 mt-2"><span class="font-weight-bold"><?php echo $this->session->usern; ?></span></div>
                  <div class="col-12"><span><?php echo $this->session->nama; ?></span></div>
                  <div class="col-12">
                    <?php
                    if($this->session->verifikasi==0){
                    ?>
                    <?php if($this->session->enable_edit_biodata==true){ ?>
                    <span style="margin:0;" class="badge badge-warning badge-pill font-italic">Menunggu</span>
                      <?php }else{ ?>
                    <span style="margin:0;" class="badge badge-danger badge-pill font-italic">Tidak Terkelompok</span>
                    <?php } ?>
                    <?php
                    }else{
                    ?>
                    <span style="margin:0;" class="badge badge-success badge-pill font-italic">Terkelompok</span>
                    <?php
                    }
                    ?>
                  </div>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item" href="<?php echo base_url('keluar'); ?>">
                Keluar
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="fa fa-bars"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">

          
          <li class="nav-item <?php echo ($menu=='beranda') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('mhs/beranda'); ?>"><i class="menu-icon fa fa-home"></i><span class="menu-title">Beranda</span></a></li>

          <?php if($this->session->enable_edit_biodata!=true){ ?>
            <li class="nav-item <?php echo ($menu=='verified') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('mhs/verified'); ?>"><i class="menu-icon fa fa-check"></i><span class="menu-title">Verifikasi</span></a></li>
          <?php  } ?>

          <li class="nav-item <?php echo ($menu=='biodata') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('mhs/biodata'); ?>"><i class="menu-icon fa fa-id-card-o"></i><span class="menu-title">Biodata</span></a></li>
          


        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">

<?php
if($this->session->verifikasi==0 && $this->uri->segment(2)!='verified'){
?>
<?php if($this->session->enable_edit_biodata==true){ ?>
<div class="alert alert-warning" role="alert">
  <b><i class="fa fa-thumbs-up"></i> Informasi</b><br>Data anda sudah kami terima, tinggal menunggu pada tahap pembentukan kelompok kkn!</a>
</div>
<?php
}else{
  ?>
<div class="alert alert-danger" role="alert">
  <b><i class="fa fa-exclamation-triangle"></i> Peringatan</b><br>Silahkan verifikasi pendaftaran anda ke LPPM dengan syarat <a href="<?php echo base_url('mhs/verified') ?>">baca disini</a>
</div>
  <?php
}
}
?>