<div class="alert alert-warning" role="alert">
  Segeralah melakukan verifikasi agar kesempatan untuk mendapat lokasi terdekat bisa anda tempati.<!--  batas verifikasi sampai dengan <b><?php //echo $this->mfungsi->tgl($this->mfungsi->batas_verifikasi()); ?></b> -->
</div>
<div class="card">
  <div class="card-body">
  	<p>
    Lakukan langkah demi langkah dibawah ini.</p>
    <ul>
      <li>Pastikan anda sudah membayar biaya Kuliah Kerja Nyata (KKN) tahun akademik <?php echo $this->mfungsi->tahun()->label; ?></li>
    	<li>Cetak formulir biodata diri anda <a href="#!" onclick="openbiodata()">disini</a></li>
      <li>Silahkan ditanda tangani biodata yang sudah di cetak sebagai persetujuan dari Prodi</li>
      <li>Setorkan berkas (biodata yang sudah ditandatangani kaprodi + foto warna 2 lembar ukuran 3x4 Cm + kwitansi asli bukti pembayaran KKN) ke LPPM</li>
    </ul>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="biodata" tabindex="-1" role="dialog" aria-labelledby="titleexcel" aria-hidden="true">
  <div class="modal-dialog xmodal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titleexcel">Biotodata Mahasiswa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body xbiodata">
      	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <a onclick="_cetak(this)" href="<?php echo base_url('mhs/cetak/biodata'); ?>" class="btn btn-primary">Cetak</a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	function openbiodata(){
		$('.xbiodata').html('');
		$('#biodata').modal('toggle');
		_ajax({
			url:'<?php echo base_url('mhs/biodata/view'); ?>',
			loading:'.xbiodata',
			success:function(data){
				$('.xbiodata').html(data);
			}
		})
	}
</script>