<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SIM-KKN | UNIVERSITAS MADURA</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/bootstrapselect/css/bootstrap-select.min.css">
  <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
  <noscript>
    Aktifkan javascript pada browser
    <style>div { display:none; }</style>
  </noscript>
  <style type="text/css">
    .a {
        display: table;
        width: 100%;
        position: absolute;
        height: 100%;
        padding: 30px;
    }
    .b {
        display: table-cell;
        vertical-align: middle;
    }
    .icn {
        font-size: 6pc;
    }
  </style>
</head>

<body class="bg-warning">
<div class="a text-center">
  <div class="b">
    <div class="mb-3 text-danger">
      <i class="fa icn fa-exclamation"></i>
    </div>
    <div class="font-weight-bold text-white h1 mb-2">
      PERHATIAN
    </div>
    <div class="text-danger h3">
      <?php echo $pesan; ?>
    </div>
    <div class="font-weight-bold h2 mt-3 text-info">
     <a class="btn btn-lg btn-primary" href="<?php echo base_url('keluar'); ?>">Keluar</a>
    </div>
  </div>
</div>
</body>
</html>