</div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © <?php echo date('Y'); ?> <a href="https://lp3m.unira.ac.id" target="_blank">LPPM</a>. Universitas Madura Pamekasan.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">LPPM UNIRA</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="<?php echo base_url('assets/'); ?>node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url('assets/'); ?>node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="<?php echo base_url('assets/'); ?>node_modules/chart.js/dist/Chart.min.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->

    <script src="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>plugins/datatable/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>plugins/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>plugins/daterangepicker/moment.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>plugins/daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url('assets/'); ?>plugins/bootstrapselect/js/bootstrap-select.min.js"></script>
  
  <script src="<?php echo base_url('assets/'); ?>js/off-canvas.js"></script>
  <script src="<?php echo base_url('assets/'); ?>js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo base_url('assets/'); ?>js/dashboard.js"></script>
  <!-- <script src="<?php echo base_url('assets/'); ?>https://maps.googleapis.com/maps/api/js?key=AIzaSyB5NXz9eVnyJOA81wimI8WYE08kW_JMe8g&amp;callback=initMap" async defer></script> -->
  <script src="<?php echo base_url('assets/'); ?>js/maps.js"></script>
  <script src="<?php echo base_url('assets/'); ?>js/fungsi.js?<?php echo rand(111,999); ?>"></script>
  <!-- End custom js for this page-->
</body>


<!-- Mirrored from www.bootstrapdash.com/demo/star-admin-free/jquery/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Feb 2018 07:02:14 GMT -->
</html>