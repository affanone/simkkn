<div class="container mt-2">
	<?php 
	$this->load->view('mhs/print/kop');
?>
	<div class="mt-2 mb-2 text-center card-outline-secondary"><u>FORMULIR PENDAFTARAN KULIAH KERJA NYATA (KKN)<br>TAHUN AKADEMIK <?php echo $this->mfungsi->tahun()->label; ?></u></div>

			<table class="mt-3 table-striped table" style="font-size: 14px;">
			  <tbody>
			  	<tr>
			  		<td width="3">1.</td>
			  		<td width="350">Nama Mahasiswa</td>
			  		<td width="1">:</td>
			  		<td><?php echo strtoupper($nama); ?></td>
			  	</tr>
			  	<tr>
			  		<td>2.</td>
			  		<td>Nama Pokok Mahasiswa</td>
			  		<td>:</td>
			  		<td><?php echo $npm; ?></td>
			  	</tr>
			  	<tr>
			  		<td>3.</td>
			  		<td>Fakultas / Prodi</td>
			  		<td>:</td>
			  		<td><?php echo strtoupper($prodi); ?></td>
			  	</tr>
			  	<tr>
			  		<td>4.</td>
			  		<td>Nomor Kontak</td>
			  		<td>:</td>
			  		<td><?php echo strtoupper($telp); ?></td>
			  	</tr>
			  	<tr>
			  		<td>5.</td>
			  		<td>Pekerjaan</td>
			  		<td>:</td>
			  		<td><?php echo ($kerja=='')?'-':$kerja; ?></td>
			  	</tr>
			  	<tr>
			  		<td>6.</td>
			  		<td>Alamat</td>
			  		<td>:</td>
			  		<td><?php echo $alamat; ?></td>
			  	</tr>
			  	<tr>
			  		<td>7.</td>
			  		<td>Jumlah Kumulatif sks yang diperoleh</td>
			  		<td>:</td>
			  		<td><?php echo $sks; ?></td>
			  	</tr>
			  	<tr>
			  		<td>8.</td>
			  		<td>Ukuran Seragam</td>
			  		<td>:</td>
			  		<td><?php echo strtoupper($seragam); ?></td>
			  	</tr>
			  	<tr>
			  		<td>9.</td>
			  		<td colspan="3"> Menyatakan mendaftarkan diri sebagai peserta Kuliah Kerja Nyata (KKN) Mahasiswa Universitas Madura Tahun Akademik <?php echo $this->mfungsi->tahun()->label; ?>, dan menyatakan patuh serta tunduk kepada semua ketentuan Akademik yang berlaku.</td>
			  	</tr>
			  	<tr>
			  		<td>10.</td>
			  		<td colspan="3">Catatan : <b><i>Peseta KKN menyertakan Foto 3x4 Cm, 2 Lembar warna.</i></b></td>
			  	</tr>

			  </tbody>
			</table>
<div class="row mt-5">
	<div class="col-12">
		<div class="col-5 offset-7">
			Pamekasan, . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .  <?php echo date('Y'); ?>
		</div>
	</div>
	<div class="col-7">
		<div class="font-weight-bold">
			Peserta KKN,
		</div><br><br>
		<div class="mt-5">
			(. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .)
		</div>
	</div>
	<div class="col-5">
		<div class="font-weight-bold">
			Pembantu Dekan I / Kaprodi,
		</div><br><br>
		<div class="mt-5">
			(. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .)
		</div>
	</div>
</div>