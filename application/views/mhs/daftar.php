<?php
$cek = intval($this->mfungsi->setting('halaman_daftar'));
if($cek==1){
  $this->load->view('locked2');
}else{
?>
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.bootstrapdash.com/demo/star-admin-free/jquery/pages/samples/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Feb 2018 07:02:26 GMT -->
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Mendaftar KKN</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/font-awesome/css/font-awesome.min.css" />
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/preloader.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
  <style type="text/css">
  	.auth form .form-group i{
  		position: unset;
  	}
  </style>
    <script src="<?php echo base_url('assets/'); ?>node_modules/jquery/dist/jquery.min.js"></script>
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth register-full-bg">
        <div class="row w-100">
          <div class="col-lg-5 mx-auto">
            <div class="auth-form-light text-left p-5">
              <h2>Mendaftar</h2>
              <h4 class="font-weight-light">Sebagai anggota kkn tahun akademik <?php echo $this->mfungsi->tahun()->label; ?></h4>
                <form xonsubmit="return submitform(this)" id="formreg" action="<?php echo base_url('mhs/daftar/simpan'); ?>" method="post">
                  <div class="form-group">
                    <label for="npm">NPM</label>
                    <input autocomplete="off"  type="text" class="form-control" disabled value="<?php echo $this->session->usern; ?>" id="npm" xplaceholder="NPM">
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input autocomplete="off"  type="text" class="form-control" disabled value="<?php echo $this->session->nama; ?>" id="nama" xplaceholder="Nama">
                  </div>
                  <div class="alert alert-warning" role="alert">
									  <b>Penting..! </b>Silahkan cari alamat anda di <a href="#!" onclick="findaddress();" class="font-weight-bold">disini</a> untuk menentukan tempat tinggal anda saat ini! hal ini bertujuan agar kami dapat mempertimbangkan tempat kkn yang akan anda tempati
									</div>
                  <div class="form-group" id="alamatvalid">
                    <label for="alamat">Alamat <small class="font-italic text-warning">Desa, Kecamatan, Kabupaten</small></label>
                      <?php echo form_error('kordx','<br><small class="text-danger">','</small>'); ?>
                      <?php echo form_error('kordy','<br><small class="text-danger">','</small>'); ?>
                    <input autocomplete="off"  type="text" class="form-control" value="<?php echo $alamat; ?>" id="alamat" name="alamat" xplaceholder="Alamat">
                    <input autocomplete="off"  type="hidden" required="" value="<?php echo $kordx; ?>" id="kordx" name="kordx" placeholder="lat">
                    <input autocomplete="off"  type="hidden" required="" value="<?php echo $kordy; ?>" id="kordy" name="kordy" placeholder="lng">
                    <?php  echo form_error('alamat','<small class="text-danger xerralamat">','</small>'); ?>
                    <br>

                    <a href="#!" class="btn-link float-right"  onclick="findaddress();">Buka Peta</a>

                   <!--  <div class="btn-group" role="group" aria-label="Button group with nested dropdown" style="    float: right;">
                      <button onclick="validasialamat();" type="button" class="btn btn-primary">Buka Peta</button>

                      <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                          <a class="dropdown-item" onclick="findaddress();" href="#">Buka Map</a>
                        </div>
                      </div>
                    </div> -->

                  <br>
                  </div>
                  <div class="form-group">
                    <label for="prodi">Prodi</label>
                    <input autocomplete="off"  type="text" class="form-control" disabled value="<?php echo $this->session->prodi; ?>" id="prodi" name="prodi" xplaceholder="Prodi">
                  </div>
                  <div class="form-group">
                    <label for="telp">Telp <small class="font-italic text-warning">Gunakan nomor yang dapat kami hubungi</small></label>
                    <input autocomplete="off"  type="text" class="form-control" value="<?php echo $telp; ?>" id="telp" name="telp" xplaceholder="Telp">
                    <?php  echo form_error('telp','<small class="text-danger">','</small>'); ?>
                  </div>
                  <div class="form-group">
                    <label for="kerja">Pekerjaan <small class="font-italic text-warning">Biarkan apabila anda tidak bekerja</small></label>
                    <input autocomplete="off"  type="text" class="form-control" value="<?php echo $kerja; ?>" id="kerja" name="kerja" xplaceholder="Pekerjaan">
                  </div>
                  <div class="form-group">
                    <label for="sks">SKS yang sudah ditempuh <small class="font-italic text-warning">Sesuaikan dengan Transkip terbaru anda</small></label>
                    <input autocomplete="off"  type="text" class="form-control" value="<?php echo $sks; ?>" id="sks" name="sks" xplaceholder="sks">
                    <?php  echo form_error('sks','<small class="text-danger">','</small>'); ?>
                  </div>
                  <div class="form-group">
                    <label for="seragam">Ukuran seragam <small class="font-italic text-warning">Sesuaikan dengan ukuran tubuh anda</small></label>
                    <?php  echo form_error('seragam','<br><small class="text-danger">','</small>'); ?>
                    <div class="col-md-6">
                        <div class="form-group">
                          <div class="form-radio">
                            <label class="form-check-label">
                              <input autocomplete="off"  type="radio" class="form-check-input" <?php echo ($seragam=='xl') ? 'checked':''; ?> name="seragam" id="xl" value="xl">
                              XL
                            <i class="input-helper"></i></label>
                          </div>
                          <div class="form-radio">
                            <label class="form-check-label">
                              <input autocomplete="off"  type="radio" class="form-check-input" <?php echo ($seragam=='l') ? 'checked':''; ?> name="seragam" id="l" value="l">
                              L
                            <i class="input-helper"></i></label>
                          </div>
                          <div class="form-radio">
                            <label class="form-check-label">
                              <input autocomplete="off"  type="radio" class="form-check-input" name="seragam" <?php echo ($seragam=='m') ? 'checked':''; ?> id="m" value="m">
                              M
                            <i class="input-helper"></i></label>
                          </div>
                          <div class="form-radio">
                            <label class="form-check-label">
                              <input autocomplete="off"  type="radio" class="form-check-input" <?php echo ($seragam=='s') ? 'checked':''; ?> name="seragam" id="s" value="s">
                              S
                            <i class="input-helper"></i></label>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="mt-5 xloading">
                    <button class="btn btn-block btn-primary btn-lg font-weight-medium" type="submit">Daftar</button>
                  </div>
                  <div class="mt-3">
                  	<center><a href="<?php echo base_url('keluar'); ?>">Keluar</a></center>
                  </div>
                </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>


  <div class="modal fade" tabindex="-1" role="dialog" id="map-google">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Cari Alamat</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
  $(document).ready(function() {
    _ajax({
      url:'<?php echo base_url('map'); ?>',
      success:function(res){
        $('#map-google .modal-body').html(res);
      }
    })
  });
  function findaddress(){
    $('#map-google').modal('show');
  }
</script>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.min.js"></script>
  <script src="<?php echo base_url('assets/'); ?>node_modules/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/'); ?>node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url('assets/'); ?>node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="<?php echo base_url('assets/'); ?>js/off-canvas.js"></script>
  <script src="<?php echo base_url('assets/'); ?>js/misc.js"></script>
  <script src="<?php echo base_url('assets/'); ?>js/fungsi.js?<?php echo rand(111,999); ?>"></script>
  <script type="text/javascript">
  var _alamatValid = false;
  $(document).ready(function() {
    _ajax({
      url:'<?php echo base_url('map'); ?>',
      success:function(res){
        $('#map-google .modal-body').html(res);
      }
    })
  });
  function findaddress(){
    $('#map-google').modal('show');
  }
  function validasialamat(){
    var alm = $('#alamat').val();
    _ajax({
      url:'<?php echo base_url('mhs/daftar/alamat'); ?>',
      data:{
        alamat:alm
      },
      loading:'#alamatvalid',
      append:true,
      success:function(res){
        res = JSON.parse(res);
        if(res.status==false){
          var _alamatValid = false;
          if(res.message=='OVER_QUERY_LIMIT'){
             _alert('Mohon Maaf','Layanan untuk mem-Validasi alamat tidak merespon, silahkan menghubungi admin untuk menggunakan layanan ini');
          }else{
            _alert('Kesalahan','Alamat tidak tersedia, inputkan dalam cakupan nama kelurahan kecamatan dan kabupaten','error');
          }
        }else{
        	_alamatValid = true; 
          _form({
            alamat:res.alamat,
            kordx:res.lat,
            kordy:res.lng
          })
          $('.xerralamat').text('');
        }        
      }
    })
  }

  function submitform(t){
  	if(_alamatValid==true){
  		_ajax({
  			url:$(t).attr('action'),
  			data:$(t).serialize(),
  			loading:'.xloading',
  			success:function(d){
  			}
  		})
  	}else{
  		_alert('Kesalahan','Alamat belum di valiad, silahkan validasi kembali','error');
  	}
  	return false;
  }
</script>
  <!-- endinject -->
</body>
</html>
<?php } ?>