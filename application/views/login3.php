<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login V10</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/login/') ?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/login/') ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/login/') ?>fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/login/') ?>vendor/animate/animate.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/login/') ?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/login/') ?>vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/login/') ?>vendor/select2/select2.min.css">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/login/') ?>vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/login/') ?>css/util.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/login/') ?>css/main.css">

    <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
<!--===============================================================================================-->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/fancy/dist/') ?>jquery.fancybox.min.css">

    <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.css">

</head>
<body>
    
    <div class="" id="logo-unira" style="position:  fixed;width:  100%;height:  100%;display:  table;">
        <div style="display:  table-cell;vertical-align:  middle;">
            <div class="row text-center" onclick="login()">
                <div class="col-12 mb-3">
                    <img width="210px" src="<?php echo base_url('assets/images/logo.png'); ?>" class=" img-fluid">
                </div>
                <div class="col-12 h2 font-weight-bold">
                    SIM<font class="text-danger">KKN</font> UNIVERSITAS MADURA
                </div>
                <div class="col-12 h4">
                    LEMBAGA PENELITIAN DAN PENGABDIAN PADA MASYARAKAT
                </div>
            </div>
        </div>
    </div>


    <div class="limiter" style="display: none;" id="form-login">
        <div class="container-login100">
            <div class="wrap-login100 p-t-50 p-b-90">
                <form method="post" class="login100-form validate-form flex-sb flex-w" action="<?php echo base_url('login/masuk'); ?>">
                    <span class="login100-form-title p-b-51">
                        Login
                    </span>

                    
                    <div class="wrap-input100 validate-input m-b-16" data-validate = "Username is required">
                        <input autocomplete="off"  class="input100" type="text" name="uname" placeholder="Username">
                        <span class="focus-input100"></span>
                    </div>
                    
                    
                    <div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
                        <input autocomplete="off"  class="input100" type="password" name="passwd" placeholder="Password">
                        <span class="focus-input100"></span>
                    </div>
                    
                    <div class="flex-sb-m w-full p-t-3 p-b-24">

                        <div>
                            <a href="<?php echo base_url('notlogin'); ?>" target="_blank"  class="txt1">
                                Tidak bisa masuk?
                            </a>
                        </div>
                    </div>

                    <div class="container-login100-form-btn m-t-17">
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    

    
    <!-- #region Jssor Slider Begin -->
    <!-- Generator: Jssor Slider Maker -->
    <!-- Source: https://www.jssor.com -->
    <script src="<?php echo base_url('assets/plugins/corrasual/') ?>js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $Idle: 0,
              $SlideDuration: 5000,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 140,
              $Align: 0
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 0;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /*jssor slider loading skin spin css*/
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }
    </style>
    <div id="jssor_1" class="d-none d-md-block" style="position:fixed;margin:0 auto;bottom:0px;left:0px;width:980px;height:100px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="<?php echo base_url('assets/plugins/corrasual/') ?>img/spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:100px;overflow:hidden;">
            <div data-p="30.00">
                <img data-u="image" src="<?php echo base_url('assets/plugins/corrasual/') ?>img/001.jpg" />
            </div>
            <div data-p="30.00">
                <img data-u="image" src="<?php echo base_url('assets/plugins/corrasual/') ?>img/002.jpg" />
            </div>
            <div data-p="30.00">
                <img data-u="image" src="<?php echo base_url('assets/plugins/corrasual/') ?>img/003.jpg" />
            </div>
            <div data-p="30.00">
                <a  class="viewImg"  href="<?php echo base_url('assets/plugins/corrasual/') ?>img/004.jpg"><img data-u="image" src="<?php echo base_url('assets/plugins/corrasual/') ?>img/004.jpg" /></a>
            </div>
            <div data-p="30.00">
                <img data-u="image" src="<?php echo base_url('assets/plugins/corrasual/') ?>img/009.jpg" />
            </div>
            <div data-p="30.00">
                <img data-u="image" src="<?php echo base_url('assets/plugins/corrasual/') ?>img/010.jpg" />
            </div>
            <div data-p="30.00">
                <img data-u="image" src="<?php echo base_url('assets/plugins/corrasual/') ?>img/019.jpg" />
            </div>
            <div data-p="30.00">
                <img data-u="image" src="<?php echo base_url('assets/plugins/corrasual/') ?>img/020.jpg" />
            </div>
            <div data-p="30.00">
                <img data-u="image" src="<?php echo base_url('assets/plugins/corrasual/') ?>img/021.jpg" />
            </div>
            <div data-p="30.00">
                <img data-u="image" src="<?php echo base_url('assets/plugins/corrasual/') ?>img/022.jpg" />
            </div>
            <div data-p="30.00">
                <img data-u="image" src="<?php echo base_url('assets/plugins/corrasual/') ?>img/024.jpg" />
            </div>
        </div>
    </div>

    <div id="caption-img" style="display:none;position: fixed;z-index: 99999;background: #000000a8;color:  white;padding:  10px;width:  100%;bottom: 0;height: 138px;">
        <div>adcadcadcadsc dcadcadscadscasdc adscadscadscasdcadsca asdcadscadscadscadscsdcasdc</div>
        <div style="position: absolute;bottom:  0;right:  0;margin: 20px 40px;">23:44</div>
    </div>
    <script type="text/javascript">jssor_1_slider_init();</script>



    
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/plugins/login/') ?>vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/plugins/login/') ?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/plugins/login/') ?>vendor/bootstrap/js/popper.js"></script>
    <script src="<?php echo base_url('assets/plugins/login/') ?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url('assets/plugins/fancy/dist/') ?>jquery.fancybox.min.js"></script>
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/plugins/login/') ?>vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/plugins/login/') ?>vendor/daterangepicker/moment.min.js"></script>
    <script src="<?php echo base_url('assets/plugins/login/') ?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/plugins/login/') ?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
    <script src="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url('assets/plugins/login/') ?>js/main.js"></script>

    <script src="<?php echo base_url('assets/'); ?>js/fungsi.js?<?php echo rand(111,999); ?>"></script>
    <script type="text/javascript">
        function login(){
            $('#logo-unira').fadeOut('slow');
            $('#form-login').fadeIn('slow');
        }
        $(document).ready(function() {
            $(".viewImg").fancybox({
                beforeShow : function(){
                    //alert('belom')
                },
                afterShow : function( instance, current ) {
                    $('#caption-img').fadeIn('fast');
                },
                afterClose : function(){
                    $('#caption-img').fadeOut('fast');
                }
            });
        });
    </script>
    <?php
    if(isset($auth) && $auth==true){
        ?>
        <script type="text/javascript">
            $(document).ready(function() {
                _alert('Kesalahan','<?php echo $msg; ?>','error');
                login();
            });
        </script>
        <?php
    }
    ?>
</body>
</html>