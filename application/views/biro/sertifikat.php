<style type="text/css">
</style>
<div class="row">
	<div class="col-12">
		<form class="form-inline" onsubmit="return generate(this)">
		  <div class="form-group mx-sm-3 mb-2">
		    <label for="inputPassword2" class="sr-only">Password</label>
		    <input autocomplete="off"  type="text" class="form-control" id="npm" name="npm" placeholder="NPM">
		  </div>
		  <button type="submit" class="btn btn-primary mb-2 mr-2">Generate</button>

					<a style="display: none;" href="#!" onclick="_cetak(this)" class="btn btn-warning mb-2 btnprint"><i class="fa fa-print"></i></a>
		</form>
	</div>
	<div class="dropdown-divider"></div>
	<div class="col-12 mt-3  xloading">
		<div class="sertifikat" style="display: none;
    padding: 19px;
    border: 1px solid #ded6d6;
    background: white;
    box-shadow: -7px -6px 15px #86868626;
">
			<div class="row">
				<div class="col-12">
					<div id="qrcode"></div>
				</div>
				<div class="col-12">
					<div class="kodeqr" style="
    font-size: 80%;
"></div>
				</div>
				<div class="col-12 text-muted" style="
    font-size: 80%;
">
					<div class="kodematch"></div>
				</div>
				<div class="col-12 text-center mt-md-0 mt-3">
					<h2><u>SERTIFIKAT</u></h2>
				</div>
				<div class="col-12 text-center">
					<div class="nomor text-primary"></div>
				</div>
				<div class="col-12 text-center mt-5">
					Diberikan kepada :
				</div>
				<div class="col-12 mt-2 text-center">
					<div class="nama font-weight-bold"></div>
				</div>
				<div class="col-12 text-center">
					<div class="npm font-weight-bold text-muted" style="
    font-size:  80%;
"></div>
				</div>
				<div class="col-12 mt-2 text-center">
					<div class="alert alert-info pelaksanaan"></div>
				</div>
				<div class="col-12 text-right mb-2">
					Pamekasan, <?php echo $this->mfungsi->tgl(date('Y-m-d')); ?>
				</div>
				<div class="col-md-4 text-center font-weight-bold">
					<div class="row">
						<div class="col-12 n-rektor">
							-
						</div>
						<div class="col-12 rektor mt-5">
							-
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center font-weight-bold">
					<div class="row">
						<div class="col-12 n-presma">
							-
						</div>
						<div class="col-12 presma mt-5">
							-
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center font-weight-bold">
					<div class="row">
						<div class="col-12 n-panitia">
							-
						</div>
						<div class="col-12 panitia mt-5">
							-
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var wqr = 0;
	var urlcetak = '<?php echo base_url('biro/sertifikat/cetak/') ?>';
	function generate(t){
		var npm = $('#npm').val();
		if(npm!=''){
			_ajax({
				url:'<?php echo base_url('biro/sertifikat/create') ?>',
				data:$(t).serialize(),
				loading:'.xloading',
				success:function(res){
					res = JSON.parse(res);
					if(res.status==true){
						var data = res.data;
						$('.sertifikat .nama').text(data.nama);
						$('.sertifikat .npm').text(data.npm);
						$('.sertifikat .kodeqr').text(data.qrcode);
						$('.sertifikat .kodematch').text(data.security_code);
						$('.sertifikat .nomor').text(data.nomor);
						$('.sertifikat .akademik').text(data.akademik);
						$('.sertifikat .dibuat').text(data.dibuat);
						$('.sertifikat .n-rektor').text(data.rektorLabel);
						$('.sertifikat .rektor').text(data.rektor);
						$('.sertifikat .n-presma').text(data.presmaLabel);
						$('.sertifikat .presma').text(data.presma);
						$('.sertifikat .n-panitia').text(data.panitiaLabel);
						$('.sertifikat .panitia').text(data.panitia);
						$('.sertifikat .pelaksanaan').html(data.pelaksanaan);
						createQr(data);
					}else{
						$('.btnprint,.sertifikat').hide();
						_alert('Kesalaha',res.data,'error');
					}
				}
			})
		}
		return false;
	}

	function createQr(data){
		var plaintext = '';
		plaintext += 'KODE SERTIFIKAT = '+data.qrcode+'\n';
		plaintext += 'NPM = '+data.npm+'\n';
		plaintext += 'NAMA = '+data.nama+'\n';
		plaintext += 'PRODI = '+data.prodi_nama+'\n';
		plaintext += 'DIBUAT = '+data.dibuat+'\n';
		plaintext += 'ANGKATAN = '+data.angkatan+'\n';
		// plaintext += 'KELOMPOK = '+data.NAMAKEL+'\n';
		// plaintext += 'TAHUN KKN = '+data.NAMATAHUN+'\n';
		// plaintext += 'ALAMAT KELOMPOK = '+data.ALAMATKEL+'\n';
		// plaintext += 'KAMPUS = UNIVERSITAS MADURA';

		$('#qrcode').empty();
		$('#qrcode').qrcode({
	    render: 'canvas',
	    minVersion: 1,
	    maxVersion: 40,
	    ecLevel: 'L',
	    left: 0,
	    top: 0,
	    size: 80,
	    fill: '#000',
	    background: '#fff',
	    text: plaintext,
	    radius: 0,
	    quiet: 0,
	    mode: 0,
	    mSize: 0.1,
	    mPosX: 0.5,
	    mPosY: 0.5,
	    label: 'no label',
	    fontname: 'sans',
	    fontcolor: '#000',
	    image: null
		});
		$('.btnprint,.sertifikat').show();
		$('.btnprint').attr('href', urlcetak+data.npm);
	}
</script>