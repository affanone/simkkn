<div class="card">
  <div class="card-body">
    <h4 class="card-title">Data Nilai Mahasiswa KKN</h4>
    <div class="row mb-3">
	    <div class="col offset-8 col-4">
	    	<a class="float-right btn btn-outline-primary" onclick="_cetak(this)" href="<?php echo base_url('dosen/nilai/cetak'); ?>"><i class="fa fa-print"></i>Cetak</a>
	    </div>
	  </div>
    <table class="table datatable table-bordered table-striped">
    	<thead>
    		<tr>
    			<th>#</th>
    			<th>NPM</th>
    			<th>NAMA</th>
    			<th>FAK / PRODI</th>
    			<th>NILAI</th>
    		</tr>
    	</thead>
    	<tbody>
    		<?php
    		$i = 1;
    		foreach ($data as $key => $value) {
    		?>
    			<tr>
	    			<td><?php echo $i++; ?></td>
	    			<td><?php echo $value->NPM; ?></td>
	    			<td><?php echo $value->NAMAMHS; ?></td>
	    			<td><?php echo $value->FAKPRODI.' / '.$value->NAMAPRODI; ?></td>
	    			<td id="nilai<?php echo $value->NPM; ?>"><?php echo $value->NILAIMHS; ?></td>
	    		</tr>
    		<?php
    		}
    		?>
    	</tbody>
    </table>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.datatable').DataTable({
			info:false,
			paging:false,
			scrollX:true
		})
	});
</script>