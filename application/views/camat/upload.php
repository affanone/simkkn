<div class="card">
  <div class="card-body">
    <h4 class="card-title">Data Laporan</h4>

    <table class="table datatable table-hover xloadfile">
    	<thead>
    		<tr class="text-muted">
                <th></th>
    			<th>File</th>
    			<th>Upload User</th>
    			<th>Size</th>
    			<th>Date</th>
    		</tr>
    	</thead>
    	<tbody>
    		
    	<?php
    	foreach ($data as $key => $value){ 
    		$ext = explode('.', $value->FILEUPL);
    		$ext = end($ext);
			echo '<tr><td width="60px"><img src="'.base_url('assets/images/icons/'.$ext.'.png').'" class="img-fluid rounded"></td>
    			<td><a '.(($ext=='pdf')? 'onclick="_cetak(this)"' : '' ).' href="'.base_url('camat/upload/view/').$value->KDUPL.'" class="btn-link">'.$value->FILEUPL.'</a>&nbsp;.&nbsp;<a href="'.base_url('camat/upload/download/').$value->KDUPL.'">Download</a></td>
    			<td><span data-toggle="tooltip" data-placement="bottom" title="'.$value->NPM.'">'.$value->NAMAMHS.'</span></td>
    			<td>'.$value->SIZEUPL.'Kb</td>
    			<td>'.$this->mfungsi->tgl($value->TGLUPL,true).'</td>
	    			</tr>';
    	}
    	?>
    	</tbody>
    </table>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.datatable').DataTable({
            scrollX:true,
            paging:false
        });
    });
	
</script>