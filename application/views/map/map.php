<!DOCTYPE html>
<html>
  <head>
    <!-- This stylesheet contains specific styles for displaying the map
         on this page. Replace it with your own styles as described in the
         documentation:
         https://developers.google.com/maps/documentation/javascript/tutorial -->
    <!-- <link rel="stylesheet" href="/maps/documentation/javascript/demos/demos.css"> -->
    <style>
      .controls {
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
        background-color: #fff;
        font-family: Roboto;
        font-size: 12px;
        font-weight: 300;
        padding: 0 8px 0px 10px;
        text-overflow: ellipsis;
        height: 29px;
      }
      #feature-search{
          margin-left: -8px;
          margin-top: 10px;
          display: none;
      }

      .controls:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }
      #my-button-find {
          height: 29px;
          border: none;
          background: white;
          box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
          color: #5b5b5b;
          width: 36px;
          cursor:pointer;
      }
      #my-button-find:active{
        background: #e4e1e1;
      }
    </style>
    <?php
      $key = $this->mfungsi->setting('api_key');
      $key = ($key==null) ? '' : '&key='.$key;
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap&language=id-ID<?php echo $key; ?>"
        async defer></script>
  </head>
  <body>
    <div id="feature-search">
      <input autocomplete="off"  id="pac-input" class="controls d-none zd-md-flex float-left" type="text" placeholder="Cari Alamat">
      <input autocomplete="off"  id="pac-input2" onkeypress="return x33(event)" class="controls d-flex zd-md-none float-left" type="text" placeholder="Cari Alamat">
      <button id="my-button-find" type="button" onclick="x34()" class="d-flex d-md-none text-center"><i style="margin:  auto;" class="fa fa-search"></i></button>
    </div>
      
    <div id="map" style="height: 450px;width: 100%;display: table;"></div>
    <script>
      var map = null;
      var markers = [];
      var marker;
      var geocoder; 
      var fiturSearch;
      var input;
      var searchBox;
      function x33(e){
        if (e.keyCode == 13) {
          x34();
          return false;
        }
      }
      function x34(){
        $('#pac-input2').blur();
        _ajax({
          url:'<?php echo base_url('map/cari') ?>',
          data:{
            alamat:$('#pac-input2').val()
          },
          loading:'#map',
          success:function(res){
            res=JSON.parse(res);
            if(res.status==true){
              map.setCenter({
                lat:res.lat,
                lng:res.lng
              });
              var mev = {
                stop:null,
                latLng:new google.maps.LatLng(res.lat,res.lng)
              }
              google.maps.event.trigger(map,'click',mev);
            }else{
              _alert('Opz..','"'+$('#pac-input2').val()+'" tidak ditemukan!','warning');
            }
          }
        })
      }
      function initMap() {
        var chicago = {lat: -7.031318, lng: 113.396589};
        //var indianapolis = {lat: 39.79, lng: -86.14};
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map'), {
          center: chicago,
          zoom: 11
        });

        var directionsDisplay = new google.maps.DirectionsRenderer({
          map: map
        });

        // Create the search box and link it to the UI element.
        fiturSearch = document.getElementById('feature-search');
        input = document.getElementById('pac-input');
        searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(fiturSearch);
        google.maps.event.addListenerOnce(map, 'tilesloaded', function(){
          $('#feature-search').css({'display':'block'});
        });

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.

        searchBox.addListener('places_changed', function() {
          $('#pac-input').blur();
          var places = searchBox.getPlaces();
          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
        // document.getElementById('my-button-find').onclick = function () {
        //     var e = jQuery.Event("keydown");
        //     e.which = 50; // # Some key code value
        //     console.log(e)
        //     $("#pac-input").trigger(e);
        // };


        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });
      }

      function placeMarker(location) {
          if(marker){ //on vérifie si le marqueur existe
              marker.setPosition(location); //on change sa position
          }else{
              marker = new google.maps.Marker({ //on créé le marqueur
                  position: location, 
                  map: map
              });
          }
          $('#kordx').val(location.lat());
          $('#kordy').val(location.lng());
          getAddress(location);
      }

      function getAddress(latLng) {
        geocoder.geocode( {'latLng': latLng},
        function(results, status) {
          var alamat = $('#map-google .modal-footer');
          if(status == google.maps.GeocoderStatus.OK) {
            if(results[0]) {
              _alamatValid = true;
              var alamatfix = results[0].formatted_address.replace('Unnamed Road, ','');
              alamat.html('<div class="row"><div class="col-md-7">'+alamatfix+'</div><div class=" col-md-5 text-muted">Tutup peta apabila alamat lokasi anda sudah benar!</div>');
              $('#alamat').val(alamatfix);
            }
            else {
              _alamatValid = false;
              alamat.html('Alamat Tidak Ditemukan');
              $('#kordx').val('');
              $('#kordx').val('');
              $('#alamat').val('');
            }
          }
          else {
            _alamatValid = false;
            alamat.text(status);
            $('#kordx').val('');
            $('#kordx').val('');
            $('#alamat').val('');
          }
        });
      }
    </script>
    <?php
      $key = $this->mfungsi->setting('api_key');
      $key = ($key==null) ? '' : '&key='.$key;
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap&language=id-ID<?php echo $key; ?>"
        async defer></script>
  </body>
</html>