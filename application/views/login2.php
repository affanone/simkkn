<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SIM-KKN UNIRA <?php echo $this->mfungsi->tahun()->label; ?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/simple-line-icons/css/simple-line-icons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth login-full-bg">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-dark text-left p-5">
              <h2 class="font-weight-bold text-center">SIM<span class="text-danger">KKN</span> UNIRA</h2>
              <h4 class="font-weight-light"></h4>
              <form class="pt-5" action="<?php echo base_url('login/masuk'); ?>" method="post">
                  <div class="form-group">
                    <!-- <label for="uname">Username</label> -->
                    <input autocomplete="off"  type="text" class="form-control" id="uname" name="uname"value="" placeholder="Username">
                    <i class="mdi mdi-account"></i>
                  </div>
                  <div class="form-group">
                    <!-- <label for="passwd">Password</label> -->
                    <input autocomplete="off"  type="password" class="form-control" id="passwd" name="passwd" value="" placeholder="Password">
                    <i class="mdi mdi-key"></i>
                  </div>
<!--                   <div class="form-group">
                    <div class="dropdown pull-12" >
                        <input autocomplete="off"  type="hidden" name="level">
                      <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" label="level">
                        Siapakah Anda?
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" onclick="_setValue('level','admin','Administrator')" href="#!">Administrator</a>
                        <a class="dropdown-item" onclick="_setValue('level','dpl','DPL')" href="#!">DPL</a>
                        <a class="dropdown-item" onclick="_setValue('level','mhs','Mahasiswa')" href="#!">Mahasiswa</a>
                      </div>
                    </div>
                  </div> -->
                  <div class="mt-5">
                    <button type="submit" class="btn btn-block btn-warning btn-lg font-weight-medium">Masuk</button>
                  </div>
                  <div class="mt-3 text-center">
                    <a href="<?php echo base_url('notlogin'); ?>" target="_blank" class="auth-link text-white">Tidak bisa masuk?</a>
                  </div>               
              </form>
              <div style="position:  absolute;font-size:  11px;margin-left:  -74px;margin-top:  28px;color: #85878a;width: 100%;text-align: right;">
                Dev. @LABORITY OF IT UNIRA - Version 1.2.1
              </div>
            </div>
          </div>
            <div class="w-100 text-center mt-3" style="font-size: 13px;">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright ©<?php echo date('Y'); ?> All Rights Reserved | <a href="https://lp3m.unira.ac.id" target="_blank">LPPM</a>. Universitas Madura Pamekasan.</span>
            </div>
        </div>
      </div>



      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->



  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="<?php echo base_url('assets/'); ?>node_modules/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/'); ?>node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url('assets/'); ?>node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- endinject -->
  <!-- inject:js -->

  <!-- plugins -->
  <script src="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.min.js"></script>

  <script src="<?php echo base_url('assets/'); ?>js/off-canvas.js"></script>
  <script src="<?php echo base_url('assets/'); ?>js/misc.js"></script>
  <script src="<?php echo base_url('assets/'); ?>js/fungsi.js?<?php echo rand(111,999); ?>"></script>
  <!-- endinject -->

<?php
if(isset($auth) && $auth==true){
    ?>
    <script type="text/javascript">
        $(document).ready(function() {
            _alert('Kesalahan','<?php echo $msg; ?>','error');
        });
    </script>
    <?php
}
?>

        
</body>
</html>
