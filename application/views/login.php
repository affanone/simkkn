<!DOCTYPE html>
<html>
<head>
    <title>SIM-KKN UNIRA <?php echo $this->mfungsi->tahun()->label; ?></title>
    <style type="text/css">
        .box {
   position: relative;
   top: 0;
   opacity: 1;
   float: left;
   padding: 60px 50px 40px 50px;
   width: 100%;
   background: #000000bf;
   border-radius: 10px;
   transform: scale(1);
   -webkit-transform: scale(1);
   -ms-transform: scale(1);
   z-index: 5;
}

.box.back {
   transform: scale(.95);
   -webkit-transform: scale(.95);
   -ms-transform: scale(.95);
   top: -20px;
   opacity: .8;
   z-index: -1;
}

.box:before {
   content: "";
   width: 100%;
   height: 30px;
   border-radius: 10px;
   position: absolute;
   top: -10px;
   background: rgba(255, 255, 255, .6);
   left: 0;
   transform: scale(.95);
   -webkit-transform: scale(.95);
   -ms-transform: scale(.95);
   z-index: -1;
}

.overbox .title {
   color: #fff;
}

.overbox .title:before {
   background: #fff;
}

.title {
   width: 100%;
   float: left;
   line-height: 46px;
   font-size: 34px;
   font-weight: 700;
   letter-spacing: 2px;
   color: #ED2553;
   position: relative;
}

.title:before {
   content: "";
   width: 5px;
   height: 100%;
   position: absolute;
   top: 0;
   left: -50px;
   background: #ED2553;
}

.input,
.input label,
.input input,
.input .spin,
.button,
.button button .button.login button i.fa,
.material-button .shape:before,
.material-button .shape:after,
.button.login button {
   transition: 300ms cubic-bezier(.4, 0, .2, 1);
   -webkit-transition: 300ms cubic-bezier(.4, 0, .2, 1);
   -ms-transition: 300ms cubic-bezier(.4, 0, .2, 1);
}

.material-button,
.alt-2,
.material-button .shape,
.alt-2 .shape,
.box {
   transition: 400ms cubic-bezier(.4, 0, .2, 1);
   -webkit-transition: 400ms cubic-bezier(.4, 0, .2, 1);
   -ms-transition: 400ms cubic-bezier(.4, 0, .2, 1);
}

.input,
.input label,
.input input,
.input .spin,
.button,
.button button {
   width: 100%;
   float: left;
}

.input,
.button {
   margin-top: 30px;
   height: 70px;
}

.input,
.input input,
.button,
.button button {
   position: relative;
}

.input input {
   height: 60px;
   top: 10px;
   border: none;
   background: transparent;
}

.input input,
.input label,
.button button {
   font-family: 'Roboto', sans-serif;
   font-size: 24px;
   color:rgba(255, 255, 255, 0.8);
   font-weight: 300;
}

.input:before,
.input .spin {
   width: 100%;
   height: 1px;
   position: absolute;
   bottom: 0;
   left: 0;
}

.input:before {
   content: "";
   background: rgba(0, 0, 0, 0.1);
   z-index: 3;
}

.input .spin {
   background: #ED2553;
   z-index: 4;
   width: 0;
}

.overbox .input .spin {
   background: rgba(255, 255, 255, 1);
}

.overbox .input:before {
   background: rgba(255, 255, 255, 0.5);
}

.input label {
   position: absolute;
   top: 10px;
   left: 0;
   z-index: 2;
   cursor: pointer;
   line-height: 60px;
}

.button.login {
   width: 60%;
   left: 20%;
}

.button.login button,
.button button {
    width: 100%;
    line-height: 64px;
    left: 0%;
    background-color: #ffffff00;
    border: 3px solid rgba(255, 255, 255, 0.1);
    font-weight: 900;
    font-size: 18px;
    color: rgba(255, 255, 255, 0.6);
}

.button.login {
   margin-top: 30px;
}

.button {
   margin-top: 20px;
}

.button button {
   background-color: #fff;
   color: #ED2553;
   border: none;
}

.button.login button.active {
   border: 3px solid transparent;
   color: #fff !important;
}

.button.login button.active span {
   opacity: 0;
   transform: scale(0);
   -webkit-transform: scale(0);
   -ms-transform: scale(0);
}

.button.login button.active i.fa {
   opacity: 1;
   transform: scale(1) rotate(-0deg);
   -webkit-transform: scale(1) rotate(-0deg);
   -ms-transform: scale(1) rotate(-0deg);
}

.button.login button i.fa {
   width: 100%;
   height: 100%;
   position: absolute;
   top: 0;
   left: 0;
   line-height: 60px;
   transform: scale(0) rotate(-45deg);
   -webkit-transform: scale(0) rotate(-45deg);
   -ms-transform: scale(0) rotate(-45deg);
}

.button.login button:hover {
   color: #ffffff;
   border-color: #ffffff;
}

.button {
   margin: 40px 0;
   overflow: hidden;
   z-index: 2;
}

.button button {
   cursor: pointer;
   position: relative;
   z-index: 2;
}

.pass-forgot {
   width: 100%;
   float: left;
   text-align: center;
   color: rgba(255, 255, 255, 0.4);
   font-size: 18px;
}

.click-efect {
   position: absolute;
   top: 0;
   left: 0;
   background: #ED2553;
   border-radius: 50%;
}

.overbox {
   width: 100%;
   height: 100%;
   position: absolute;
   top: 0;
   left: 0;
   overflow: inherit;
   border-radius: 10px;
   padding: 60px 50px 40px 50px;
}

.overbox .title,
.overbox .button,
.overbox .input {
   z-index: 111;
   position: relative;
   color: #fff !important;
   display: none;
}

.overbox .title {
   width: 80%;
}

.overbox .input {
   margin-top: 20px;
}

.overbox .input input,
.overbox .input label {
   color: #fff;
}

.overbox .material-button,
.overbox .material-button .shape,
.overbox .alt-2,
.overbox .alt-2 .shape {
   display: block;
}

.material-button,
.alt-2 {
   width: 140px;
   height: 140px;
   border-radius: 50%;
   background: #ED2553;
   position: absolute;
   top: 40px;
   right: -70px;
   cursor: pointer;
   z-index: 100;
   transform: translate(0%, 0%);
   -webkit-transform: translate(0%, 0%);
   -ms-transform: translate(0%, 0%);
}

.material-button .shape,
.alt-2 .shape {
   position: absolute;
   top: 0;
   right: 0;
   width: 100%;
   height: 100%;
}

.material-button .shape:before,
.alt-2 .shape:before,
.material-button .shape:after,
.alt-2 .shape:after {
   content: "";
   background: #fff;
   position: absolute;
   top: 50%;
   left: 50%;
   transform: translate(-50%, -50%) rotate(360deg);
   -webkit-transform: translate(-50%, -50%) rotate(360deg);
   -ms-transform: translate(-50%, -50%) rotate(360deg);
}

.material-button .shape:before,
.alt-2 .shape:before {
   width: 25px;
   height: 4px;
}

.material-button .shape:after,
.alt-2 .shape:after {
   height: 25px;
   width: 4px;
}

.material-button.active,
.alt-2.active {
   top: 50%;
   right: 50%;
   transform: translate(50%, -50%) rotate(0deg);
   -webkit-transform: translate(50%, -50%) rotate(0deg);
   -ms-transform: translate(50%, -50%) rotate(0deg);
}

body {
   /*background-image: url(<?php echo base_url('assets/images/bg.jpg'); ?>);*/
   background: white;
   background-position: center;
   background-size: cover;
   background-repeat: no-repeat;
   min-height: 100vh;
   font-family: 'Roboto', sans-serif;
}

body,
html {
   overflow: hidden;
}

.materialContainer {
   width: 100%;
   max-width: 460px;
   position: absolute;
   top: 50%;
   left: 50%;
   transform: translate(-50%, -50%);
   -webkit-transform: translate(-50%, -50%);
   -ms-transform: translate(-50%, -50%);
}

*,
*:after,
*::before {
   -webkit-box-sizing: border-box;
   -moz-box-sizing: border-box;
   box-sizing: border-box;
   margin: 0;
   padding: 0;
   text-decoration: none;
   list-style-type: none;
   outline: none;
}


.box-hasil {
    background: white;
    z-index: 999;
    box-shadow: -1px -1px 2px #00000069;
    padding-top: 30px;
}

.box-hasil .list {
    margin-bottom: 10px;
    border-bottom: dashed 1px #cacaca;
    padding-bottom: 4px;
}

.box-hasil .list .label {
    font-size: 8pt;
    margin-left: 15px;
    color: #000000;
    font-weight: 300;
    margin-bottom: 4px;
}

.box-hasil .list .item {
    margin-left: 15px;
    font-size: 18px;
    color: black;
    font-weight: bold;
}
.box-hasil .foto img {
    width: 100%;
}

.box-hasil .foto {
    overflow: hidden;
    width: 155px;
    height: 168px;
    margin-left: 15px;
    box-shadow: 2px 2px 3px #000000d1;
}
    </style>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900&subset=latin,latin-ext">
    <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
    <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.css">
</head>
<body>
<div class="materialContainer">


   <form class="box" action="<?php echo base_url('login/masuk'); ?>" method="post">

      <div class="title">LOGIN</div>

      <div class="input">
         <label for="name">Username</label>
         <input autocomplete="off"  type="text" id="name" name="uname" required="" value="">
         <span class="spin"></span>
      </div>

      <div class="input">
         <label for="pass">Password</label>
         <input autocomplete="off"  type="password" id="pass" name="passwd" required="" value="">
         <span class="spin"></span>
      </div>

      <div class="button login">
         <button type="submit"><span>Masuk</span> <i class="fa fa-refresh"></i></button>
      </div>

      <div style="color:  white;text-align:  center;width:  100%;position:  absolute;left:  0;bottom: 70px;
"><span>SIM</span><span style="color: #ed2553;margin-right: 5px;">KKN</span><span>UNIRA</span> | LPPM</div>
      <a href="<?php echo base_url('notlogin'); ?>" target="_blank" class="pass-forgot">Tidak bisa masuk?</a>

   </form>

   <div class="overbox">
      <div class="material-button alt-2" ><div style="display:  table;height:  100%;width:  100%;
    text-align:  center;"><div style="display:  table-cell;vertical-align: middle;"><img id="logo-unira" style="width:  50%;" src="<?php echo base_url('assets/images/logo.png'); ?>"></div></div></div>

      <div class="title">E-Sertifikat <a href="#!" style="color:  white;text-decoration:  none;position:  absolute;top: -50px;right: -100px;"><i class="fa fa-close btn-close-sert"></i></a></div>

      <div class="input">
         <label for="regname">Kode Sertifikat</label>
         <input autocomplete="off"  type="text" name="regname" id="regname">
         <span class="spin"></span>

           <div class="box-hasil" style="display: none;">
            <a href="#!" class=" btn-close-info-sert" onclick="tutupInfoSert(this)" style="color: red;position:  absolute;right: 10px;top: 5px;font-size:  28px;"><i class="fa fa-close"></i></a>
              <div class="list" style="margin-top: 20%;">
                  <div class="label">
                      KODE SERTIFIKAT
                  </div>
                  <div class="item kode">
                      &nbsp;
                  </div>
              </div>

              <div class="list">
                  <div class="label">
                      NOMOR POKOK PESERTA
                  </div>
                  <div class="item pokok">
                      &nbsp;
                  </div>
              </div>

              <div class="list">
                  <div class="label">
                      NAMA PESERTA
                  </div>
                  <div class="item nama">
                      &nbsp;
                  </div>
              </div>

              <div class="list">
                  <div class="label">
                      JENIS SERTIFIKAT
                  </div>
                  <div class="item sertifikat">
                      &nbsp;
                  </div>
              </div>

           </div>

      </div>

      <div class="button cek-restifikat">
         <button onclick="ceksertifikat(this)">CEK KODE</button>
      </div>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.min.js"></script>

<?php
if(isset($auth) && $auth==true){
    ?>
    <script type="text/javascript">
        $(document).ready(function() {
            swal('Kesalahan','<?php echo $msg; ?>','error');
        });
    </script>
    <?php
}
?>


<script type="text/javascript">

    function tutupInfoSert(t){
        $('.box-hasil').fadeOut(400);
        setTimeout(function(){
            $('.cek-restifikat').fadeIn(400);
        },400)
        
    }

    function ceksertifikat(t){
        var overbox = $('.overbox');
        var kode = $('#regname').val();
        if(kode!=''){
            $.ajax({
                url:'<?php echo base_url('login/sertifikat') ?>',
                type:'post',
                data:{
                    kode:kode
                },
                beforeSend:function(){
                    $(t).text('Tunggu..');
                },
                success:function(res){
                  res = JSON.parse(res);
                  if(res.status==true){
                    $('.box-hasil .kode').text(res.data.kode);
                    $('.box-hasil .pokok').text(res.data.induk);
                    $('.box-hasil .nama').text(res.data.nama);
                    $('.box-hasil .sertifikat').text(res.data.jenis);
                    $('.cek-restifikat').fadeOut(400);
                    setTimeout(function(){
                        $('.box-hasil').height(overbox.height()+63);
                        $('.box-hasil').width(overbox.width()+92);
                        var pos = overbox.position();
                        $(".box-hasil").css({top: pos.top-122, left: pos.left-46, position:'absolute'});
                        $('.box-hasil').fadeIn(400);
                    },400);
                  }else{
                    swal('Info',res.data);
                  }
                  $(t).text('CEK KODE');
                }
            })
        }
    }

    $(function() {

   $(".input input").focus(function() {

      $(this).parent(".input").each(function() {
         $("label", this).css({
            "line-height": "18px",
            "font-size": "18px",
            "font-weight": "100",
            "top": "0px"
         })
         $(".spin", this).css({
            "width": "100%"
         })
      });
   }).blur(function() {
      $(".spin").css({
         "width": "0px"
      })
      if ($(this).val() == "") {
         $(this).parent(".input").each(function() {
            $("label", this).css({
               "line-height": "60px",
               "font-size": "24px",
               "font-weight": "300",
               "top": "10px"
            })
         });

      }
   });

   $(".button").click(function(e) {
      var pX = e.pageX,
         pY = e.pageY,
         oX = parseInt($(this).offset().left),
         oY = parseInt($(this).offset().top);

      $(this).append('<span class="click-efect x-' + oX + ' y-' + oY + '" style="margin-left:' + (pX - oX) + 'px;margin-top:' + (pY - oY) + 'px;"></span>')
      $('.x-' + oX + '.y-' + oY + '').animate({
         "width": "500px",
         "height": "500px",
         "top": "-250px",
         "left": "-250px",

      }, 600);
      $("button", this).addClass('active');
   })

   $('.btn-close-sert').click(function(event) {
        $(".alt-2").trigger('click');
   });

   $(".alt-2").click(function() {
      if (!$(this).hasClass('material-button')) {
         $(".shape").css({
            "width": "100%",
            "height": "100%",
            "transform": "rotate(0deg)"
         })

         setTimeout(function() {
            $(".overbox").css({
               "overflow": "initial"
            })
         }, 600)

         $(this).animate({
            "width": "140px",
            "height": "140px"
         }, 500, function() {
            $(".box").removeClass("back");
            $('.box-hasil').hide(400);
            $('#logo-unira').fadeTo('slow', 1);
            $(this).removeClass('active')
         });

         $(".overbox .title").fadeOut(300);
         $(".overbox .input").fadeOut(300);
         $(".overbox .button").fadeOut(300);

         $(".alt-2").addClass('material-buton');
      }

   })

   $(".material-button").click(function() {

      if ($(this).hasClass('material-button')) {
         setTimeout(function() {
            $(".overbox").css({
               "overflow": "hidden"
            })
            $(".box").addClass("back");
         }, 200)
         $(this).addClass('active').animate({
            "width": "700px",
            "height": "700px"
         });

         setTimeout(function() {
            $(".shape").css({
               "width": "50%",
               "height": "50%",
               "transform": "rotate(45deg)"
            })
            $('#logo-unira').fadeTo('slow', 0.1);
            $(".overbox .title").fadeIn(300);
            $(".overbox .input").fadeIn(300);
            $(".overbox .button").fadeIn(300);
         }, 700)

         $(this).removeClass('material-button');

      }

      if ($(".alt-2").hasClass('material-buton')) {
         $(".alt-2").removeClass('material-buton');
         $(".alt-2").addClass('material-button');
      }

   });

});

</script>
   </div>
</body>
</html>