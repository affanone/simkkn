<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<script src="<?php echo base_url('assets/'); ?>node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">


	var data = <?php echo json_encode($data); ?>;
// upload ajax

function _(el) {
  return document.getElementById(el);
}

var prgs235 = null;
var err235 = null;
var aborts235 = null;
var cmpl235 = null;

function _upload(a = {}) {
  prgs235 = (a.progress !=undefined) ? a.progress : null;
  cmpl235 = (a.complete !=undefined) ? a.complete : null;
  err235 = (a.error !=undefined) ? a.error : null;
  abort235 = (a.abort !=undefined) ? a.abort : null;
  var formdata = new FormData();
  if(typeof(a.name)=='object'){
    for (var inx in a.name) {
        var name =  null;
        var val = null;
        var dxf = null;
        if ($.isNumeric(inx)==true){
            dxf = _((a.name[inx] !=undefined) ? a.name[inx] : 'formdata');
            name = a.name[inx];
            val = dxf.value;
            if(dxf.files==null){
                formdata.append((name !=undefined) ? name : 'dataname', val);
            }
            else{
                formdata.append((name !=undefined) ? name : 'fileUpload', dxf.files[0]);
            }
        }else{
            formdata.append((inx !=undefined) ? inx : 'dataname', a.name[inx]);
        }
        
    }
  }else{
    var data = _((a.name !=undefined) ? a.name : 'fileUpload');
    if(data.files==null)
        formdata.append((a.name !=undefined) ? a.name : 'dataname', data.value);
    else
        formdata.append((a.name !=undefined) ? a.name : 'fileUpload', data.files[0]);
  }
  var ajax = new XMLHttpRequest();
  ajax.upload.addEventListener("progress", progressHandler, false);
  ajax.addEventListener("load", completeHandler, false);
  ajax.addEventListener("error", errorHandler, false);
  ajax.addEventListener("abort", abortHandler, false);
  ajax.open("POST", (a.action !=undefined) ? a.action : 'upload_parse.php');
  ajax.send(formdata);
}

function progressHandler(event) {
  var percent = (event.loaded / event.total) * 100;
  if(typeof(prgs235)=='function')
    prgs235({
        'percent':Math.round(percent),
        'loaded':event.loaded,
        'total':event.total
    });
}

function completeHandler(event) {
    if(typeof(cmpl235)=='function')
        cmpl235(event.target.responseText);
}

function errorHandler(event) {
    if(typeof(err235)=='function')
        err235(false);
}

function abortHandler(event) {
   if(typeof(abort235)=='function')
        abort235(false);
}

// end upload ajax
var batas = 0;
function aaaa(){
	_upload({
		action:'<?php echo base_url('tes/b') ?>',
		name:{
			path:data[batas].path+'/'+data[batas].file,
      kode:data[batas].id
		},
		progress:function(res){
			batas++;
		},
		complete:function(res){
			console.log(res)
			if(batas<data.length){
				aaaa();
      }
		}
	})
}
jQuery(document).ready(function($) {
	aaaa();
});
</script>
</body>
</html>