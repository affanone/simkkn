<div class="row">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
		  		<div class="row justify-content-md-center">
				    <div class="col col-md-5">
                  <h4 class="card-title">
                  	<?php
                  	if($tipe=='tambah')
                  		echo 'Tambah Camat Baru';
                  	else if($tipe=='edit')
                  		echo 'Edit Camat';
                  	?>
                  </h4>
                  <p class="card-description">
                    Yang bertanda (*) harus diisi!
                  </p>
                  <?php
                  	if($tipe=='tambah')
                  		$act = base_url('admin/pengguna/camat/simpan');
                  	else if($tipe=='edit')
                  		$act = base_url('admin/pengguna/camat/update/'.$kode);
                  ?>

                  <form class="forms-sample" action="<?php echo $act; ?>" method="post">

                    <div class="form-group">
                      <label for="usern">* Username <?php echo form_error('username','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="text" class="form-control" id="usern" value="<?php echo $username; ?>"  placeholder="Username" name="username">
                    </div>

                    <div class="form-group">
                      <label for="nama">* Nama Lengkap <?php echo form_error('nama','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="text" class="form-control" id="nama" value="<?php echo $nama; ?>"  placeholder="Nama Lengkap" name="nama">
                    </div>
                    <?php
                    if($tipe=='edit'){
                    ?>
                    <div class="alert alert-warning" role="alert">
                      Password diisi apabila anda ingin mengubahnya
                    </div>
                    <?php
                    }
                    ?>
                    <div class="form-group">
                      <label for="pass">* Password <?php echo form_error('pass','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="password" class="form-control" id="pass" value="<?php echo $pass; ?>"  placeholder="Password" name="pass">
                    </div>

                    <div class="form-group">
                      <label for="rpass">* Ulangi password <?php echo form_error('rpass','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="password" class="form-control" id="rpass" value="<?php echo $rpass; ?>"  placeholder="Ulangi Password" name="rpass">
                    </div>
                    
                    <button type="submit" class="btn btn-success mr-2">Simpan</button>
                    <a class="btn btn-light" href="<?php echo $kembali; ?>">Kembali</a>
                  </form>
				    </div>
				  </div>
		  	</div>
		</div>
	</div>
</div>