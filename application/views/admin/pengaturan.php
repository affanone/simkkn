<div class="row">
	<div class="col">
		<div class="card">
	  	<div class="card-body">
	  		<div class="row justify-content-center">
	  			<div class="col-md-6">
	  				<form action="<?php echo base_url('admin/pengaturan/update') ?>" method="post" id="settingForm">
						  <div class="form-group" >
						    <label for="apikey" class="col-md-12">Google Api Key</label>
						    <input autocomplete="off"  type="text" class="form-control" id="apikey" aria-describedby="keyhelp" name="apikey" value="<?php echo $key; ?>" placeholder="API KEY GOOGLE">
						    <small id="keyhelp" class="form-text text-muted">Dibutuhkan untuk layanan google api sebagai pencarian nama lokasi</small>
						    <a class="float-right" href="https://developers.google.com/maps/documentation/geocoding/get-api-key?hl=id" target="_blank">Dapatkan key API Google</a>
						    <br>
						  </div>

						  <div class="form-group"  id="formPengaturan">

						  	<div class="alert alert-warning" role="alert">
								  Untuk mengecek layanan google geocoding sebagai pencarian nama alamat, cek disini <b>Key Api</b> apakah merespon dengan baik?
									<input autocomplete="off"  type="text" class="form-control" id="alamatapikey" aria-describedby="keyaddress" placeholder="Cari nama alamat">
									<small id="alamatapikey" class="form-text text-muted">Gunakan nama alamat yang mencakupi nama desa, kecamatan dan kabupaten agar pencarian lebih terakurasi</small>
							    <button type="button" onclick="cekApi()" class="btn btn-outline-primary" >CEK</button>
								</div>
						  </div>
						  <div class="dropdown-divider"></div>
						  <div class="form-group">
						  	<div class="alert alert-warning" role="alert">
								  <b>Pengaturan untuk Sertifikat KKN</b><br><br>
								  <label>Pimpinan LPPM</label>
									<input autocomplete="off" name="pimpinan" value="<?php echo $pimpinan; ?>"  type="text" class="form-control" placeholder="Pimpinan LPPM">
									<small class="form-text text-muted">Untuk menambahkan nomor pokok seperti NIP maka inputkan <strong>NAMA_PIMPINAN|NIP: 333222111</strong> yang dipisah dengan tanda <strong>|</strong></small>
									<br>
									<label>Pola Nomor Sertifikat</label>
									<input autocomplete="off" name="nosertifikat" value="<?php echo $nosertifikat; ?>"  type="text" class="form-control" placeholder="Pola nomor sertifikat">
									<small class="form-text text-muted">jangan membuang "%nomor%" dan "%tahun%". kode tersebut akan diisi secara otomatis</small>
									<br>
									<label>Tanda Tangan Sertifikat</label>
									<img class="img-thumbnail w-100" id="view-ttd" src="<?php echo base_url('assets/images/signature.png?'.rand(111,999)) ?>">
									<div id="progress4" style="
									    width:  100%;
									    margin-top: 7px;
									    display:  none;
									    text-align: center;
									">
										0%
									</div>
									<label class="btn-link mt-2" id="btn-ganti-ttd">
								    <span id="gantifoto">Ganti tanda tangan (*.png, < 500Kb)</span><input autocomplete="off"  type="file" onchange="gantittd(this)" name="file" id="fotottd" hidden accept="image/png">
									</label>
									<script type="text/javascript">
										function gantittd(t){
											$('#btn-ganti-ttd').hide('fast');
											$('#progress4').text('0%');
											$('#progress4').show('fast');
											_upload({
												progress:function(res){
													$('#progress4').text(res.percent+'%');
												},
												complete:function(res){
													$('#btn-ganti-ttd').show('fast');
													$('#progress4').hide('fast');
													res = JSON.parse(res);
													if(res.status==false){
														_alert('Kesalahan',res.data,'error');
													}else{
														$('#view-ttd').attr('src', res.foto);
													}
												},
												name:'fotottd',
												action:'<?php echo base_url('admin/pengaturan/ganti_ttd') ?>'
											})
										}
									</script>
								</div>
						  </div>
						  <div class="dropdown-divider"></div>
						  <div class="form-group" >
						    <label for="apikey" class="col-md-12">Model Pembentukan KKN</label>
	      				<div id="accordion" role="tablist">
							    <div class="card">
								    <div class="card-header" role="tab" id="headingOne">
								      <h5 class="mb-0">
								        <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								          MODEL A
								        </a>
								        <span class="float-right model_bentuk" id="model_a">
								        <?php
								        if($model=='a')
								        	echo '<i class="fa fa-check"></i>';
								        ?>
								        </span>
								      </h5>
								    </div>

								    <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
								      <div class="card-body">
								        <ul>
								        	<li>Program studi akan dibagi secara merata pada setiap kelompok</li>
								        	<li>Kapasitas dibentuk berdasarkan jumlah prodi prodi yang tercipta pada masing-masing kelompok</li>
								        	<li>Semua nilai jarak antara mahasiswa ke kelompok kkn diambil berdasarkan mahasiswa prodi yang telah tercipta di kapasitas kelompok kkn, selanjutnya dijumlahkan dan diambil berdasarkan hasil terkecilnya</li>
								        	<li>Tidak pasti semua mahasiswa akan memperoleh jarak terdekat</li>
								        </ul>
								        <button id="btn-model-a" type="button" onclick="setModel('a')" class="btn btn-primary btn-block">Gunakan Model Ini</button>
								      </div>
								    </div>
								  </div>

								  <div class="card">
								    <div class="card-header" role="tab" id="headingTwo">
								      <h5 class="mb-0">
								        <a data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								          MODEL B 
								        </a>
								        <span class="float-right model_bentuk" id="model_b">
								        <?php
								        if($model=='b')
								        	echo '<i class="fa fa-check"></i>';
								        ?>
								        </span>
								      </h5>
								    </div>

								    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
								      <div class="card-body">
								        <ul>
								        	<li>Kelompok kkn mencari mahasiswa yang terdekat</li>
								        	<li>Tidak dibatasi jumlah prodi pada masing-masing kelompok kkn</li>
								        	<li>Rata-rata jarak mahasiswa ke kelompok kkn dibuat sama atau mendekati sama</li>
								        </ul>
								        <button id="btn-model-b" type="button" onclick="setModel('b')" class="btn btn-primary btn-block">Gunakan Model Ini</button>
								      </div>
								    </div>
								  </div>

								  <div class="card">
								    <div class="card-header" role="tab" id="headingThree">
								      <h5 class="mb-0">
								        <a data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
								          MODEL C
								        </a>
								        <span class="float-right model_bentuk" id="model_c">
								        <?php
								        if($model=='c')
								        	echo '<i class="fa fa-check"></i>';
								        ?>
								        </span>
								      </h5>
								    </div>

								    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
								      <div class="card-body">
								        <ul>
								        	<li>Mencari rata-rata jarak mahasiswa ke kelompok kkn pada masing-masing mahasiswa</li>
								        	<li>Sorting jarak mahasiswa terkecil dari hasil rata-rata</li>
								        	<li>Membatasi jumlah prodi per kelompok kkn</li>
								        	<li>Batas Prodi <input autocomplete="off"  class="form-control w-25 text-center" value="<?php echo $btsprodi; ?>" type="text" id="minimalprodi"></li>
								        </ul>
								        <button id="btn-model-c" type="button" onclick="setModel('c')" class="btn btn-primary btn-block">Gunakan Model Ini</button>
								      </div>
								    </div>
								  </div>

								  <div class="card">
								    <div class="card-header" role="tab" id="headingFor">
								      <h5 class="mb-0">
								        <a data-toggle="collapse" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">
								          MODEL D
								        </a>
								        <span class="float-right model_bentuk" id="model_d">
								        <?php
								        if($model=='d')
								        	echo '<i class="fa fa-check"></i>';
								        ?>
								        </span>
								      </h5>
								    </div>

								    <div id="collapseFor" class="collapse" role="tabpanel" aria-labelledby="headingFor" data-parent="#accordion">
								      <div class="card-body">
								        <ul>
								        	<li>Mencari rata-rata jarak mahasiswa ke kelompok kkn pada masing-masing mahasiswa</li>
								        	<li>Sorting jarak mahasiswa terkecil dari hasil rata-rata</li>
								        	<li>Tidak memperhatikan jumlah prodi pada masing-masing kelompok KKN</li>
								        </ul>
								        <button id="btn-model-d" type="button" onclick="setModel('d')" class="btn btn-primary btn-block">Gunakan Model Ini</button>
								      </div>
								    </div>
								  </div>

								  <div class="card">
								    <div class="card-header" role="tab" id="headingFive">
								      <h5 class="mb-0">
								        <a data-toggle="collapse" href="#collapseFive" aria-expanded="false" aria-controls="collapseFor">
								          MODEL E
								        </a>
								        <span class="float-right model_bentuk" id="model_e">
								        <?php
								        if($model=='e')
								        	echo '<i class="fa fa-check"></i>';
								        ?>
								        </span>
								      </h5>
								    </div>

								    <div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingFive" data-parent="#accordion">
								      <div class="card-body">
								        <ul>
								        	<li>Proses dilakukan dari mahasiswa Terjauh terlebih dahulu.</li>
								        	<li>:)</li>
								        </ul>
								        <button id="btn-model-e" type="button" onclick="setModel('e')" class="btn btn-primary btn-block">Gunakan Model Ini</button>
								      </div>
								    </div>
								  </div>
								</div>
						  </div>
						  <div class="dropdown-divider"></div>
						  <div class="form-group" >
						    <label class="col-md-12">Informasi Kabupaten Asal</label>
						    <input autocomplete="off"  type="text" class="form-control" id="kabupaten" aria-describedby="kab" name="kabupaten" value="<?php echo $kabupaten; ?>" placeholder="Kabupaten Asal">
						    <small id="kab" class="form-text text-muted">Digunakan sebagai informasi asal kabupaten mahasiswa pada saat pembentukan KKN, gunakan pemisah "|" apabilah nama kabupaten lebih dari 1. ex : sampang|pamekasan</small>
						  </div>
						  <div class="dropdown-divider"></div>
						  <div class="form-group" >
						    <div class="alert alert-warning" role="alert">
								  Memperbolehkan calon peserta kkn untuk mengubah biodata setelah pendaftaran kkn tervalidasi
									<div class="form-check form-check-flat">
	                  <label class="form-check-label">
	                    <input autocomplete="off"  type="checkbox" class="form-check-input" name="editbiodata" <?php echo ($biodata==1)? 'checked' : ''; ?>>
	                    Perbolehkan edit biodata
	                  <i class="input-helper"></i></label>
	                </div>
								</div>
						  </div>
						  <div class="dropdown-divider"></div>
						  <div class="form-group" >
						    <div class="alert alert-warning" role="alert">
								  Mengunci beberapa halaman
								  <div class="form-check form-check-flat">
	                  <label class="form-check-label">
	                    <input autocomplete="off"  type="checkbox" class="form-check-input" name="halaman_uploadp" <?php echo ($halaman['uploadp']==1)? 'checked' : ''; ?>>
	                    Halaman Fitur Upload Laporan/Proposal
	                  <i class="input-helper"></i></label>
	                </div>
	                <div class="dropdown-divider"></div>
	                <div class="form-check form-check-flat">
	                  <label class="form-check-label">
	                    <input autocomplete="off"  type="checkbox" class="form-check-input" name="halaman_uploadd" <?php echo ($halaman['uploadd']==1)? 'checked' : ''; ?>>
	                    Halaman Fitur Upload Lembar Monitoring
	                  <i class="input-helper"></i></label>
	                </div>
	                <div class="dropdown-divider"></div>
								  <div class="form-check form-check-flat">
	                  <label class="form-check-label">
	                    <input autocomplete="off"  type="checkbox" class="form-check-input" name="halaman_camat" <?php echo ($halaman['camat']==1)? 'checked' : ''; ?>>
	                    Halaman Camat
	                  <i class="input-helper"></i></label>
	                </div>
	                <div class="dropdown-divider"></div>
	                <div class="form-check form-check-flat">
	                  <label class="form-check-label">
	                    <input autocomplete="off"  type="checkbox" class="form-check-input" name="halaman_biro" <?php echo ($halaman['biro']==1)? 'checked' : ''; ?>>
	                    Halaman Biro
	                  <i class="input-helper"></i></label>
	                </div>
	                <div class="dropdown-divider"></div>
									<div class="form-check form-check-flat">
	                  <label class="form-check-label">
	                    <input autocomplete="off"  type="checkbox" class="form-check-input" name="halaman_dpl" <?php echo ($halaman['dpl']==1)? 'checked' : ''; ?>>
	                    Halaman DPL
	                  <i class="input-helper"></i></label>
	                </div>
	                <div class="dropdown-divider"></div>
	                <div class="form-check form-check-flat">
	                  <label class="form-check-label">
	                    <input autocomplete="off"  onchange="pesertaKKN(this)" type="checkbox" class="form-check-input" name="halaman_peserta" <?php echo ($halaman['peserta']==1)? 'checked' : ''; ?>>
	                    Halaman Peserta KKN
	                  <i class="input-helper"></i></label>
	                </div>
	                <div style="display: <?php echo ($halaman['peserta']==1)? 'none' : 'block'; ?>;" class="form-check ml-3 form-check-flat" id="halaman_peserta_kkn">
	                  <label class="form-check-label mb-2">
	                    <input autocomplete="off"  onchange="skedulKKN(this)" type="checkbox" class="form-check-input" name="halaman_peserta_kelompok" <?php echo ($halaman['peserta_kelompok']==1)? 'checked' : ''; ?>>
	                    Profil KKN
	                  <i class="input-helper"></i></label>

	                  <div style="display: <?php echo ($halaman['peserta_kelompok']!=1)? 'none' : 'block'; ?>;" class="alert alert-info" role="alert" id="skedul_kkn">
		                	Tanggal skedul pembukaan informasi KKN
			                <input autocomplete="off"  type="text" class="w-md-25 form-control" id="tgl_skedul" name="tgl_skedul" placeholder="Tgl pembukaan">
			                <div class="form-check form-check-flat">
			                  <label class="form-check-label">
			                    <input autocomplete="off"  type="checkbox" class="form-check-input" name="auto_skedul" <?php echo ($halaman['auto_skedul']==1)? 'checked' : ''; ?>>
			                    Buka Otomatis
			                  <i class="input-helper"></i></label>
			                </div>
		                </div>

	                </div>

	                <div class="dropdown-divider"></div>

	                <div class="form-check form-check-flat">
	                  <label class="form-check-label">
	                    <input autocomplete="off"  type="checkbox" class="form-check-input" name="halaman_daftar" <?php echo ($halaman['daftar']==1)? 'checked' : ''; ?>>
	                    Halaman Pendaftaran KKN
	                  <i class="input-helper"></i></label>
	                </div>
								</div>
						  </div>
						  <div class="dropdown-divider"></div>

						  <div class="form-group" >
						    <label for="apikey" class="col-md-12">Cadangkan dan Pulihkan</label>
						  <a href="<?php echo base_url('admin/pengaturan/backup') ?>" class="btn btn-outline-primary btn-block">Cadangkan Data</a>
						  <form action="" class="x334" method="post" enctype="multipart/form-data">
		        		<label class="btn btn-outline-warning btn-block">
								    <span id="namafile">Pulihkan Data</span><input autocomplete="off"  type="file" onchange="konfirm(this,'restore')" name="file" id="filesql" hidden accept="application/x-zip-compressed">
								</label>
								<!-- <div class="form-group">
	                <div class="form-check form-check-flat">
	                  <label class="form-check-label">
	                    <input autocomplete="off"  type="checkbox" class="form-check-input" value="1" id="vrfUpload">
	                    Auto Verifikasi
	                  <i class="input-helper"></i></label>
	                </div>
	              </div> -->
							</form>
							<a href="<?php echo base_url('admin/pengaturan/renew') ?>" onclick="return konfirm(this,'reset')" class="btn btn-outline-danger btn-block">Reset sistem</a>
						  </div>

						  <div class="dropdown-divider"></div>
						  <button type="submit" onclick="simpanSetting()" class="btn btn-primary">Simpan</button>
						</form>
	  			</div>
	  		</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="konfirmasiproses">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<form action="<?php echo base_url('admin/pengaturan/cekpass'); ?>" onsubmit="return proseskonfirm(this)" method="post">
      	<div class="modal-header">
	        <h5 class="modal-title">Password diminta!</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div class="alert alert-warning" role="alert">
					  Proses ini membutuhkan password yang anda gunakan pada saat masuk ke sistem ini.
					</div>
					  <div class="form-group">
					  	<input autocomplete="off"  type="hidden" id="konfirmTipe">
					    <input autocomplete="off"  type="password" name="password" class="form-control" id="passwordkonfirm" aria-describedby="passHelp" placeholder="Masukkan Password">
					    <small id="passHelp" class="form-text text-muted">Demi keamanan dari kesalahan pengguna</small>
					  </div>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" class="btn btn-primary">Ok</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	      </div>
	    </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#tgl_skedul').daterangepicker({
		    "singleDatePicker": true,
		    "startDate": "<?php echo $halaman['tgl_skedul']; ?>"
		});
	});

	function skedulKKN(t){
		var cek = $(t).prop('checked');
		if(cek==true){
			$('#skedul_kkn').slideDown('fast');
		}else{
			$('#skedul_kkn').slideUp('fast');
		}
	}
	function pesertaKKN(t){
		var cek = $(t).prop('checked');
		if(cek==false){
			$('#halaman_peserta_kkn').slideDown('fast');
		}else{
			$('#halaman_peserta_kkn').slideUp('fast');
		}
	}
	function simpanSetting(){
		$('#settingForm').submit();
	}
	function proseskonfirm(t){
		_ajax({
			url:$(t).attr('action'),
			data:{
				pass:$('#passwordkonfirm').val()
			},
			success:function(res){
				res = JSON.parse(res);
				if(res!=false){
					$('#konfirmasiproses').modal('toggle');
					switch($('#konfirmTipe').val()){
						case 'restore':
							restoreData(res);
							break;
						case 'reset':
							window.location.href = '<?php echo base_url('admin/pengaturan/renew') ?>?gen='+res
							break;
					}
					$('#passwordkonfirm').val('');
				}else{
					_alert('password yang anda masukkan salah!');
					$('#passwordkonfirm').val('');
				}
			},
			loading:'#konfirmasiproses form'
		})
		return false;
	}
	function konfirm(t,p){
		$('#konfirmTipe').val(p);
		$('#konfirmasiproses').modal('show');
		return false;
	}
	function restoreData(t){
		_upload({
			name:'filesql',
			action:'<?php echo base_url('admin/pengaturan/restore/upload?gen='); ?>'+t,
			progress:function(res){
				$('#namafile').html(res.percent+'% <i>Mengupload...!</i>');
			},
			complete:function(res){
				res = JSON.parse(res);
				if(res.status==false){
					_alert('Kesalahan',res.data,'error');
					$('#namafile').html('Pulihkan Data');
				}else{
					_progress({
						url:'<?php echo base_url('admin/pengaturan/restore'); ?>',
						start:function(){
							$('#namafile').html('0% <i>Memulihkan data...</i>');
						},
						running:function(res){
							$('#namafile').html(res.proses+'% <i>Memulihkan data...</i>');
						},
						end:function(){
							$('#namafile').html('Pulihkan Data');
							_alert({
								title:'Info',
								msg:'Sistem telah dipulihkan',
								type:'success',
								isClose:function(){
									location.reload();
								}
							})
						},
						callback:{
							file:function(res){
								$('#namafile').html(res.proses+'% <i>Memulihkan dokumen...</i>');
							}
						}
					})
				}
				//$('#namafile').html('Pulihkan Data');
			}
		});
	}
	function cekApi(){
		if($('#alamatapikey').val()==''){
			_alert('Alamat tidak diisi');
		}else{
			_ajax({
				url:'<?php echo base_url('admin/pengaturan/cek') ?>',
				loading:'#formPengaturan',
				data:{
					api:$('#apikey').val(),
					alamat:$('#alamatapikey').val()
				},
				success:function(res){
					res = JSON.parse(res);
					if(res.status==true){
						_alert('Respon OK',res.alamat);
					}else{
						_alert('Tidak tidak benar',res.message,'error');
					}
				}
			})
		}
	}
	function setModel(b){
		var m = null;
		if(b=='c')
			m = $('#minimalprodi').val();
		_ajax({
			url:'<?php echo base_url('admin/pengaturan/setModel'); ?>',
			data:{
				model:b,
				minimal:m
			},
			loading:'#btn-model-'+b,
			success:function(res){
				$('.model_bentuk').html('');
				$('#model_'+b).html('<i class="fa fa-check"></i>');
			}
		})
	}
</script>