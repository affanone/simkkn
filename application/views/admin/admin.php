<div class="row">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
			  	<div class="row">
				  	<div class="col-md-8">
				  		<h4 class="card-title">Data Admin</h4>
				    	<h6 class="card-subtitle mb-2 text-muted">Data admin yang memiliki hak akses penuh pada sistem</h6>
				    </div>
				    <div class="col-md-4">
				    	<a class="float-right btn btn-primary" href="<?php echo base_url('admin/pengguna/admins/tambah'); ?>"><i class="fa fa-plus"></i>Tambah</a>
				    </div>
				    <div class="col-12">
				    	<table id="table" class="table table-bordered">
		                    <thead>
			                    <tr>
			                        <th>
			                        	#
			                        </th>
			                        <th>
			                        	Username
			                        </th>
			                        <th>
			                        	Nama Lengkap
			                        </th>
			                        <th>
			                        	Opsi
			                        </th>
			                    </tr>
		                    </thead>
		                    <tbody>
		                    	<?php
		                    	$i = 1;
		                    		foreach ($data as $key => $value) {
		                    			?>
			                    			<tr>
					                     		<td><?php echo $i++; ?></td>
					                     		<td><?php echo $value->USERN; ?></td>
					                     		<td><?php echo $value->NAMA; ?></td>
					                     		<td width="50px">
					                     			<a title="Edit" class="text-primary" href="<?php echo base_url('admin/pengguna/admins/edit/'.$value->USERN); ?>"><i class="fa fa-pencil"></i></a>
					                     			&nbsp;.&nbsp;
					                     			<?php if($value->USERN!=$this->session->usern){ ?>
																	    <a title="Hapus" class="text-danger"  onclick="hapus('<?php echo base_url('admin/pengguna/admins/hapus/'.$value->USERN); ?>')" href="#!"><i class="fa fa-close"></i></a>
															    	<?php }else{ ?>
															    		<a title="Tidak dapat dihapus" class="text-muted"><i class="fa fa-close"></i></a>
															    	<?php } ?>
					                     		</td>
					                     	</tr>
		                    			<?php
		                    		}
		                    	?>
		                    </tbody>
		                  </table>
				    </div>
				</div>
		  	</div>
		</div>	
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable({
			scrollX: true
		});
	});

	function hapus(url){
			_alert({
				mode:'confirm',
				title:'Apakah akan dihapus?',
				msg:'Semua data yang berhubungan dengan data ini akan ikut terhapus, apakah akan dilanjutkan?',
				yes:'Ya, lanjutkan!',
				no:'Tidak',
				isConfirm:function(){
					window.location = url;
				}
			})
	}
</script>