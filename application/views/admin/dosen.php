<div class="row">
	<div class="col">
		<div class="card" id="containerdosen">
		  	<div class="card-body">
			  	<div class="row">
				  	<div class="col-md-8">
				  		<h4 class="card-title">Data Dosen</h4>
				    	<h6 class="card-subtitle mb-2 text-muted">Data dosen digunakan sebagai Dosen Pendamping Lapangan (DPL)</h6>
				    </div>
				    <div class="col-md-4 text-right">
				    	<div class="btn-group"style="margin-bottom: 15px;" role="group" aria-label="First group">
                <a class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Tambah manual data dosen" href="<?php echo base_url('admin/pengguna/dosen/tambah'); ?>"><i class="fa fa-plus"></i></a>
                <!-- <a href="#!" onclick="upload()" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Upload data via file excel"><i class="fa fa-file-excel-o"></i></a> -->
                <a href="#!" class="btn btn-primary"  onclick="api()" data-toggle="tooltip" data-placement="bottom" title="Download data dari SIMAT"><i class="fa fa-cloud-download"></i></a>
              </div>	
				    </div>
				    <div class="col col-12">
									<table id="table" class="table table-bordered">
	                  <thead>
	                    <tr>
	                        <th>
	                        	#
	                        </th>
	                        <th>
	                        	Username
	                        </th>
	                        <th>
	                        	Nama Lengkap
	                        </th>
	                        <th>
	                        	Alamat
	                        </th>
	                        <th>
	                        	Kontak
	                        </th>
	                        <th>
	                        	Opsi
	                        </th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  	<?php
	                  		$i = 1;
	                  		foreach ($data as $key => $value) {
	                  			?>
	                    			<tr>
			                     		<td><?php echo $i++; ?></td>
			                     		<td><a href="<?php echo base_url('admin/pengguna/dosen/bio/'.$value->KDDPL); ?>" class="btn-link"><?php echo $value->KDDPL; ?></a></td>
			                     		<td><?php echo $value->NAMADPL; ?></td>
			                     		<td><?php echo $value->ALAMATDPL; ?></td>
			                     		<td><?php echo $value->KONTAKDPL; ?></td>
			                     		<td width="50px">
			                     			<a title="Edit" class="text-primary" href="<?php echo base_url('admin/pengguna/dosen/edit/'.$value->KDDPL); ?>"><i class="fa fa-pencil"></i></a>
			                     			&nbsp;.&nbsp;
			                     			<a title="Hapus" class="text-danger" onclick="hapus('<?php echo base_url('admin/pengguna/dosen/hapus/'.$value->KDDPL); ?>')" href="#!"><i class="fa fa-close"></i></a>
			                     		</td>
			                     	</tr>
	                  			<?php
	                  		}
	                  	?>
	                  </tbody>
	                </table>
				    	
				    </div>
				</div>
		  	</div>
		</div>	
	</div>
</div>

<!-- Modal upload -->
<div class="modal fade bd-example-modal-lg" id="upload" tabindex="-1" role="dialog" aria-labelledby="titleexcel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titleexcel">Upload file excel</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col col-8">
        		<div class="alert alert-warning" role="alert">
						  <b>Penting</b> Format file harus ".xls" dan struktur data seperti dibawah!
						</div><br>
        		<img src="<?php echo base_url('assets/images/exampleExcel.jpg'); ?>" class="img-fluid" alt="Responsive image">
        	</div>
        	<div class="col col-4">
        		<div class="alert alert-info" role="alert">
						  Pilih file yang akan diuploa!
						</div>
        		<form action="" class="x334" method="post" enctype="multipart/form-data">
	        		<label class="btn btn-primary btn-lg btn-block">
							    <span id="namafile"><i class="fa fa-file-excel-o "></i> Pilih File </span><input autocomplete="off"  type="file" onchange="pilihfile(this)" name="file" id="file" hidden accept="application/vnd.ms-excel">
							</label>
						</form>

						<h1 class="text-center" style="margin-top: 30px;" id="progressupload">
						</h1>
        	</div>
        	
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="button" onclick="prosesupload()" class="btn btn-primary">Proses</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal api -->
<div class="modal fade bd-example-modal-lg" id="api" tabindex="-1" role="dialog" aria-labelledby="titleapi" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titleapi">Data Dosen Universitas Madura</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col col-12" id="bodyapi">
        		
        	</div>        	
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="button" onclick="prosesupload()" class="btn btn-primary">Proses</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable({scrollX:true});
	});

	function hapus(url){
		_alert({
			mode:'confirm',
			title:'Apakah akan dihapus?',
			msg:'Semua data yang berhubungan dengan data ini akan ikut terhapus, apakah akan dilanjutkan?',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				window.location = url;
			}
		})
	}

	function upload(){
		$('#upload').modal('toggle');
	}

	function api(){
		_ajax({
			url:'<?php echo base_url('admin/pengguna/dosen/api'); ?>',
			loading:'#containerdosen',
			success:function(res){
				$('#bodyapi').html(res);
				$('#api').modal('toggle');
			}
		})
	}

	var vup = '';
	function pilihfile(t){
		vup = $(t).val();
		if(vup==''){
			$('#namafile').html('<i class="fa fa-file-excel-o "></i> Pilih File');
		}else{
			$('#namafile').html($(t).val());
		}
	}

	function prosesupload(){
		if(vup==''){
			_alert('Pilih file terlebih dahulu!');
		}else{
			_upload({
				name:'file',
				action:'<?php echo base_url('admin/pengguna/dosen/upload'); ?>',
				progress:function(data){
					$('#progressupload').text(data.percent+'%');
				},
				complete:function(data){
					data = JSON.parse(data);
					$('#progressupload').text('');
					if(data.status == false){
						_alert(data.data);
					}else{
						_ajax({
							url:'<?php echo base_url('admin/pengguna/dosen/exceltodb'); ?>',
							loading:'.x334',
							success:function(d){
								window.location = '<?php echo base_url('admin/pengguna/dosen'); ?>';
							}
						})

					}
				}
			});
		}
	}
</script>