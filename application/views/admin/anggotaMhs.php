<form>
	<table class="table datatable table-bordered table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Npm</th>
				<th>Nama</th>
				<th>Fak / Prodi</th>
			</tr>
		</thead>
		<tbody class="ul">
			<?php
			$i = 1;
				foreach ($data as $key => $value) {
					?>
					<tr style="cursor: pointer;" onclick="mark(this,'<?php echo $tipe; ?>')" class="ls-<?php echo $tipe; ?>" data-npm="<?php echo $value->NPM; ?>" data-kelompok="<?php echo $kelompok; ?>">
						<td class="li"></td>
						<td><?php echo $value->NPM; ?></td>
						<td><?php echo $value->NAMAMHS; ?></td>
						<td><?php echo $value->NAMAPRODI; ?></td>
					</tr>
					<?php
				}
			?>
		</tbody>
	</table>	  
</form>