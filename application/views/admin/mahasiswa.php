<div id="data-mahasiswa"><div class="row">
	<div class="col">
		<div class="card" id="containermahasiswa">
		  	<div class="card-body">
			  	<div class="row">
				  	<div class="col-md-8">
				  		<h4 class="card-title">Data Mahasiswa</h4>
				    	<h6 class="card-subtitle mb-2 text-muted">Data mahasiswa digunakan sebagai peserta KKN</h6>
				    </div>
				    <div class="col-md-4 text-right">
				    	<div class="btn-group"style="margin-bottom: 15px;" role="group" aria-label="First group">
                <a class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Tambah manual data mahasiswa" href="<?php echo base_url('admin/pengguna/mahasiswa/tambah'); ?>"><i class="fa fa-plus"></i></a>
                <a href="#!" onclick="upload()" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Upload data via file excel"><i class="fa fa-file-excel-o"></i></a>
        <!--         <a href="<?php echo base_url('admin/pengguna/mahasiswa/verified'); ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Verifikasi Semua"><i class="fa fa-check"></i></a> -->
                <a href="#!" onclick="return confirmArsip(this)" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="Arsipkan peserta kkn guna untuk pengadaan peserta kkn baru"><i class="fa fa-archive"></i></a>
                <!-- <a href="#!" class="btn btn-primary"  onclick="api()" data-toggle="tooltip" data-placement="bottom" title="Download data dari SIMAT"><i class="fa fa-cloud-download"></i></a> -->
              </div>	
				    </div>

				    <div class="modal fade" tabindex="-1" role="dialog" id="konfirmmodalarsip">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
									<form action="<?php echo base_url('admin/pengguna/mahasiswa/arsipkan'); ?>" onsubmit="return proseskonfirm(this)" method="post">
						      	<div class="modal-header">
							        <h5 class="modal-title">Konfirmasi Pengarsipan Data</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							      	<div class="alert alert-warning" role="alert">
											  Proses pengarsipan memerlukan password untuk melanjutkannya!, gunakan password pada saat anda login
											</div>
											  <div class="form-group">
											    <input autocomplete="off"  type="password" name="password" class="form-control" id="passwordkonfirm" aria-describedby="passHelp" placeholder="Masukkan Password">
											    <small id="passHelp" class="form-text text-muted">Demi keamanan dari kesalahan pengguna</small>
											  </div>
							      </div>
							      <div class="modal-footer">
							        <button type="submit" class="btn btn-primary">Arsipkan</button>
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
							      </div>
							    </form>
						    </div>
						  </div>
						</div>

				    <div class="col col-12">
				    	<form class="form-inline float-right mb-2" action="<?php echo base_url('admin/pengguna/mahasiswa/data') ?>" method="post" id="formcari">
								<input autocomplete="off"  type="hidden" name="status" value="<?php echo $status; ?>">
				    		<div class="dropdown mr-2">
								  <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" label="status">
								    <?php
								    	switch ($status) {
								    		case '0':
								    			echo 'Tidak Terkelompok';
								    			break;
								    		case '1':
								    			echo 'Terkelompok';
								    			break;
								    		case '2':
								    			echo 'Lulus';
								    			break;
								    		case '3':
								    			echo 'Tidak Lulus';
								    			break;
								    		default:
								    			echo 'Semua';
								    			break;
								    	}
								    ?>
								  </button>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								    <a class="dropdown-item" onclick="statusFilter('status','','Semua')" href="#">Semua</a>
								    <a class="dropdown-item" onclick="statusFilter('status','1','Terkelompok')" href="#">Terkelompok</a>
								    <a class="dropdown-item" onclick="statusFilter('status','0','Tidak Terkelompok')" href="#">Tidak Terkelompok</a>
								    <a class="dropdown-item" onclick="statusFilter('status','2','Lulus')" href="#">Lulus</a>
								    <a class="dropdown-item" onclick="statusFilter('status','3','Tidak Lulus')" href="#">Tidak Lulus</a>
								  </div>
								</div>
			
							  <div class="input-group  mr-sm-2">
							    <input autocomplete="off"  type="text" name="cari" value="<?php echo $cari; ?>" class="form-control" placeholder="Cari">
							  </div>


							  <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
							</form>
				    </div>
				    <div class="col col-12">
									<table id="table" class="table datatable table-sm table-bordered table-hover">
	                  <thead>
	                    <tr>
	                        <th>
	                        	#
	                        </th>
	                        <th>
	                        	Tgl Reg
	                        </th>
	                        <th>
	                        	NPM
	                        </th>
	                        <th>
	                        	Nama Lengkap
	                        </th>
	                        <th>
	                        	Fakultas / Prodi
	                        </th>
	                        <th>
	                        	Status
	                        </th>
	                        <th>
	                        	Opsi
	                        </th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  	<?php
	                  		$i = $nomor;
	                  		foreach ($data as $key => $value) {
	                  			?>
	                    			<tr class="<?php echo ($value->STATUSMHS==0) ? 'text-danger' : ''; ?>">
			                     		<td><?php echo $i++; ?></td>
			                     		<td><?php echo $this->mfungsi->tgl($value->TGLREGMHS,true); ?></td>
			                     		<td><a class="btn-link" href="<?php echo base_url('admin/pengguna/mahasiswa/biodata/'.$value->NPM); ?>"><?php echo $value->NPM; ?></a></td>
			                     		<td><?php echo $value->NAMAMHS; ?></td>
			                     		<td><?php echo $value->NAMAPRODI; ?></td>
			                     		<td>
			                     			<?php
			                     			$sts = '';
														    	switch ($value->STATUSMHS) {
														    		case '0':
														    			if($value->LULUS>0){
														    				$sts =  '<span class="badge badge-danger badge-pill">Tidak Lulus</span>';
														    			}else{
														    				$sts = '<span class="badge badge-warning badge-pill">Tidak Terkelompok</span>';
														    			}
														    			break;
														    		case '1':
														    			$sts =  '<span class="badge badge-info badge-pill">Terkelompok</span>';
														    			break;
														    		case '2':
														    			$sts =  '<span class="badge badge-primary badge-pill">Lulus</span>';
														    			break;
														    	}
														    ?>
								    						<?php echo $sts; ?></td>
			                     		<td width="100px">
			                     			<a title="Edit" class="text-primary" href="<?php echo base_url('admin/pengguna/mahasiswa/edit/'.$value->NPM); ?>"><i class="fa fa-pencil"></i></a>
			                     			&nbsp;.&nbsp;

			                     				
			                     			<?php if($value->STATUSMHS==0 && $value->EXISTS==NULL){ ?>
															    <a title="Hapus" class="text-danger" onclick="hapus('<?php echo base_url('admin/pengguna/mahasiswa/hapus/'.$value->NPM); ?>')" href="#!"><i class="fa fa-close"></i></a>
															  <?php }else if($value->STATUSMHS==1){ ?>
															  	<a title="Keluarkan dari kelompok kkn" class="text-danger" onclick="keluarkan('<?php echo base_url('admin/pengguna/mahasiswa/keluar/'.$value->NPM); ?>')" href="#!"><i class="fa fa-level-down"></i></a>
													    	<?php }else{ ?>
													    		<a title="Tidak dapat dihapus" class="text-muted"><i class="fa fa-close"></i></a>
													    	<?php } ?>
			                     		</td>
			                     	</tr>
	                  			<?php
	                  		}
	                  	?>
	                  </tbody>
	                </table>
				    			<i>Total data : <?php echo $total; ?></i><div style="font-size: 12px;">XL : <?php echo (!isset($srg['xl'])) ? 0 : $srg['xl']; ?> | L : <?php echo (!isset($srg['l'])) ? 0 : $srg['l']; ?> | M : <?php echo (!isset($srg['m'])) ? 0 : $srg['m']; ?> | S : <?php echo (!isset($srg['s'])) ? 0 : $srg['s']; ?></div>
				    </div>
				</div>
		  	</div>

		  	<?php
			  	//if($cari == ''){

			  		echo $this->mpaging->set(array(
							'url'=>base_url('admin/pengguna/mahasiswa/data/'),
							'uri'=>5,
							'count'=>$total,
							'attribute'=>array('class' => 'page-link','onclick'=>'return paging(this)','base-url'=>base_url('admin/pengguna/mahasiswa/data/'))
						));
			  	//}
		  	?>
		</div>	
	</div>
</div>

<!-- Modal upload -->
<div class="modal fade bd-example-modal-lg" id="upload" tabindex="-1" role="dialog" aria-labelledby="titleexcel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titleexcel">Upload file excel</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col col-8">
        		<div class="alert alert-warning" role="alert">
						  <b>Penting</b> Format file harus ".xls" dan struktur data seperti dibawah!
						</div><br>
        		<img src="<?php echo base_url('assets/images/mhsExcel.jpg?').rand(1111,9999); ?>" class="img-fluid" alt="Responsive image">
        	</div>
        	<div class="col col-4">
        		<div class="alert alert-info" role="alert">
						  Pilih file yang akan diuploa!
						</div>
        		<form action="" class="x334" method="post" enctype="multipart/form-data">
	        		<label class="btn btn-primary btn-lg btn-block">
							    <span id="namafile"><i class="fa fa-file-excel-o "></i> Pilih File </span><input autocomplete="off"  type="file" onchange="pilihfile(this)" name="file" id="file" hidden accept="application/vnd.ms-excel">
							</label>
							<!-- <div class="form-group">
                <div class="form-check form-check-flat">
                  <label class="form-check-label">
                    <input autocomplete="off"  type="checkbox" class="form-check-input" value="1" id="vrfUpload">
                    Auto Verifikasi
                  <i class="input-helper"></i></label>
                </div>
              </div> -->
						</form>

						<h1 class="text-center" style="margin-top: 30px;" id="progressupload">
						</h1>
        	</div>
        	
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="button" onclick="prosesupload()" class="btn btn-primary">Proses</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal api -->
<div class="modal fade bd-example-modal-lg" id="api" tabindex="-1" role="dialog" aria-labelledby="titleapi" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="titleapi">Data Mahasiswa Universitas Madura</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col col-3">
        		       		<div class="alert alert-info" role="alert">
						  Tentukan range tahun untuk angkatan mahasiswa yang akan ditampilkan!
						</div>

        		<form class="forms-sample" action="" method="post">

		        			<input autocomplete="off"  type="hidden" name="smpthn">
			        			<input autocomplete="off"  type="hidden" name="drthn">
              <div class="form-group">
                <label for="labeltahun">* Dari <?php echo form_error('label','<small class="text-danger">','</small>'); ?></label>
	                <div class="dropdown show">
									  <a class="btn btn-primary dropdown-toggle" href="#!" role="button" id="darithndr" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" label="drthn">
									    Tahun?
									  </a>

									  <div class="dropdown-menu" aria-labelledby="darithndr">
									  	<?php 
									  		for ($i=date('Y'); $i > date('Y')-7; $i--) { 
									  			?>
									  				<a class="dropdown-item" onclick="_setValue('drthn','<?php echo $i; ?>')" href="#!"><?php echo $i; ?></a>
									  			<?php
									  		}
									  	?>
									  </div>
									</div>
              </div>

              <div class="form-group">
                <label for="labeltahun">* Sampai <?php echo form_error('label','<small class="text-danger">','</small>'); ?></label>
                <div class="dropdown show">
								  <a class="btn btn-primary dropdown-toggle" href="#!" role="button" id="smpthnsm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" label="smpthn">
								    Tahun?
								  </a>

								  <div class="dropdown-menu" aria-labelledby="smpthnsm">
								  	<?php 
								  		for ($i=date('Y'); $i > date('Y')-7; $i--) { 
								  			?>
								  				<a class="dropdown-item" onclick="_setValue('smpthn','<?php echo $i; ?>')" href="#!"><?php echo $i; ?></a>
								  			<?php
								  		}
								  	?>
								  </div>
								</div>
              </div>
              <div class="form-group">
              	<div class="dropdown-divider">
              		
              	</div>
              </div>
              
              <button type="button" onclick="callapi()" class="btn btn-success mr-2">Ok</button>
            </form>
        	</div>
        	<div class="col col-9" id="bodyapi">
        			<div class="alert alert-success" role="alert">
							  Belum ada data!
							</div>
        	</div>        	
        </div>
      </div>
      <div class="modal-footer">
        <button type="button"  class="btn btn-secondary" data-dismiss="modal">Kembali</button>
      </div>
    </div>
  </div>
</div>

<div class="maxBlack align-items-center justify-content-center">
	<div class="row w-50  w-md-75">
		<div class="col-12 xbefore">
			<h1 class="text-white text-center xval">0%</h1>
			<div class="progress">
			  <div class="progress-bar xbar" style="width: 0%;" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
			</div>	
		</div>
		<div class="col-12 xafter">
			<div class="list-group">
			  <div href="#" class="list-group-item list-group-item-action flex-column align-items-start">
			    <div class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1">Alamat Mahasiswa</h5>
			    </div>
			    <p class="mb-1 alamatmhs">
			    	Jl manhattam
			    </p>
			  </div>
			  <div href="#" class="list-group-item list-group-item-action flex-column align-items-start">
			    <div class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1">Kelompok KKN</h5>
			      <small class="text-muted">Kelompok yang tersedia</small>
			    </div>
			    <p class="mb-1">
			    	<ul class="kelompoktersedia">
			    	</ul>
			    </p>
			  </div>
			  <div href="#" class="list-group-item list-group-item-action flex-column align-items-start">
			    <div class="d-flex w-100 justify-content-between">
			      <h5 class="mb-1">Kelompok KKN Mahasiswa</h5>
			      <small class="text-muted">Dipilih secara otomatis dengan jarak terdekat</small>
			    </div>
			    <p class="mb-1">
						<ul>
			    		<li class="kelompokdipilih"><b>Kelompok 1</b> manhattam</li>
			    	</ul>
			    </p>
			  </div>
			</div>
			<button type="button" onclick="tutupinfoverifikasi();" class="btn mt-3 btn-primary">Tutup</button>
		</div>
	</div>
</div>


<div class="modal fade" id="modalstatusverifikasi" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
 <!--      <div class="modal-header">
       <h5 class="modal-title"></h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> 
      </div>-->
      <div class="modal-body">
        
        <div class="col-12 xbefore">
					<h1 class="text-primary text-center xval">0%</h1>
					<div class="progress">
					  <div class="progress-bar xbar" style="width: 0%;" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
					</div>	

					<div class="row mt-2">
						<div class="col-12">
							<div class="dropdown-divider">
								
							</div>
						</div>
						<div class="col-12">
							<div class="card-title">
								<b>Informasi</b>
							</div>
						</div>
						<div class="col-12">
							<table class="table-striped table">
								<tbody id="msgerror">
								</tbody>
							</table>
						</div>
					</div>
				</div>

						
				<div class="col-12 xafter mt-2">

					<div class="list-group">
					  <div href="#" class="list-group-item list-group-item-action flex-column align-items-start">
					    <div class="d-flex w-100 justify-content-between">
					      <h5 class="mb-1">Alamat Mahasiswa</h5>
					    </div>
					    <p class="mb-1 alamatmhs">
					    	Jl manhattam
					    </p>
					  </div>
					  <div href="#" class="list-group-item list-group-item-action flex-column align-items-start">
					    <div class="d-flex w-100 justify-content-between">
					      <h5 class="mb-1">Kelompok KKN</h5>
					      <small class="text-muted">Kelompok yang tersedia</small>
					    </div>
					    <p class="mb-1">
					    	<ul class="kelompoktersedia">
					    	</ul>
					    </p>
					  </div>
					  <div href="#" class="list-group-item list-group-item-action flex-column align-items-start">
					    <div class="d-flex w-100 justify-content-between">
					      <h5 class="mb-1">Kelompok KKN Mahasiswa</h5>
					      <small class="text-muted">Dipilih secara otomatis dengan jarak terdekat</small>
					    </div>
					    <p class="mb-1">
								<ul>
					    		<li class="kelompokdipilih"><b>Kelompok 1</b> manhattam</li>
					    	</ul>
					    </p>
					  </div>
					</div>

				</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary xbtn32" onclick="kembali(this);" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.datatable').DataTable({
			paging:false,
			scrollX:true,
			searching:false,
			responsive:true,
			info:false,
			width:'100%'
		});
	});

	function paging(t){
		var url = $(t).attr('base-url');
		var i = $(t).attr('data-ci-pagination-page');
		_ajax({
			url:url+i,
			data:{
				cari:'<?php echo $cari; ?>',
				status:'<?php echo $status; ?>',
				ajax:true
			},
			loading:'#containermahasiswa',
			success:function(res){
				$('#data-mahasiswa').html(res);
			}
		})
		return false;
	}

	function tutupinfoverifikasi(){
		kembali();
		return false;
	}

	function statusFilter(a,b,c){
		_setValue(a,b,c);
		$('#formcari').submit();
	}

	function verified(t){
		$('#modalstatusverifikasi').modal({
		    backdrop: 'static',
		    keyboard: false
		}).toggle();
		var url = $(t).attr('href');
		_progress({
			url:url,
			loading:'#containermahasiswa',
			start:function(){
				$('#msgerror').html('');
				$('.xbtn32').attr('disabled', '');
				$('.xbar').css({'width':'0%'});
				$('.xval').text('0%');
				$('.xbefore').css({'display':'block'});
				$('.xafter').css({'display':'none'});
				$('.xafter .alamatmhs').text('');
				$('.xafter .kelompoktersedia').html('');
				$('.xafter .kelompokdipilih').html('');
			},
			running:function(res){
				$('.xbar').css({'width':res.process+'%'});
				$('.xval').text(res.process+'%');
			},
			callback:{
				error:function(res){
					$('#msgerror').append('<tr><td class="text-danger">'+res.msg+'</td></tr>');
				},
				end:function(v){
					$('.xbar').css({'width':v.val+'%'});
					$('.xval').text(v.val+'%');
				}
			},
			end:function(res){
				$('.xbtn32').removeAttr('disabled');
				if(res.error != true){
					var list = res.data.list;
					for(var i in list){
						$('.xafter .kelompoktersedia').append('<li><b>'+list[i].NAMAKEL+'</b> ('+list[i].ALAMATKEL+')</li>');
					}
					$('.xbefore').css({'display':'none'});
					$('.xbefore').css({'display':'unset'});
					$('.xafter .alamatmhs').text(res.data.mhs.alamat);
					$('.xafter .kelompokdipilih').html('<b>'+res.data.select.NAMAKEL+'</b> ('+res.data.select.ALAMATKEL+')');
					$('.xbefore').css({'display':'none'});
					$('.xafter').css({'display':'block'});
				}
			}
		})
		return false;
	}

	function cancelverified(url){
		_alert({
			mode:'confirm',
			title:'Apakah verifikasi dibatalkan?',
			msg:'',
			yes:'Ya, batalkan!',
			no:'Tidak',
			isConfirm:function(){
				_ajax({
					url:url,
					loading:'#containermahasiswa',
					success:function(res){
						kembali();
					}
				})
			}
		})
	}

	function kembali(t=this){
		$(t).dequeue();
		$(t).delay(500).queue(function(){
			_ajax({
				url:'<?php echo $this->mhistory->back(); ?>',
				data:{
					cari:'<?php echo $cari; ?>',
					status:'<?php echo $status; ?>',
					ajax:true
				},
				loading:'#containermahasiswa',
				success:function(res){
					$('#data-mahasiswa').html(res);
				}
			})
		})
	}
	function hapus(url){
		_alert({
			mode:'confirm',
			title:'Apakah akan dihapus?',
			msg:'Semua data yang berhubungan dengan data ini akan ikut terhapus, apakah akan dilanjutkan?',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				window.location = url;
			}
		})
	}

	function keluarkan(url){
		_alert({
			mode:'confirm',
			title:'Apakah akan dikeluarkan?',
			msg:'Peserta akan dikeluarkan dari anggota kelompok KKN saat ini, apakah akan dilanjutkan?',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				window.location = url;
			}
		})
	}

	function upload(){
		$('#upload').modal('toggle');
	}

	function api(){

		$('#api').modal('toggle');
	}

	function callapi(){
		_progress({
			url:'<?php echo base_url('admin/pengguna/mahasiswa/api'); ?>',
			loading:'#api .modal-dialog',
			framework:true,
			data:{
				dari:$('[name="drthn"]').val(),
				sampai:$('[name="smpthn"]').val(),
			},
			start:function(){
				$('#bodyapi').html('<center><h1>Menghitung...!</h1></center>');
			},
			running:function(res){
				$('#bodyapi').html('<center><h1>'+res.progress+' s/d '+res.total+'</h1></center>');
			},
			end:function(res){
				$('#bodyapi').html(res.template);
			}
		})
	}

	var vup = '';
	function pilihfile(t){
		vup = $(t).val();
		if(vup==''){
			$('#namafile').html('<i class="fa fa-file-excel-o "></i> Pilih File');
		}else{
			$('#namafile').html($(t).val());
		}
	}

	function prosesupload(){
		if(vup==''){
			_alert('Pilih file terlebih dahulu!');
		}else{
			_upload({
				name:'file',
				action:'<?php echo base_url('admin/pengguna/mahasiswa/upload1'); ?>',
				progress:function(data){
					$('#progressupload').text(data.percent+'%');
				},
				complete:function(data){
					data = JSON.parse(data);
					$('#progressupload').text('');
					if(data.status == false){
						_alert(data.data);
					}else{
						var total = null;
						_progress({	
							url:'<?php echo base_url('admin/pengguna/mahasiswa/upload2'); ?>',
							start:function(d){
								total = 100/d;
								$('#progressupload').text(0+'%');
							},
							running:function(d){
								$('#progressupload').text(Math.floor(total*d)+'%');
							},
							end:function(){
								window.location = '<?php echo base_url('admin/pengguna/mahasiswa'); ?>';
							}
						})
					}
				}
			});
		}
	}

	function confirmArsip(t){
		_ajax({
			url:'<?php echo base_url('admin/pengguna/mahasiswa/cek_nilai') ?>',
			loading:'#containermahasiswa',
			success:function(res){
				res = JSON.parse(res);
				// if(res.status==true)
				// 	$('#konfirmmodalarsip .modal-content').html(res.html);

				$('#konfirmmodalarsip').modal('toggle');
			}
		})
		return false;
	}

	function proseskonfirm(t){
		var url = $(t).attr('action');
		_ajax({
			url:url,
			data:$(t).serialize(),
			success:function(res){
				res = JSON.parse(res);
				if(res.status==true){
					$('#konfirmmodalarsip').modal('toggle');
					_alert({
						title:'Info',
						msg:res.msg,
						isClose:function(){
							location.reload();
						}
					})
				}else{
					_alert(res.msg);
				}
			},
			loading:'#konfirmmodalarsip form'
		})
		return false;
	}
</script>
</div>