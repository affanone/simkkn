<style type="text/css">
	.body-tab{
		display: none;
	}
</style>
<div class="card">
  <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs">
      <li class="nav-item">
        <a class="nav-link active tab-menu" href="#upload">Unggah Laporan <span class="badge badge-pill badge-primary"><?php echo count($data); ?></span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link tab-menu" href="#ajuan">Pengajuan <span class="badge badge-pill badge-primary"><?php echo count($pengajuan); ?></a>
      </li>
      <li class="nav-item">
        <a class="nav-link tab-menu" href="#ajuan-terima">Pengajuan Diterima <span class="badge badge-pill badge-primary"><?php echo count($diterima); ?></a>
      </li>
      <li class="nav-item">
        <a class="nav-link tab-menu" href="#monitor">Lembar Monitoring <span class="badge badge-pill badge-primary"><?php echo count($monitoring); ?></a>
      </li>
      <li class="nav-item">
        <a class="nav-link tab-menu" href="#nilai">Nilai Mahasiswa</a>
      </li>
      <li class="nav-item" style="right:  0;position:  absolute;">
        <a class="nav-link text-danger" href="<?php echo $this->mhistory->back(); ?>" data-toggle="tooltip" data-placement="bottom" title="Tutup"><i class="fa fa-close"></i></a>
      </li>
    </ul>
  </div>
  <div class="card-body">
    <h4 class="card-title">KELOMPOK KKN <?php echo $kelompok[0]->NAMAKEL; ?></h4>
    <h6 class="card-title"><?php echo $kelompok[0]->NAMADPL; ?> <?php echo ($kelompok[0]->KONTAKDPL==NULL) ? '' : ' ('.$kelompok[0]->KONTAKDPL.')'; ?></h6>
    <p class="card-text text-muted"><?php echo $kelompok[0]->ALAMATKEL; ?></p>
    <div class="body-tab" id="upload">
		    <table class="table datatable table-hover xloadfile">
		    	<thead>
		    		<tr class="text-muted">
		    			<th></th>
		    			<th>File</th>
		    			<th>Upload User</th>
		    			<th>Size</th>
		    			<th>Date</th>
              <th>Jenis</th>
              <th>Deskripsi</th>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		
		    	<?php
		    	foreach ($data as $key => $value){ 
		    		$ext = explode('.', $value->FILEUPL);
		    		$ext = end($ext);
						echo '<tr><td width="60px"><img src="'.base_url('assets/images/icons/'.$ext.'.png').'" class="img-fluid rounded"></td>
			    			<td><a '.(($ext=='pdf')? 'onclick="_cetak(this)"' : '' ).' href="'.base_url('admin/pdf/view/').$kode.'/'.$value->KDUPL.'" class="btn-link" title="'.$value->FILEUPL.'">'.$this->mfungsi->minimalText($value->FILEUPL,15).'</a>&nbsp;.&nbsp;<a href="'.base_url('admin/pdf/download/').$kode.'/'.$value->KDUPL.'">Download</a></td>
			    			<td><span data-toggle="tooltip" data-placement="bottom" title="'.$value->NPM.'">'.$value->NAMAMHS.'</span></td>
			    			<td>'.$this->mfungsi->koversiSize($value->SIZEUPL).'</td>
			    			<td>'.$this->mfungsi->tgl($value->TGLUPL,true).'</td>
			    			<td>'.ucwords($value->JENISUPL).'</td>
                <td>'.$value->DESKUPL.'</td>
			    			</tr>';
		    	}
		    	?>
		    	</tbody>
		    </table>
    </div>
    <div class="body-tab" id="ajuan">
    	<table class="table datatable table-hover xloadfile">
		    	<thead>
		    		<tr class="text-muted">
		    			<th></th>
		    			<th>File</th>
		    			<th>Upload User</th>
		    			<th>Size</th>
		    			<th>Date</th>
              <th>Jenis</th>
              <th>Deskripsi</th>
		    			<th>Konfirm</th>
		    		</tr>
		    	</thead>
		    	<tbody>
		    	<?php
		    	foreach ($pengajuan as $key => $value){ 
		    		$ext = explode('.', $value->FILEUPL);
		    		$ext = end($ext);
						echo '<tr>
										<td width="60px"><img src="'.base_url('assets/images/icons/'.$ext.'.png').'" class="img-fluid rounded"></td>
			    					<td><a '.(($ext=='pdf')? 'onclick="_cetak(this)"' : '' ).' href="'.base_url('admin/pdf/view/').$kode.'/'.$value->KDUPL.'" class="btn-link" title="'.$value->FILEUPL.'">'.$this->mfungsi->minimalText($value->FILEUPL,15).'</a>&nbsp;.&nbsp;<a href="'.base_url('admin/pdf/download/').$kode.'/'.$value->KDUPL.'">Download</a></td>
			    					<td><span data-toggle="tooltip" data-placement="bottom" title="'.$value->NPM.'">'.$value->NAMAMHS.'</span></td>
			    					<td>'.$this->mfungsi->koversiSize($value->SIZEUPL).'</td>
			    					<td>'.$this->mfungsi->tgl($value->TGLUPL,true).'</td>
			    					<td>'.ucwords($value->JENISUPL).'</td>
                    <td>'.$value->DESKUPL.'</td>
			    					<td><a onclick="return ajuan(this)" href="'.base_url('admin/statistika/kinerja/ajuan/y/').$kode.'/'.$value->KDUPL.'" class="btn-link text-primary">Ya</a> <span class="text-muted">|</span> <a onclick="return ajuancancel(this)" href="'.base_url('admin/statistika/kinerja/ajuan/n/').$kode.'/'.$value->KDUPL.'" class="btn-link text-danger">Tidak</a></td>
			    			</tr>';
		    	}
		    	?>
		    	</tbody>
		    </table>
		    <div class="modal fade" tabindex="-1" id="alasan-ajuan" role="dialog">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h3 class="modal-title">Berikan Alasan</h3>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body xloading-alasan">
				        <h5 class="text-muted modal-title">Mengapa anda menolak pengajuan ini?</h5>				 
				        <p>
				        	<form>
									  <div class="form-group">
									  	<input autocomplete="off"  type="hidden" id="url-alasan">
									    <textarea name="" class="form-control" id="input-alasan" placeholder="Alasan Anda..."></textarea>
									  </div>
									</form> 
				        </p>       
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-primary" onclick="kirimcancelajuan()">Kirim Ke DPL</button>
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				      </div>
				    </div>
				  </div>
				</div>
    </div>
    <div class="body-tab" id="ajuan-terima">
	    <table class="table datatable table-hover xloadfile">
	    	<thead>
	    		<tr class="text-muted">
	    			<th>#</th>
	    			<th>File</th>
	    			<th>Upload User</th>
	    			<th>Size</th>
	    			<th>Date</th>
            <th>Jenis</th>
            <th>Deskripsi</th>
	    			<th></th>
	    		</tr>
	    	</thead>
	    	<tbody>
	    		
	    	<?php
	    	foreach ($diterima as $key => $value){ 
	    		$ext = explode('.', $value->FILEUPL);
	    		$ext = end($ext);
					echo '<tr>
									<td width="60px"><img src="'.base_url('assets/images/icons/'.$ext.'.png').'" class="img-fluid rounded"></td>
		    					<td><a '.(($ext=='pdf')? 'onclick="_cetak(this)"' : '' ).' href="'.base_url('admin/pdf/view/').$kode.'/'.$value->KDUPL.'" class="btn-link" title="'.$value->FILEUPL.'">'.$this->mfungsi->minimalText($value->FILEUPL,15).'</a>&nbsp;.&nbsp;<a href="'.base_url('admin/pdf/download/').$kode.'/'.$value->KDUPL.'">Download</a></td>
		    					<td><span data-toggle="tooltip" data-placement="bottom" title="'.$value->NPM.'">'.$value->NAMAMHS.'</span></td>
		    					<td>'.$this->mfungsi->koversiSize($value->SIZEUPL).'</td>
		    					<td>'.$this->mfungsi->tgl($value->TGLUPL,true).'</td>
                  <td>'.ucwords($value->JENISUPL).'</td>
                  <td>'.$value->DESKUPL.'</td>
		    					<td><a onclick="return ajuan(this)" href="'.base_url('admin/statistika/kinerja/btlajuan/').$kode.'/'.$value->KDUPL.'" class="btn-link text-danger">Batal</a></td>
		    			</tr>';
	    	}
	    	?>
	    	</tbody>
	    </table>
    </div>
    <div class="body-tab" id="monitor">
    	<div class="row" style="overflow:  auto;">
        <div class="col-12">
            <table class="table datatable table-hover xloadfile">
                <thead>
                    <tr class="text-muted">
                        <th></th>
                        <th>File</th>
                        <th>Size</th>
                        <th>Tgl Unggah</th>
                    </tr>
                </thead>
                <tbody>
                   <?php 
                   foreach ($monitoring as $key => $value) {
                        $ext = explode('.', $value->FILELM);
                        $ext = end($ext);
                            echo '<tr><td width="60px"><img src="'.base_url('assets/images/icons/'.$ext.'.png').'" class="img-fluid rounded"></td>
                                <td><a '.(($ext=='pdf')? 'onclick="_cetak(this)"' : '' ).' href="'.base_url('admin/pdf/monitoring/view/').$kode.'/'.$value->KDLM.'" class="btn-link">'.str_replace(array('-','_'), ' ', $value->FILELM).'</a>&nbsp;.&nbsp;<a href="'.base_url('admin/pdf/monitoring/download/').$kode.'/'.$value->KDLM.'">Download</a></td>
                                <td>'.$this->mfungsi->koversiSize($value->SIZELM).'</td>
                                <td>'.$this->mfungsi->tgl($value->TGLLM,true).'</td>
                                </tr>';
                   }
                    ?>
                </tbody>
            </table>
        </div>
    	</div>
    </div>
    <div class="body-tab" id="nilai">
    	<div class="row mb-3">
		    <div class="col offset-8 col-4">
		    	<a class="float-right btn btn-outline-primary" onclick="_cetak(this)" href="<?php echo base_url('admin/statistika/kinerja/cetak/nilai/').$kode; ?>"><i class="fa fa-print"></i>Cetak</a>
		    </div>
		  </div>
	    <table class="table datatable table-bordered table-striped">
	    	<thead>
	    		<tr>
	    			<th>#</th>
	    			<th>NPM</th>
	    			<th>NAMA</th>
	    			<th>FAK / PRODI</th>
	    			<th>NILAI ANGKA</th>
	    			<th>NILAI HURUF</th>
	    		</tr>
	    	</thead>
	    	<tbody>
	    		<?php
	    		$i = 1;
	    		foreach ($nilai as $key => $value) {
	    		?>
	    			<tr>
		    			<td><?php echo $i++; ?></td>
		    			<td><?php echo $value->NPM; ?></td>
		    			<td><?php echo $value->NAMAMHS; ?></td>
		    			<td><?php echo $value->FAKPRODI.' / '.$value->NAMAPRODI; ?></td>
		    			<td align="center" id="nilaiagk<?php echo $value->NPM; ?>"><?php echo $value->NILAIMHS; ?></td>
		    			<td align="center" id="nilaihrf<?php echo $value->NPM; ?>"><?php echo $this->mfungsi->nilaihuruf($value->NILAIMHS); ?></td>
		    		</tr>
	    		<?php
	    		}
	    		?>
	    	</tbody>
	    </table>
    </div>
  </div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		_tab({
			tab:'.tab-menu',
			body:'.body-tab',
			default:'#upload'
		})
		$('.datatable').DataTable({
			scrollX: true,
			paging:false
		});
	});

	function ajuan(t){
		var url = $(t).attr('href');
		_alert({
			mode:'confirm',
			title:'Apakah akan dilanjutkan?',
			msg:'',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				_ajax({
					url:url,
					loading:'.xloadfile',
					success:function(){
						location.reload();
					}
				})
			}
		})
		return false;
	}

	function ajuancancel(t){
		$('#alasan-ajuan').modal('toggle');
		var url = $(t).attr('href');
		$('#url-alasan').val(url);
		return false;
	}

	function kirimcancelajuan(){
		_alert({
			mode:'confirm',
			title:'Apakah akan dilanjutkan?',
			msg:'',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				_ajax({
					url:$('#url-alasan').val(),
					data:{
						alasan:$('#input-alasan').val()
					},
					loading:'.xloading-alasan',
					success:function(res){
						location.reload();
					}
				})
			}
		})
	}
</script>