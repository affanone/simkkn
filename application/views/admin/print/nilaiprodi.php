<div class="container mt-2">
	<?php 
	$this->load->view('dosen/print/kop');
?>
	<div class="mt-2 mb-2 text-center card-outline-secondary"><u>DAFTAR NILAI MAHASISWA KULIAH KERJA NYATA<br><?php echo $nama; ?><br>UNIVERSITAS MADURA TAHUN AKADEMIK TAHUN AKADEMIK <?php echo $this->mfungsi->tahun()->label; ?></u></div>


	    <table  class="table table-bordered table-hover">
				<thead class="text-center text-muted">
					<tr>
						<th>#</th>
						<th>NPM</th>
						<th>NAMA</th>
						<th>KELOMPOK KKN</th>
						<th>NILAI</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach ($data as $key => $value) {
						?>
						<tr>
							<td><?php echo $key+1; ?></td>
							<td><?php echo $value->NPM; ?></td>
							<td><?php echo $value->NAMAMHS; ?></td>
							<td align="center"><?php echo $value->NAMAKEL; ?></td>
							<td><?php echo $this->mfungsi->nilaihuruf($value->NILAIMHS); ?></td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>

<div class="row mt-5">
	<div class="col-12">
		<div class="col-5 offset-7">
			Pamekasan, . . . . . . . . . . . . . . . . . . . . . . . . . .  <?php echo date('Y'); ?>
		</div>
	</div>
	<div class="col-5 offset-7">
		<div class="font-weight-bold">
			KEPALA LPPM,
		</div><br><br>
		<div class="mt-5">
			( . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .)
		</div>
	</div>
</div>