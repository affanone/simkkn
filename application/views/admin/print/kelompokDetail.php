<div class="container mt-2">
	<?php 
	$this->load->view('dosen/print/kop');
?>
	<div class="mt-2 mb-2 text-center card-outline-secondary"><u>DATA KULIAH KERJA NYATA UNIVERSITAS MADURA<br>TAHUN AKADEMIK <?php echo $this->mfungsi->tahun()->label; ?></u></div>
			
			<div class="row">
				<div class="col-md-4 font-weight-bold">
  				Nama Kelompok KKN
  			</div>
  			<div class="col-md-8">
  				<?php echo $kelompok[0]->NAMAKEL; ?>
  			</div>
  			<div class="col-md-4 font-weight-bold">
  				Alamat Kelompok KKN
  			</div>
  			<div class="col-md-8">
  				<?php echo $kelompok[0]->ALAMATKEL; ?>
  			</div>
  			<div class="col-md-4 font-weight-bold">
  				Dosen Pendamping Lapangan
  			</div>
  			<div class="col-md-8">
  				<?php echo $kelompok[0]->NAMADPL; ?>
  			</div>
  			<div class="col-md-4 font-weight-bold">
  				KONTAK DPL
  			</div>
  			<div class="col-md-8">
  				<?php echo ($kelompok[0]->KONTAKDPL==NULL)?'-':$kelompok[0]->KONTAKDPL; ?>
  			</div>
  		</div>
			<table class="table table-sm table-bordered table-striped mt-2">
				<tdead>
					<tr>
						<th>No</th>
						<th>Npm</th>
						<th>Nama</th>
						<th>Fak / Prodi</th>
						<th>Alamat</th>
					</tr>
				</tdead>
				<tbody>
					<?php
					$i = 1;
						foreach ($mahasiswa as $key => $value) {
							?>
							<tr>
								<td><?php echo $i++; ?></td>
								<td><?php echo $value->NPM; ?></td>
								<td><?php echo $value->NAMAMHS; ?></td>
								<td><?php echo $value->NAMAPRODI; ?></td>
								<td><?php echo $value->ALAMATMHS; ?></td>
							</tr>
							<?php
						}
					?>
				</tbody>
			</table>	
<div class="row mt-5">
	<div class="col-12">
		<div class="col-5 offset-7">
			Pamekasan, . . . . . . . . . . . . . . . . . . . . . . . . . .  <?php echo date('Y'); ?>
		</div>
	</div>
	<div class="col-5 offset-7">
		<div class="font-weight-bold">
			KEPALA LPPM,
		</div><br><br>
		<div class="mt-5">
			( . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .)
		</div>
	</div>
</div>
</div>