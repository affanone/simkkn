<style type="text/css">
  body{
  background: rgba(0,0,0,0.9);
}
.formUpload{
  position: absolute;
  top: 50%;
  left: 50%;
  margin-top: -100px;
  margin-left: -250px;
  width: 500px;
  height: 200px;
  border: 4px dashed #fff;
}
.formUpload p{
  width: 100%;
  height: 100%;
  text-align: center;
  line-height: 170px;
  color: #ffffff;
  font-family: Arial;
}
.formUpload input{
  position: absolute;
  margin: 0;
  padding: 0;
  width: 100%;
  height: 100%;
  outline: none;
  opacity: 0;
}
.formUpload button{
  margin: 0;
  color: #fff;
  background: #16a085;
  border: none;
  width: 508px;
  height: 35px;
  margin-top: -20px;
  margin-left: -4px;
  border-radius: 4px;
  border-bottom: 4px solid #117A60;
  transition: all .2s ease;
  outline: none;
}
.formUpload button:hover{
  background: #149174;
  color: #0C5645;
}
.formUpload button:active{
  border:0;
}




</style>
<div class="row">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
          <div class="row">
            <div class="col">
              <form class="formUpload" action="upload.php" method="POST">
                <input autocomplete="off"  type="file" multiple>
                <p>Drag your files here or click in this area.</p>
                <button type="submit">Upload</button>
              </form> 
            </div>
          </div>
		  	</div>
		</div>
	</div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('.formUpload input').change(function () {
      $('.formUpload p').text(this.files.length + " file(s) selected");
    });
  });
</script>