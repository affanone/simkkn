<div class="row">
	<div class="col">
		<div class="card">
		  <div class="card-body">
		  	<div class="alert alert-warning" role="alert">
				  Proses penukaran anggota kkn akan menyebabkan ketidak sesuaian pada masing-masing batas kapasitas prodi, <i>(Recommended, gunakan fitur ini setelah pengelokasian anggota kkn selesai semua)</i>.
				</div>
		  	<h4 class="card-title">Tukar Anggota KKN</h4>
		  	<table class="table table-bordered">
		  		<tbody>
		  			<tr class="row">
		  				<td class="col-md-5">
		  					<form class="forms-sample xloading" action="" method="post">
		  						<div class="form-group">
                    <label for="kkndari">Dari Kelompok KKN</label>

                    <select onchange="getanggota(this,'#bodydarikel','dari');" class="form-control selectpicker" data-live-search="true" id="kkndari" name="kkndari">
                      <option disabled="" value="" selected=""> - Dari Kelompok Kkn -</option>
                      <?php
                      foreach ($kelompok as $key => $value) {
                          echo '<option data-tokens="'.$value->NAMAKEL.' '.$value->ALAMATKET.'" value="'.$value->KDKEL.'">'.$value->NAMAKEL.'</option>';
                        }
                      ?>
                      
                    </select>
                  </div>
                </form>
                <div class="dropdown-divider"></div>
                <div id="bodydarikel">
                	
                </div>
		  				</td>
		  				<td class="col-md-2 text-center">
		  					<button type="button" class="btn btn-sm btn-outline-primary" onclick="tukar()">Tukar</button>
		  				</td>
		  				<td class="col-md-5">
		  					<form class="forms-sample" action="" method="post">
		  						<div class="form-group">
                    <label for="kknke">Ke Kelompok KKN</label>

                    <select onchange="getanggota(this,'#bodykekel','ke');" class="form-control selectpicker" data-live-search="true" id="kknke" name="kknke">
                      <option disabled="" value="" selected=""> - Ke Kelompok Kkn -</option>
                      <?php
                      foreach ($kelompok as $key => $value) {
                          echo '<option data-tokens="'.$value->NAMAKEL.' '.$value->ALAMATKET.'" value="'.$value->KDKEL.'">'.$value->NAMAKEL.'</option>';
                        }
                      ?>
                      
                    </select>
                  </div>
                </form>
                <div class="dropdown-divider"></div>
                <div id="bodykekel">
                	
                </div>
		  				</td>
		  			</tr>
		  		</tbody>
		  	</table>
		  </div>
		</div>
	</div>
</div>
<script type="text/javascript">
		var dataDari = {};
		var dataKe = {};
	function getanggota(t,h,p){
		if(p=='dari'){
			dataDari = {
				kelompokDari : null,
				npmDari : null,
				htmlDari : null,
				_thisDari : null
			};
		}else if(p=='ke'){
			dataKe = {
				npmKe : null,
				kelompokKe : null,
				htmlKe : null,
				_thisKe :null
			};
		}

		_ajax({
			url:'<?php echo base_url('admin/anggota/getkelompok'); ?>',
			data:{
				kode:$(t).val(),
				tipe:p
			},
			success:function(res){
				$(h).html(res);
			},
			loading:h
		})
	}

	function mark(t,p){
		$('.ls-'+p).removeClass('bg-primary text-white');
		$(t).addClass('bg-primary text-white');
		if(p=='dari'){
			dataDari = {
				kelompokDari : $(t).attr('data-kelompok'),
				npmDari : $(t).attr('data-npm'),
				htmlDari : $(t).html(),
				_thisDari : t
			};
		}else if(p=='ke'){
			dataKe = {
				kelompokKe : $(t).attr('data-kelompok'),
				npmKe : $(t).attr('data-npm'),
				htmlKe : $(t).html(),
				_thisKe : t
			};
		}
	}

	function tukar(){
		if(dataDari.npmDari==null || dataKe.npmKe==null){
			_alert('Mahasiswa "dari" atau "Ke" belum dipilih!');
		}else{
			if(dataDari.kelompokDari != dataKe.kelompokKe){
				_ajax({
					url:'<?php echo base_url('admin/anggota/prosestukar'); ?>',
					data:{
						npmke:dataKe.npmKe,
						kelompokke:dataKe.kelompokKe,
						npmdari:dataDari.npmDari,
						kelompokdari:dataDari.kelompokDari,
					},
					success:function(res){
						$(dataKe._thisKe).html(dataDari.htmlDari);
						$(dataDari._thisDari).html(dataKe.htmlKe);
						$('.ls-ke[data-npm="'+dataKe.npmKe+'"]').attr('data-npm', dataDari.npmDari);
						$('.ls-dari[data-npm="'+dataDari.npmDari+'"]').attr('data-npm', dataKe.npmKe);
						var temp = {
							npm : dataDari.npmDari,
							html : dataDari.htmlDari
						};
						dataDari.htmlDari = dataKe.htmlKe;
						dataDari.npmDari = dataKe.npmKe;
						dataKe.htmlKe = temp.html;
						dataKe.npmKe = temp.npm;
					},
					loading:'.xloading'
				})
			}else{
				_alert('Kelompok KKN harus berbeda');
			}			
		}
	}
</script>