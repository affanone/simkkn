<div class="row">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
			  	<div class="row">
				  	<div class="col-md-8">
				  		<h4 class="card-title">Kelompok KKN Tahun Akademik <?php echo $this->mfungsi->tahun()->label; ?></h4>
				    	<h6 class="card-subtitle mb-2 text-muted">Kelompok-kelompok KKN dibawah ini yang akan diselenggarakan pada tahun akademik <?php echo $this->mfungsi->tahun()->label; ?></h6>
				    </div>
				    <div class="col-12 mb-3">
				    	<a class="float-right btn btn-outline-primary ml-2" onclick="_cetak(this)" href="<?php echo base_url('admin/kelompok/cetakall'); ?>"><i class="fa fa-print"></i>Cetak</a>
			    		<a class="float-right btn btn-outline-success mr-2" href="<?php echo base_url('admin/kelompok/arsipall'); ?>"><i class="fa fa-save"></i>Simpan</a>
				    	<a class="float-right btn btn-primary  mr-3" href="<?php echo base_url('admin/kelompok/tambah'); ?>"><i class="fa fa-plus"></i>Tambah</a>
				    </div>
				    <div class="col col-12">
				    	<table id="table" class="table table-bordered">
		                    <thead>
			                    <tr>
			                        <th>
			                        	#
			                        </th>
			                        <th>
			                        	Nama Kelompok
			                        </th>
			                        <th>
			                        	Kapasitas
			                        </th>
			                        <th>
			                        	DPL
			                        </th>
			                        <th>
			                        	Lokasi
			                        </th>
			                        <th>
			                        	Opsi
			                        </th>
			                    </tr>
		                    </thead>
		                    <tbody>
		                    	<?php
		                    	$i = 1;
		                    		foreach ($data as $key => $value) {
		                    			?>
			                    			<tr>
					                     		<td><?php echo $i++; ?></td>
					                     		<td><a title="Detail lengkap kelompok kkn" class="btn-link" href="<?php echo base_url('admin/kelompok/detail/'.$value->KDKEL); ?>"><?php echo $value->NAMAKEL; ?></a></td>
					                     		<td><?php echo ($value->KAPASITAS==null)?0:$value->KAPASITAS; ?></td>
					                     		<td><a href="<?php echo base_url('admin/pengguna/dosen/bio/'.$value->KDDPL); ?>" class="btn-link"><?php echo $value->NAMADPL.'<span class="text-muted"> ('.$value->KDDPL.')</span>'; ?></a></td>
					                     		<td><?php echo $value->ALAMATKEL; ?></td>
					                     		<td width="100px">
					                     			<a title="Edit" class="text-primary" href="<?php echo base_url('admin/kelompok/edit/'.$value->KDKEL); ?>"><i class="fa fa-pencil"></i></a>
					                     			&nbsp;.&nbsp;
					                     			<a title="Hapus" class="text-danger" onclick="hapus('<?php echo base_url('admin/kelompok/hapus/'.$value->KDKEL); ?>')" href="#!"><i class="fa fa-close"></i></a>
					                     		</td>
					                     	</tr>
		                    			<?php
		                    		}
		                    	?>
		                    </tbody>
		                  </table>
				    </div>
				</div>
		  	</div>
		</div>	
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable({scrollX:true});
	});

	function hapus(url){
		_alert({
			mode:'confirm',
			title:'Apakah akan dihapus?',
			msg:'Semua data yang berhubungan dengan data ini akan ikut terhapus, apakah akan dilanjutkan?',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				window.location = url;
			}
		})
	}
</script>