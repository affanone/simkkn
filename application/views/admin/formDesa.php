<div class="row xloading">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
		  		<div class="row justify-content-md-center">
				    <div class="col col-5">
                <h4 class="card-title">
                	<?php
                	if($tipe=='tambah')
                		echo 'Tambah Desa';
                	else if($tipe=='edit')
                		echo 'Edit Desa';
                	?>
                </h4>
                <p class="card-description">
                  Yang bertanda (*) harus diisi!
                </p>
                <?php
                	if($tipe=='tambah')
                		$act = base_url('admin/master/desa/simpan');
                	else if($tipe=='edit')
                		$act = base_url('admin/master/desa/update/'.$kode);
                ?>

                <form class="forms-sample" action="<?php echo $act; ?>" method="post">

                  <div class="form-group">
                    <label for="kabupaten">* Kabupaten <?php echo form_error('kabupaten','<small class="text-danger">','</small>'); ?></label>
                    <select onchange="loadKecamatan(this.value)" class="form-control" id="kabupaten" name="kabupaten">
                      <option value=""> - Pilih - </option>
                      <?php
                        foreach ($dataKab as $key => $value) {
                          echo '<option value="'.$value->KDKAB.'" '.(($value->KDKAB==$kabupaten) ? 'selected' : '').'>'.$value->NAMAKAB.'</option>';
                        }
                      ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="kecamatan">* Kecamatan <?php echo form_error('kecamatan','<small class="text-danger">','</small>'); ?></label>
                    <select class="form-control" id="kecamatan" name="kecamatan">
                      <option value="" selected disabled> - Pilih - </option>
                      <?php
                      console($proses);
                      if($proses==true){
                        foreach ($dataKec as $key => $value) {
                          echo '<option value="'.$value->KDKEC.'" '.(($value->KDKEC==$kecamatan) ? 'selected' : '').'>'.$value->NAMAKEC.'</option>';
                        }
                      }
                      ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>* Nama Desa <?php echo form_error('desa','<small class="text-danger">','</small>'); ?></label>
                    <input autocomplete="off"  type="text" class="form-control" value="<?php echo $desa; ?>"  placeholder="Nama Desa" name="desa">
                  </div>
                  
                  <button type="submit" class="btn btn-success mr-2">Simpan</button>
                  <a class="btn btn-light" href="<?php echo $kembali; ?>">Kembali</a>
                </form>
				    </div>
				  </div>
		  	</div>
		</div>
	</div>
</div>
<script type="text/javascript">
  function loadKecamatan(v){
    _ajax({
      url:'<?php echo base_url('admin/master/desa/loadkecamatan') ?>',
      data:{
        kabupaten:v
      },
      loading:'.xloading',
      success:function(data){
        $('#kecamatan').html(data);
      }
    })
  }
</script>