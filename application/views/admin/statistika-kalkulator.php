<form onsubmit="return terapkan(this)" action="<?php echo base_url('admin/statistika/pendaftar/terapkan'); ?>" method="post">
<div class="row">
	<div class="col-12">
		<div class="card">
		  <div class="card-body">
		    <h4 class="card-title">Hasil pembagian kelompok secara merata</h4>
		    <h6 class="card-subtitle mb-2 text-muted">Terapkan ke kelompok kkn tahun akademik <?php echo $this->mfungsi->tahun()->label; ?> (Rekomendasi)</h6>
		    <table class="table table-bordered xtable-sm table-hover xtable-responsive">
				  <thead>
				    <tr>
				      <th scope="col" class="font-weight-bold">#</th>
				      <th scope="col" class="font-weight-bold">Kel</th>
				      <?php
				      foreach ($prodilabel as $k => $v) {
				    		?>
				    			<th class="text-center font-weight-bold" data-toggle="tooltip" data-placement="top" title="<?php echo $prodilabel[$k]; ?>"><?php echo $k; ?></th>
				    		<?php
				    	}
				      ?>
				      <th class="text-center font-weight-bold">Total</th>
				    </tr>
				  </thead>
				  <tbody>
				  	<?php
				  	$i = 1;
				  	$jmlIsi = [];
				  	foreach ($kel as $key => $value) {
				  		?>
				  		<tr>
				  			<th scope="row"><?php echo $i++; ?></th>
					      <th scope="row" data-toggle="tooltip" data-placement="bottom" title="<?php echo $kellabel[$key]['alamat']; ?>"><?php echo $kellabel[$key]['nama']; ?></th>
					      <?php
					      	$kapasitas = 0;

					      	foreach ($prodilabel as $k => $v) {
					      		$kapasitas += $value[$k];
					      		if(!isset($jmlIsi[$k]))
					      			$jmlIsi[$k] = 0;
					      		$jmlIsi[$k] += $value[$k];
					      		?>
					      			<td class="text-center"><?php echo $value[$k]; ?><input autocomplete="off"  type="hidden" name="value[<?php echo $key; ?>][<?php echo $k; ?>]" value="<?php echo $value[$k]; ?>"></td>
					      		<?php
					      	}
					      ?>
					      <th class="text-center" ><?php echo $kapasitas; ?></th>
					    </tr>
				  		<?php
				  	}
				  	?>
				  </tbody>
				  <tfoot>
				  	<tr>
				  		<th  class="text-right"  colspan="2">
				  			Jumlah
				  		</th>
				  		<?php
				  		$total = 0;
				  		foreach ($jmlIsi as $key => $value) {
				  			$total += $value;
				  			?>
				  			<th class="text-center"> <?php echo $value; ?></th>
				  			<?php
				  		}
				  		?>
				  		<th class="text-center"><?php echo $total; ?></th>
				  	</tr>
				  </tfoot>
				</table>
		  </div>
		</div>
	</div>
	<div class="col-12 mt-3 text-right">
  	<button type="submit" class="btn float-right btn-primary pull-right float-left">Terapkan</button>
  </div>
</div>
</form>


		
<script type="text/javascript">
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip();
	})

	function terapkan(t){
		_alert({
			mode:'confirm',
			title:'Apakah akan diterapkan?',
			msg:'Kelompok kkn akan dibagi secara merata ',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				$(t).removeAttr('onsubmit');
				$(t).submit();
			}
		})
		return false;
	}
</script>