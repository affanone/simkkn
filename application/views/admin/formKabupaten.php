<div class="row">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
		  		<div class="row justify-content-md-center">
				    <div class="col col-5">
                  <h4 class="card-title">
                  	<?php
                  	if($tipe=='tambah')
                  		echo 'Tambah Kabupaten';
                  	else if($tipe=='edit')
                  		echo 'Edit Kabupaten';
                  	?>
                  </h4>
                  <p class="card-description">
                    Yang bertanda (*) harus diisi!
                  </p>
                  <?php
                  	if($tipe=='tambah')
                  		$act = base_url('admin/master/kabupaten/simpan');
                  	else if($tipe=='edit')
                  		$act = base_url('admin/master/kabupaten/update/'.$kode);
                  ?>

                  <form class="forms-sample" action="<?php echo $act; ?>" method="post">

                    <div class="form-group">
                      <label for="nama">* Nama Kabupaten <?php echo form_error('nama','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="text" class="form-control" id="nama" value="<?php echo $nama; ?>"  placeholder="Nama Kabupaten" name="nama">
                    </div>
                    
                    <button type="submit" class="btn btn-success mr-2">Simpan</button>
                    <a class="btn btn-light" href="<?php echo $kembali; ?>">Kembali</a>
                  </form>
				    </div>
				  </div>
		  	</div>
		</div>
	</div>
</div>