<div class="alert alert-info" role="alert">
  Data ini diambil langsung dari SIMAT UNIRA, dan data tidak dapat diedit, update, hapus, dan ditambah. Namun untuk memperbarui silahkan klik <a href="<?php echo base_url('admin/master/prodi/update'); ?>" class="alert-link">disini</a>
</div>
<div class="card">
  <div class="card-body">
  	<h4 class="card-title">Data Program Studi Universitas Madura</h4>
    <table class="table datatable table-bordered table-striped">
    	<thead>
    		<tr>
    			<th>#</th>
    			<th>Kode</th>
    			<th>Fakultas</th>
    			<th>Prodi</th>
    		</tr>
    	</thead>
    	<tbody>
    		<?php
    			$i = 1;
    			foreach ($prodi as $key => $value) {
    				?>
    				<tr>
    					<td><?php echo $i++; ?></td>
		    			<td><?php echo $value->KDPRODI; ?></td>
		    			<td><?php echo $value->FAKPRODI; ?></td>
		    			<td><?php echo $value->NAMAPRODI; ?></td>
		    		</tr>
    				<?php
    			}
    		?>
    	</tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.datatable').DataTable({
            scrollX: true,
            paging:false,
            info:false
        });
    });
</script>