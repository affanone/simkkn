<div class="row">
	<div class="col">
		<div class="card">
	  	<div class="card-body">
	  		<h4 class="card-title">Data Statis / Kemajuan Kinerja KKN</h4>
	  		<div class="row">
	  			<div class="col-12">
	  				<?php if(count($duplikat)>0){ ?>
	  				<div class="alert alert-danger" role="alert">
						  <strong>Duplikat ditemukan</strong> <?php echo count($duplikat); ?> File duplikat yang diunggah oleh peserta KKN, tangani hal ini agar ruang penyimpanan dapat dihemat sebesar <?php echo $hemat; ?> Mb.
						</div>
						<?php } ?>
						<div class="row">
							<div class="col-12 mb-3">
								<div class="card border border-secondary">
  								<div class="card-body">
  									<h5 class="text-muted">Tangani file yang diunggah</h5><br>
										<button <?php echo (count($belum)>0)?'class="btn btn-sm btn-primary" onclick="konfirmasi(\'Apakah data belum diajukan akan dihapus?\',\'belum\',this);"':'class="disabled btn btn-sm btn-primary"'; ?> >Hapus Belum Diajukan</button>
										<button <?php echo (count($diajukan)>0)?'class="btn btn-sm btn-warning" onclick="konfirmasi(\'Apakah data yang sedang diajukan akan dihapus?\',\'diajukan\',this);"':'class="disabled btn btn-sm btn-warning"'; ?> >Hapus Diajukan</button>
										<button <?php echo (count($diterima)>0)?'class="btn btn-sm btn-success" onclick="konfirmasi(\'Apakah data yang sudah diterima oleh LPPM akan dihapus?\',\'diterima\',this);"':'class="disabled btn btn-sm btn-success"'; ?>  >Hapus Diterima</button>
										<button <?php echo (count($duplikat)>0)?'class="btn btn-sm btn-danger" onclick="konfirmasi(\'Apakah data yang duplikat akan dihapus?\',\'duplikat\',this);"':'class="disabled btn btn-sm btn-danger"'; ?> >Hapus Duplikat</button>
										<button <?php echo (count($monitoring)>0)?'class="btn btn-sm btn-secondary" onclick="konfirmasi(\'Apakah data monitoring DPL akan dihapus?\',\'monitoring\',this);"':'class="disabled btn btn-sm btn-secondary"'; ?> >Hapus Monitoring DPL</button>
									</div>
								</div>
							</div>
						</div>
	  				<table id="datatabel" class="table table-bordered table-hover">
	  					<thead class="text-center text-muted">
	  						<tr>
	  							<th rowspan="2" class="text-left">Kelompok</th>
	  							<th colspan="6">Total</th>
	  							<th rowspan="2"></th>
	  						</tr>
	  						<tr>
	  							<th>Unggah Peserta</th>
	  							<th>Unggah Monitoring</th>
	  							<th>Total Ukuran Unggah(Mb)</th>
	  							<th>Diajukan</th>
	  							<th>Diterima</th>
	  							<th>Anggota</th>
	  						</tr>
	  					</thead>
	  					<tbody>
	  						<?php
	  						$total = array(
	  							'upload'=>0,
	  							'size'=>0,
	  							'pengajuan'=>0,
	  							'diterima'=>0,
	  							'anggota'=>0,
	  							'monitor'=>0
	  						);
	  						foreach ($kemajuan as $key => $value) {
	  							?>
	  							<tr class="text-center">
	  								<td class="text-left"><?php echo $value['nama']; ?></td>
	  								<td><?php echo $value['upload']; ?></td>
	  								<td><?php echo $value['monitor']; ?></td>
	  								<td><?php echo $this->mfungsi->koversiSize($value['size'],'mb'); ?></td>
	  								<td><?php echo $value['pengajuan']; ?></td>
	  								<td><?php echo $value['diterima']; ?></td>
	  								<td><?php echo $value['anggota']; ?></td>
	  								<td><a href="<?php echo base_url('admin/statistika/kinerja/detail/').$value['kode']; ?>" class="btn-link" data-toggle="tooltip" data-placement="left" title="Selengkapnya"><i class="fa fa-arrow-right"></i></a></td>
	  							</tr>
	  							<?php
	  							$total['upload'] += $value['upload'];
	  							$total['size'] += $value['size'];
	  							$total['pengajuan'] += $value['pengajuan'];
	  							$total['diterima'] += $value['diterima'];
	  							$total['anggota'] += $value['anggota'];
	  							$total['monitor'] += $value['monitor'];
	  						}
	  						?>
	  					</tbody>
	  					<tfoot>
	  						<tr class="text-center">
	  							<th></th>
	  							<th><?php echo $total['upload']; ?></th>
	  							<th><?php echo $total['monitor']; ?></th>
	  							<th><?php echo $this->mfungsi->koversiSize($total['size'],'mb'); ?></th>
	  							<th><?php echo $total['pengajuan']; ?></th>
	  							<th><?php echo $total['diterima']; ?></th>
	  							<th><?php echo $total['anggota']; ?></th>
	  							<th></th>
	  						</tr>
	  					</tfoot>
	  				</table>
	  			</div>
	  		</div>
	  	</div>
	  </div>
	</div>
</div>
<?php
?>
<script type="text/javascript">
	$(document).ready(function($) {
		$('#datatabel').DataTable({scrollX: true});	
	});
	
	function editValue(kel,prod,val,isi){
		// console.log(kel)
		// console.log(prod)
		// console.log(val)
		// console.log(isi)
	}

	function openkalkulator(){
		$('#modalkalkulator').modal('toggle');
	}

	function hitungkalkulator(t){
		_ajax({
			url:$(t).attr('action'),
			data:$(t).serialize(),
			loading:'.modal-content',
			success:function(res){
				$('.xcalculator').html(res)
			},
			before:function(){
				$('.xcalculator').html('')
			}
		})
		return false;
	}

	function konfirmasi(m,f,t){
		_alert({
			mode:'confirm',
			title:'Konfirmasi',
			msg:m,
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				_progress({
					url:'<?php echo base_url('admin/statistika/kinerja/hapus/') ?>'+f,
					start:function(){
						$(t).text('0%');
					},
					running:function(res){
						$(t).text(res.proses+'%');
					},
					end:function(){
						$(t).text('Hampir selesai');
						window.location.href = '<?php echo base_url('admin/statistika/kinerja') ?>';
					}
				})
			}
		})
	}
</script>