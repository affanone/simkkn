<div class="row">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
		  		<div class="row justify-content-md-center">
				    <div class="col col-md-5">
                  <h4 class="card-title">
                  	<?php
                  	if($tipe=='tambah')
                  		echo 'Tambah Tahun Akademik';
                  	else if($tipe=='edit')
                  		echo 'Edit Tahun Akademik';
                  	?>
                  </h4>
                  <p class="card-description">
                    Yang bertanda (*) harus diisi!
                  </p>
                  <?php
                  	if($tipe=='tambah')
                  		$act = base_url('admin/master/tahun/simpan');
                  	else if($tipe=='edit')
                  		$act = base_url('admin/master/tahun/update/'.$kode);
                  ?>

                  <form class="forms-sample" action="<?php echo $act; ?>" method="post">
                    <div class="form-group">
                      <label for="labeltahun">* Label Tahun Akademik<?php echo form_error('label','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="text" class="form-control" id="labeltahun" value="<?php echo $label; ?>"  placeholder="Label Tahun" name="label">
                    </div>
                    
                    <button type="submit" class="btn btn-success mr-2">Simpan</button>
                    <a class="btn btn-light" href="<?php echo $kembali; ?>">Kembali</a>
                  </form>
				    </div>
				  </div>
		  	</div>
		</div>
	</div>
</div>