<div class="row">
	<div class="col">
		<div class="card">
	  	<div class="card-body">
	  		<h4 class="card-title">Data Statis / Kemajuan Pendaftaran KKN</h4>
	  		<div class="row">
	  			<!-- <div class="col-12">
	  				<a href="#!" data-toggle="tooltip" data-placement="left" title="Alat kalkulator pembagian anggota" class="mb-3 pull-right btn btn-outline-primary" onclick="openkalkulator();"><i class="fa fa-calculator"></i></a>
	  			</div> -->
	  			<div class="col-12">
		  			<?php if($kapasitas>0){ ?>	
				  		<table class="table table-bordered xtable-sm table-hover xtable-responsive" id="datatabel">
							  <thead>
							    <tr>
							      <th scope="col" class="font-weight-bold">#</th>
							      <th scope="col" class="font-weight-bold">Kel</th>
							      <?php
							      if(count($data)>0)
								      foreach ($prodi as $k => $v) {
							      		?>
							      			<th class="text-center font-weight-bold" data-toggle="tooltip" data-placement="top" title="<?php echo $v->NAMAPRODI; ?>"><?php echo $v->KDPRODI; ?></th>
							      		<?php
							      	}
							      ?>
							      <th class="text-center font-weight-bold" scope="col">Total</th>
							      <th class="text-center font-weight-bold" scope="col"></th>
							    </tr>
							  </thead>
							  <tbody>
							  	<?php
							  	$i = 1;
							  	$jmlKapasitas = [];
								  $jmlIsi = [];
							  	foreach ($data as $key => $value) {
							  		?>
							  		<tr>
							  			<th scope="row"><?php echo $i++; ?></th>
								      <th scope="row" data-toggle="tooltip" data-placement="left" title="<?php echo $value['alamat']; ?>"><?php echo $value['kelompok']; ?></th>
								      <?php
								      	$isi = 0;
								      	$kapasitas = 0;
								      	$j=0;
								      	foreach ($prodi as $k => $v) {
								      		$isi += $value['prodi'][$v->KDPRODI]['isi'];
								      		$kapasitas +=  $value['prodi'][$v->KDPRODI]['kapasitas'];
								      		if(!isset($jmlIsi[$j])){
								      			$jmlIsi[$j] = 0;
								      			$jmlKapasitas[$j] = 0;
								      		}
								      		?>
								      			<td class="text-center" ondblclick="editValue('<?php echo $key; ?>','<?php echo $value['prodi'][$v->KDPRODI]['kode']; ?>','<?php echo  $value['prodi'][$v->KDPRODI]['kapasitas']; ?>','<?php echo  $value['prodi'][$v->KDPRODI]['isi']; ?>')">

								      				<?php 
								      					echo  $value['prodi'][$v->KDPRODI]['isi']; //.'/'. $value['prodi'][$v->KDPRODI]['kapasitas']; 
								      				?>
								      					
								      				</td>
								      		<?php
								      		$jmlIsi[$j] = $jmlIsi[$j]+ $value['prodi'][$v->KDPRODI]['isi'];
								      		$jmlKapasitas[$j] = $jmlKapasitas[$j]+ $value['prodi'][$v->KDPRODI]['kapasitas'];
								      		$j++;
								      	}
								      	
								      ?>
								      <th class="text-center" >

								      	<?php 
								      		echo $isi;//.'/'.$kapasitas; 
								      	?>
								      	
								      </th>
								      <td><a href="<?php echo base_url('admin/kelompok/detail/'.$value['kode']);  ?>" data-toggle="tooltip" data-placement="left" title="Lihat rincian"><i class="fa fa-arrow-right"></i></a></td>
								    </tr>
							  		<?php
							  	}
							  	?>
							  </tbody>
							  <tfoot>
							  	<tr>
							  		<th  class="text-right"  colspan="2">
							  			Jumlah
							  		</th>
							  		<?php
							  		$isi = 0;
								    $kapasitas = 0;
							  		foreach ($jmlIsi as $key => $value) {
							  			$isi += $jmlIsi[$key];
								      $kapasitas += $jmlKapasitas[$key];
							  			?>
							  			<th class="text-center"> 

							  				<?php 
							  					echo $jmlIsi[$key];//.'/'.$jmlKapasitas[$key]; 
							  				?>

							  			</th>
							  			<?php
							  		}
							  		?>
							  		<th class="text-center">

							  			<?php 
							  				echo $isi;//.'/'.$kapasitas; 
							  			?>
							  				
							  		</th>
							  		<th></th>
							  	</tr>
							  </tfoot>
							</table>
						<?php }else{ ?>
							<div class="alert alert-warning">
								KAPASITAS BELUM DITENTUKAN, SILAHKAN TENTUKAN TERLEBIH DAHULU
							</div>
						<?php } ?>
						<div class="modal fade" id="modalkalkulator" tabindex="-1" role="dialog">
						  <div class="modal-dialog modal-lg" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title">Kalkulator Pembagian Anggota</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						        <div class="row">
						        	<div class="col-12">
						        		<div class="alert alert-info" role="alert">
												  Tentukan jumlah mahasiswa masing-masing prodi yang memprogram kkn 
												</div>	
													<form onsubmit="return hitungkalkulator(this)" action="<?php echo base_url('admin/statistika/pendaftar/hitung') ?>" method="post">
														<div class="row">
																<?php
																foreach ($prodi as $key => $value) {
																	?>
																	<div class="col-3">
																		<div class="form-group row">
																	    <label class="col-9 col-form-label"><?php echo $value->NAMAPRODI; ?></label>
																	    <div class="col-3">
																	      <input autocomplete="off"  type="text" name="prodi[<?php echo $value->KDPRODI; ?>]" value="<?php echo $valprodi[$value->KDPRODI]; ?>" placeholder="0" class="text-center form-control"value="">
																	    </div>
																	  </div>
																	</div>
																	<?php
																}
																?>
																<div class="col-12 text-right">
															  	<button type="submit" class="btn float-right btn-primary pull-right float-left">Hitung</button>
															  </div>
														</div>
													</form>
						        	</div>
						        	<div class="col-12 mt-3 xcalculator">
						        		 
						        	</div>
						        </div>
						      </div>
<!-- 						      <div class="modal-footer">
						        <button type="button" class="btn btn-primary">Save changes</button>
						        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						      </div> -->
						    </div>
						  </div>
						</div>



	  			</div>
	  		</div>
	  	</div>
	  </div>
	</div>
</div>
<?php
?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#datatabel').DataTable({
			scrollX: true,
			paging:false
		});	
	});
	
	function editValue(kel,prod,val,isi){
		// console.log(kel)
		// console.log(prod)
		// console.log(val)
		// console.log(isi)
	}

	function openkalkulator(){
		$('#modalkalkulator').modal('toggle');
	}

	function hitungkalkulator(t){
		_ajax({
			url:$(t).attr('action'),
			data:$(t).serialize(),
			loading:'.modal-content',
			success:function(res){
				$('.xcalculator').html(res)
			},
			before:function(){
				$('.xcalculator').html('')
			}
		})
		return false;
	}
</script>