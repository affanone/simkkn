<div id="data-mahasiswa"><div class="row">
	<div class="col">
		<div class="card" id="containermahasiswa">
		  	<div class="card-body">
			  	<div class="row">
				  	<div class="col-md-8">
				  		<h4 class="card-title">Data Mahasiswa Mendaftar</h4>
				    	<h6 class="card-subtitle mb-2 text-muted">Data mahasiswa yang mendaftar</h6>
				    </div>
				    <div class="col-md-4 text-right">
				    	<div class="btn-group"style="margin-bottom: 15px;" role="group" aria-label="First group">
                <a onclick="return hapusall(this)" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Hapus Semua" href="<?php echo base_url('admin/reg/hapusall'); ?>"><i class="fa fa-trash"></i></a>
                 <a onclick="return konfirmall(this)" href="<?php echo base_url('admin/reg/konfirmall'); ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Konfirmasi Semua"><i class="fa fa-check"></i></a>
                <!-- <a href="#!" class="btn btn-primary"  onclick="api()" data-toggle="tooltip" data-placement="bottom" title="Download data dari SIMAT"><i class="fa fa-cloud-download"></i></a> -->
              </div>	
				    </div>
				    <div class="col-12">
				    	<form class="form-inline float-md-right mb-2" action="<?php echo base_url('admin/reg/data') ?>" method="post" id="formcari">
							  <div class="input-group  mr-sm-2">
							    <input autocomplete="off"  type="text" name="cari" value="<?php echo $cari; ?>" class="form-control" placeholder="Cari">
							  </div>


							  <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
							</form>
				    </div>
				    <div class="col col-12">
									<table id="table" class="table  datatable table-sm table-bordered table-hover">
	                  <thead>
	                    <tr>
	                        <th>
	                        	#
	                        </th>
	                        <th>
	                        	Tgl Reg
	                        </th>
	                        <th>
	                        	NPM
	                        </th>
	                        <th>
	                        	Nama Lengkap
	                        </th>
	                        <th>
	                        	Fakultas / Prodi
	                        </th>
	                        <th>
	                        	Alamat
	                        </th>
	                        <th>
	                        	Opsi
	                        </th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  	<?php
	                  		$i = $nomor;
	                  		foreach ($data as $key => $value) {
	                  			?>
	                    			<tr>
			                     		<td><?php echo $i++; ?></td>
			                     		<td><?php echo $this->mfungsi->tgl($value->TGLREGMHS,true); ?></td>
			                     		<td><?php echo $value->NPM; ?></td>
			                     		<td><?php echo $value->NAMAMHS; ?></td>
			                     		<td><?php echo $value->NAMAPRODI; ?></td>
			                     		<td><?php echo $value->ALAMATMHS; ?></td>
			                     		<td width="50px">
			                     			<a title="Validasi" class="text-primary" onclick="return konfirm(this)" href="<?php echo base_url('admin/reg/konfirm/'.$value->NPM); ?>"><i class="fa fa-check"></i></a>
			                     			&nbsp;.&nbsp;
			                     			<a title="Hapus" class="text-danger" onclick="return hapus(this)" href="<?php echo base_url('admin/reg/hapus/'.$value->NPM); ?>"><i class="fa fa-close"></i></a>
			                     		</td>
			                     	</tr>
	                  			<?php
	                  		}
	                  	?>
	                  </tbody>
	                </table>
				    			<i>Total data : <?php echo $total; ?></i>
				    </div>
				</div>
		  	</div>

		  	<?php
			  	//if($cari == ''){

			  		echo $this->mpaging->set(array(
							'url'=>base_url('admin/reg/data/'),
							'uri'=>4,
							'count'=>$total,
							'attribute'=>array('class' => 'page-link','onclick'=>'return paging(this)','base-url'=>base_url('admin/reg/data/'))
						));
			  	//}
		  	?>
		</div>	
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		$('.datatable').DataTable({
			scrollX:true,
			searching:false,
			paging:false,
			info:false
		})
	});
	function konfirm(t){
		_alert({
			mode:'confirm',
			title:'Apakah data akan dikonfirmasi?',
			msg:'',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				window.location.href = $(t).attr('href');
			}
		})
		return false;
	}

	function konfirmall(t){
		_alert({
			mode:'confirm',
			title:'Apakah data akan dikonfirmasi?',
			msg:'',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				window.location.href = $(t).attr('href');
			}
		})
		return false;
	}

	function hapus(t){
		_alert({
			mode:'confirm',
			title:'Apakah data akan hapus?',
			msg:'',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				window.location.href = $(t).attr('href');
			}
		})
		return false;
	}

	function hapusall(t){
		_alert({
			mode:'confirm',
			title:'Apakah data akan hapus?',
			msg:'',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				window.location.href = $(t).attr('href');
			}
		})
		return false;
	}
</script>
</div>