<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BioModel extends CI_Model {

	public function get(){
		$this->db->where('KDDPL',$this->session->usern);
		return $this->db->get('dosen');
	}

	public function update($data){
		$this->db->set('KONTAKDPL',$data['kontak']);
		$this->db->set('ALAMATDPL',$data['alamat']);
		$this->db->set('NAMADPL',$data['nama']);
		$this->db->where('KDDPL',$this->session->usern);
		$this->db->update('dosen');
	}

	public function updatefoto($foto){
		$this->db->set('FOTODPL',$foto);
		$this->db->where('KDDPL',$this->session->usern);
		$this->db->update('dosen');
	}

	public function defaultfoto(){
		$this->db->set('FOTODPL',$this->session->fotoorigin);
		$this->db->where('KDDPL',$this->session->usern);
		$this->db->update('dosen');
	}

	public function ambilfoto(){
		$this->db->select('FOTODPL');
		$this->db->where('KDDPL',$this->session->usern);
		return $this->db->get('dosen')->result()[0]->FOTODPL;
	}

}

/* End of file bioModel.php */
/* Location: ./application/models/dosen/bioModel.php */