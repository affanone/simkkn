<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NonordikModel extends CI_Model {

	public function get(){
		return $this->db->get('no_ordik');
	}

	public function hapus($d){
		$this->db->where('NPMORD',$d);
		$this->db->delete('no_ordik');
	}

}

/* End of file kknModel.php */
/* Location: ./application/models/camat/kknModel.php */