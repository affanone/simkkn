<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengaturanModel extends CI_Model {

	public function setNew($n,$v=''){
		$this->db->where('JENISSET',$n);
		$this->db->from('pengaturan');
		$cek = $this->db->count_all_results(); 
		if($cek==0)
			$this->created($n,$v);
	}

	public function created($n,$v){
		$this->db->insert('pengaturan',array('JENISSET'=>$n,'VALUESET'=>$v));
	}

	public function update_pesan($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','biro_pesan');
		$this->db->update('pengaturan');
	}

	public function update_password($n){
		$this->db->set('PASSN',MD5($n));
		$this->db->where('USERN',$this->session->usern);
		$this->db->where('STATUS',3);
		$this->db->update('users');
	}

	public function update_pengaturan($key,$val){
		$this->db->set('VALUESET',$val);
		$this->db->where('JENISSET',$key);
		$this->db->update('pengaturan');
	}
}

/* End of file uploadModel.php */
/* Location: ./application/models/camat/uploadModel.php */