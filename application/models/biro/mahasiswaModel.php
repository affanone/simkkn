<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MahasiswaModel extends CI_Model {

	public function get(){
		$this->db->select('NPMORD');
		$n = $this->db->get('no_ordik');
		$v = array();
		foreach ($n->result() as $key => $value) {
			array_push($v, $value->NPMORD);
		}
		return $v;
	}

	public function tambah($d){
		$data = array(	
			'NPMORD'=>$d['npm'],
			'NAMAORD'=>$d['nama'],
			'HPORD'=>$d['hp'],
			'ALAMATORD'=>$d['alamat'],
		);
		$cek = $this->db->insert_string('no_ordik',$data);
		$insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $cek);
		$this->db->query($insert_query);
	}

	public function hapus($d){
		$this->db->where('NPMORD',$d);
		$this->db->delete('no_ordik');
	}
}

/* End of file nilaiModel.php */
/* Location: ./application/models/camat/nilaiModel.php */