<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BioModel extends CI_Model {

	public function get(){
		$this->db->where('KDDPL',$this->session->usern);
		return $this->db->get('dosen');
	}

	public function update($data){
		$this->db->set('KONTAKDPL',$data['kontak']);
		$this->db->set('ALAMATDPL',$data['alamat']);
		$this->db->set('NAMADPL',$data['nama']);
		$this->db->where('KDDPL',$this->session->usern);
		$this->db->update('dosen');
	}

}

/* End of file bioModel.php */
/* Location: ./application/models/camat/bioModel.php */