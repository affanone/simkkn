<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mio extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}


	public function upload($config=[]){
		$conf['upload_path'] = (!isset($config['path']))? './' : './'.$config['path'];
		$conf['allowed_types'] = (!isset($config['format']))?'gif|jpg|png':$config['format'];
		$conf['max_size'] = (!isset($config['size']))?1000:$config['size'];
		$conf['max_width'] = (!isset($config['width']))?1024:$config['width'];
		$conf['max_height'] = (!isset($config['height']))?768:$config['height'];
		$conf['file_name'] = (!isset($config['filename']))? date('YmdHis').'.'.explode('|', $conf['allowed_types'])[0] :$config['filename'];
		$conf['overwrite'] = (!isset($config['overwrite']))?false:$config['overwrite'];
		$openerr = (!isset($config['openerr']))?'':$config['openerr'];
		$closeerr = (!isset($config['closeerr']))?'':$config['closeerr'];
		$name = (!isset($config['name']))?'myFILE':$config['name'];
        $this->load->library('upload', $conf);
        if ( ! $this->upload->do_upload($name))
        {
            $data = $this->upload->display_errors($openerr,$closeerr);
            $status = false;
        }
        else
        {
            $data = $this->upload->data();
            $status = true;
        }
        return array(
        	'data' => $data,
        	'status' => $status
        );
	}

	public function download($path = '', $data = null){
		$this->load->helper('download');
		force_download($path, $data);
	}

}

/* End of file mupload.php */
/* Location: ./application/models/mupload.php */