<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapi extends CI_Model {

	public function request($config = []){
    $_postData = '';
    $_param = (!isset($config['params']))?[]:$config['params'];
    $_url = (!isset($config['url']))?'':$config['url'];
    $_method = (!isset($config['method']))?'GET':$config['method'];
    $_headers = (!isset($config['headers']))?[]:$config['headers'];
    foreach($_param as $k => $v) 
    { 
      $_postData .= $k . '='.$v.'&'; 
    }
    rtrim($_postData, '&');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE); 
    curl_setopt($ch, CURLOPT_HTTPHEADER, $_headers); 
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $_method);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $_postData);    
    $output=curl_exec($ch);
    curl_close($ch);
    $lvl1 = json_decode($output,true);
    $lvl2 = json_decode(json_encode($lvl1));
    return $lvl2;
  }

}

/* End of file mapi.php */
/* Location: ./application/models/helpers/mapi.php */