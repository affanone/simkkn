<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mnotif extends CI_Model {

	public function get($jml){
		$this->db->order_by('STATUSNOTIF DESC,TGLNOTIF DESC');
		if($jml==true)
			return $this->db->get('notifikasi')->result();
		else
			return $this->db->get('notifikasi',$jml)->result();
	}

	public function tambah($data){
		$data = array(
			'KDNOTIF'=>$data['id'],
			'TGLNOTIF'=>$data['tgl'],
			'KETNOTIF'=>$data['keterangan'],
			'URLNOTIF'=>$data['url'],
			'STATUSNOTIF'=>1,
		);
		$this->db->insert('notifikasi',$data);
	}

	public function batal($data){
		$this->db->where('KDNOTIF',$data);
		$this->db->or_where('KETNOTIF',$data);
		$this->db->delete('notifikasi');
	}

	public function baca($data){
		$this->db->where('KDNOTIF',$data);
		$this->db->set('STATUSNOTIF',0);
		$this->db->update('notifikasi');
	}

	public function kosongkan(){
		$this->db->empty_table('notifikasi');
	}

}

/* End of file mnotif.php */
/* Location: ./application/models/helpers/mnotif.php */