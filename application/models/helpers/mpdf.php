<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpdf extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function generate($config = []){
		$this->load->library('pdf');
		$konten = (!isset($config['content']))?'view':$config['content'];
		$data = (!isset($config['data']))?[]:$config['data'];
		$file = (!isset($config['filename']))?"Data".date("Y-m-d H:i:s").".pdf":$config['filename'].'.pdf';
		$this->pdf->load_view($konten,$data);
		
		 
	}

	public function view($path){
		header('Content-Type: application/pdf');
		header('Content-Destintion: inline; filename="'.basename($path).'"');
		header('Content-Transfer-Encoding: binary');
		header('Content-Ranges: bytes');
		@readfile($path);
	}

}

/* End of file mpdf.php */
/* Location: ./application/models/mpdf.php */