<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfile extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function exists($file = '/myfile.txt'){
		return file_exists(FCPATH.$file);
	}

	public function del_file($file = '/myfile.txt'){
		if ( ! @unlink(FCPATH.$file))
		{
		    return false;
		}
		else
		{
		    return true;
		}
	}

	public function copy_file($file = '/myfile.txt',$newfile = '/myfile2.txt'){
		if ( ! @copy(FCPATH.$file,FCPATH.$newfile))
		{
		    return false;
		}
		else
		{
		    return true;
		}
	}

	public function del_dir($dir = '/folder'){
		if (!file_exists($dir)) {
        return true;
    }

    if (!is_dir($dir)) {
        return unlink($dir);
    }

    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') {
            continue;
        }

        if (!$this->del_dir($dir . DIRECTORY_SEPARATOR . $item)) {
            return false;
        }

    }

    return rmdir($dir);
	}

	public function empty_dir($path = '/folder'){
		if ( ! delete_files(FCPATH.$path,true))
		{
		    return false;
		}
		else
		{
		    return true;
		}
	}

	public function read($file = 'myfile.txt'){
		return read_file(base_url($file));
	}

	public function write($config = []){
		$append = (!isset($config['append']))?false:$config['append'];
		$file = (!isset($config['file']))?'/example.txt':$config['file'];
		$data = (!isset($config['data']))?'it is example a data':$config['data'];
		$opsi = (!isset($config['method']))?'a+':$config['method'];
		if($append==true){
			if ( ! write_file($file , $data.(($append==true)?"\n":''),$opsi))
			{
			    return false;
			}
			else
			{
			    return true;
			}
		}else{
			if ( ! write_file($file , $data.(($append==true)?"\n":'')))
			{
			    return false;
			}
			else
			{
			    return true;
			}
		}
	}

}

/* End of file mfile.php */
/* Location: ./application/models/mfile.php */