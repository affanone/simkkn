<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcaptcha extends CI_Model {
	public function set($config = []){
		$word = $this->izrand();
		$path = (!isset($config['path']))?'./captcha/':$config['path'];
		$img = (!isset($config['imgurl']))?'captcha/':$config['imgurl'];
		$imgid = (!isset($config['id']))?'Imageid':$config['id'];
		$this->session->captcha = $word;
		$vals = array(
		        'word'          => $word,
		        'img_path'      => $path,
		        'img_url'       => base_url($img),
		        'img_width'     => 120,
		        'img_height'    => 30,
		        'expiration'    => 200,
		        'word_length'   => 5,
		        'font_size'     => 20,
		        'img_id'        => $imgid,
		        'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

		        // White background and border, black text and red grid
		        'colors'        => array(
		                'background' => array(255, 255, 255),
		                'border' => array(255, 255, 255),
		                'text' => array(0, 0, 0),
		                'grid' => array(212, 212, 212)//array(255, 255, 255)//array(255, 40, 40)
		        )
		);

		$cap = create_captcha($vals);
		return $cap['image'];
	}

	private function izrand($length = 5) {
		$characters = 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ0123456789';
        $string = '';
		$max = strlen($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
		     $string .= $characters[mt_rand(0, $max)];
		}
		return $string;
	}

	public function cek($n=''){
		return ($n===$this->session->captcha)?true:false;
	}

}

/* End of file mcaptcha.php */
/* Location: ./application/models/mcaptcha.php */