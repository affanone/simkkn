<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mhistory extends CI_Model {

	public function go(){
		$url = base_url(uri_string());
		$this->session->set_userdata('pageURL', $url);

	}

	public function back(){
		$url = $this->session->userdata('pageURL');
		return $url;
	}

	public function locked(){
		$this->load->view('kunci',array(
			'back'=>$this->back(),
			'title'=>'Kesalahan',
			'message'=>'Password yang anda masukkan salah'
		));
	}

}

/* End of file mhistory.php */
/* Location: ./application/models/mhistory.php */