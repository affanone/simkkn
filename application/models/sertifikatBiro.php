<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SertifikatBiro extends CI_Model {

	public function mahasiswa_api($npm){
		$url = 'https://api.unira.ac.id/v1/mahasiswa?filter[nim]='.$npm;
		$data = json_decode($this->mfungsi->file_get($url));
		if(!isset($data->data)){
			$res = new stdClass();
			$res->status = false;
			$res->pesan = 'Data tidak ditemukan';
			return $res;
		}else{
			$res = new stdClass();
			$res->status = true;
			$res->data = $data->data[0];
			return $res;
		}
	}

	public function mahasiswa_api_all(){
		$url = 'https://api.unira.ac.id/v1/mahasiswa?limit=50000000';
		$url = './mahasiswa_api_unira_all.json';
		$data = json_decode($this->mfungsi->file_get($url));
		return $data->data;
	}

	public function angkatan($npm){
		$a = intval(substr($npm, 0,4));
		if($a<2012){
			return intval('20'.substr($a, 0,2));
		}else{
			return intval($a);
		}
	}

	public function prodi($npm){
		$a = intval(substr($npm, 0,4));
		if($a<2012){
			return intval(substr($npm, 3,2));
		}else{
			return intval(substr($npm, 4,2));
		}
	}

	public function prodi_nama($kode){
		$this->db->where('KDPRODI',$kode);
		$data = $this->db->get('prodi')->result();
		if(count($data)>0){
			return $data[0]->FAKPRODI.' / '.$data[0]->NAMAPRODI;
		}else{
			return 'Unknow / Unknow';
		}
	}

	private function tambahNol($n){
		$n =  intval($n);
		if($n<10)
			return '00'.$n;
		else if($n<100)
			return '0'.$n;
		else
			return $n;
	}

	public function ambilalfabeta($n){
		$filter = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$n = strtoupper($n);
		$gbg = '';
		for ($i=0; $i < strlen($n); $i++) { 
			if(in_array($n[$i], $filter)){
				$gbg .= $n[$i];
			}
		}
		return $gbg;
	}

}

/* End of file sertifikatBoro.php */
/* Location: ./application/models/sertifikatBoro.php */