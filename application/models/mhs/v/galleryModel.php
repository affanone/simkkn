<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GalleryModel extends CI_Model {

	public function view(){
		$npm = $this->session->usern;
		$this->db->join('kelompok_peserta','kelompok_peserta.NPM = album.NPM');
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN AND STATUSTAHUN = 1');
		$this->db->where('album.NPM',$npm);
		if(isset($_POST['cari'])){
			$this->db->like('album.NPM', $_POST['cari']);
			$this->db->or_like('album.FOTOALB', $_POST['cari']);
			$this->db->or_like('mahasiswa.NAMAMHS', $_POST['cari']);
		}
		$this->db->join('mahasiswa','mahasiswa.NPM = album.NPM');
		$this->db->order_by('album.TGLALB','DESC');
		return $this->db->get('album')->result();
	}

	public function simpan($n){
		$data = array(
			'FOTOALB'=>$n['file'],
			'SIZEALB'=>$n['ukuran'],
			'TGLALB'=>$n['tgl'],
			'NPM'=>$n['npm'],
			'KDALB'=>$n['id'],
			'DESKALB'=>$n['deskripsi']
		);
		$this->db->insert('upload',$data);
	}

	public function deskripsi($n){
		$this->db->where($n['kode']);
		$this->db->set('DESKALB',$n['deskripsi']);
		$this->db->update('album');
	}

	public function kodekel(){
		$this->db->where('NPM',$this->session->usern);
		return $this->db->get('kelompok_peserta')->result();
	}

	public function maxId(){
		$this->db->select_max('KDALB','kode');
		$data = $this->db->get('album');
		return ($data->result()[0]->kode)+1;
	}

}

/* End of file galleryModel.php */
/* Location: ./application/models/mhs/v/galleryModel.php */