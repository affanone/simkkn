<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfilModel extends CI_Model {

	public function get(){
		$thn = $this->mfungsi->tahun()->kode;
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN AND STATUSTAHUN = 1');
		$this->db->join('kelompok_peserta','kelompok_peserta.KDKEL = kelompok.KDKEL AND kelompok_peserta.NPM = "'.$this->session->usern.'"');
		$this->db->join('dosen','dosen.KDDPL = kelompok.KDDPL','LEFT');
		return $this->db->get('kelompok')->result()[0];
	}

	public function anggota($kel){
		$this->db->where('kelompok_peserta.KDKEL',$kel);
		$this->db->join('mahasiswa','mahasiswa.NPM = kelompok_peserta.NPM','LEFT');
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI','LEFT');
		return $this->db->get('kelompok_peserta')->result();
	}

	public function biodata($kode){
		$thn = $this->mfungsi->tahun()->kode;
		$this->db->where('mahasiswa.NPM', $kode);
		$this->db->where_in('NPM','(SELECT NPM FROM kelompok_peserta where KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN = "'.$thn.'" AND KDKEL IN (SELECT KDKEL FROM kelompok_peserta WHERE NPM = "'.$this->session->usern.'")))',false);
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI','LEFT');
		$data = $this->db->get('mahasiswa');
		return $data->result()[0];
	}

	public function dosen(){
		$this->db->select('dosen.*');
		$this->db->join('kelompok_peserta','kelompok_peserta.NPM = "'.$this->session->usern.'" AND kelompok_peserta.KDKEL = kelompok.KDKEL');
		$this->db->join('dosen','dosen.KDDPL = kelompok.KDDPL');
		return $this->db->get('kelompok')->result();

	}
}

/* End of file profilModel.php */
/* Location: ./application/models/mhs/v/profilModel.php */