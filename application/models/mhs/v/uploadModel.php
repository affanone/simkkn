<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UploadModel extends CI_Model {

	public function view(){
		$this->db->join('kelompok_peserta','kelompok_peserta.NPM = upload.NPM AND kelompok_peserta.KDKEL IN (SELECT KDKEL FROM kelompok_peserta WHERE NPM = '.$this->session->usern.')');
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN AND STATUSTAHUN = 1');
		if(isset($_POST['cari'])){
			$this->db->like('upload.NPM', $_POST['cari']);
			$this->db->or_like('upload.FILEUPL', $_POST['cari']);
			$this->db->or_like('mahasiswa.NAMAMHS', $_POST['cari']);
		}
		$this->db->join('mahasiswa','mahasiswa.NPM = upload.NPM');
		$this->db->order_by('upload.TGLUPL','DESC');
		return $this->db->get('upload')->result();
	}

	public function kodekel(){
		$this->db->where('NPM',$this->session->usern);
		return $this->db->get('kelompok_peserta')->result();
	}

	public function simpan($n){
		$data = array(
			'FILEUPL'=>$n['file'],
			'SIZEUPL'=>$n['ukuran'],
			'TGLUPL'=>$n['tgl'],
			'NPM'=>$n['npm'],
			'KDUPL'=>$n['id'],
			'JENISUPL'=>$n['jenis'],
			'DESKUPL'=>$n['deskripsi'],
			'AJUKANUPL'=>$n['ajukan'],
			'HASHUPL'=>$n['hash']
		);
		$this->db->insert('upload',$data);
	}

	public function delete($kode){
		$this->db->where('KDUPL',$kode);
		$this->db->where('NPM',$this->session->usern);
		$this->db->where('AJUKANUPL',0);
		$this->db->or_where('AJUKANUPL',2);
		$this->db->delete('upload');
	}

	public function cek($kode){
		$this->db->where('KDUPL',$kode);
		$this->db->where('NPM',$this->session->usern);
		$this->db->from('upload');
		return $this->db->count_all_results();
	}

	public function file($kode){
		$this->db->where('KDUPL',$kode);
		return $this->db->get('upload')->result();
	}

	public function maxId(){
		$this->db->select_max('KDUPL','kode');
		$data = $this->db->get('upload');
		return ($data->result()[0]->kode)+1;
	}

	public function validasiUpload($n){
		$MAX_FILE_IN_DAY = 1;
		$this->db->where('DATE(upload.TGLUPL)',$n['tanggal']);
		$this->db->where_in('upload.NPM','select NPM from kelompok_peserta where KDKEL = '.$n['kelompok'],FALSE);
		$c=$this->db->get('upload')->num_rows();
		if($c>=$MAX_FILE_IN_DAY){
			return array(
				'status'=>false,
				'pesan'=>'Pengunggahan hanya dapat dilakukan sebanyak '.$MAX_FILE_IN_DAY.' file dalam 24 jam, <strong><br>Tips </strong>Hapus file pada tanggal saat ini selanjutnya perbaru file dan unggah kembali!'
			);
		}

		$this->db->where('HASHUPL',$n['hash']);
		$this->db->where_in('upload.NPM','select NPM from kelompok_peserta where KDKEL = '.$n['kelompok'],FALSE);
		$c=$this->db->get('upload')->num_rows();
		if($c>=1){
			return array(
				'status'=>false,
				'pesan'=>'File ini sudah pernah diupload!'
			);
		}

		$this->db->where('FILEUPL',$n['file']);
		$this->db->where_in('upload.NPM','select NPM from kelompok_peserta where KDKEL = '.$n['kelompok'],FALSE);
		$c=$this->db->get('upload')->num_rows();
		if($c>=1){
			return array(
				'status'=>false,
				'pesan'=>'Nama file sudah ada, gunakan nama file lainnya!'
			);
		}

		return  array(
				'status'=>true,
				'pesan'=>'File valid!'
			);
	}

}

/* End of file uploadModel.php */
/* Location: ./application/models/mhs/v/uploadModel.php */