<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NilaiModel extends CI_Model {

	public function get(){
		$this->db->select('kelompok_peserta.NILAIMHS,mahasiswa.STATUSMHS');
		$this->db->where('kelompok_peserta.NPM',$this->session->usern);
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->join('mahasiswa','mahasiswa.NPM = kelompok_peserta.NPM');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN AND STATUSTAHUN = 1');
		$d = $this->db->get('kelompok_peserta')->result();
		return $d[0];
	}

}

/* End of file nilaiModel.php */
/* Location: ./application/models/mhs/v/nilaiModel.php */