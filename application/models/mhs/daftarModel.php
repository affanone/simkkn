<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DaftarModel extends CI_Model {

	public function simpan($n){
		if($this->cekNpm()==0){
			$data = array(
				'NPM'=>$this->session->usern,
				'NAMAMHS'=>$this->session->nama,
				'ALAMATMHS'=>$n['alamat'],
				'KONTAKMHS'=>$n['telp'],
				'SERAGAMMHS'=>$n['seragam'],
				'KDPRODI'=>$this->session->prodikode,
				'SKSMHS'=>$n['sks'],
				'PEKERJAANMHS'=>$n['kerja'],
				'KORDXMHSD'=>$n['kordx'],
				'KORDYMHSD'=>$n['kordy'],
				'TGLREGMHS'=>$this->mfungsi->timeNow()
			);
			$this->db->insert('mahasiswa_mendaftar',$data);
		}
	}

	public function cekNpm(){
		$this->db->where('NPM',$this->session->usern);
		return $this->db->get('mahasiswa')->num_rows();
	}

}

/* End of file daftarModel.php */
/* Location: ./application/models/mhs/daftarModel.php */