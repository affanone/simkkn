<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StatusModel extends CI_Model {

	public function cetStatus(){
		$this->db->where('NPM',$this->session->usern);
		$this->db->where('STATUSMHS',0);
		$this->db->from('mahasiswa');
		$cek = $this->db->count_all_results();
		if($cek>0){
			$this->db->where('NPM',$this->session->usern);
			$this->db->from('kelompok_peserta');
			$cek = $this->db->count_all_results();
			if($cek>0){
				$this->session->verified = true;
				$this->session->tidak_lulus = true;
				redirect('mhs/ver/home');
			}else{
				if(intval($this->mfungsi->setting('edit_biodata_mhs'))==1){
					$this->session->enable_edit_biodata = true;
				}else{
					$this->session->unset_userdata('enable_edit_biodata');
					redirect('mhs/tunggu');
				}
			}
		}else{
			$cek = $this->cekVerifikasi();
			if($cek>0){
				redirect('mhs/ver/home');
			}else{
				$this->db->where('NPM',$this->session->usern);
				$this->db->where('STATUSMHS',2);
				$this->db->from('mahasiswa');
				$cek = $this->db->count_all_results();
				if($cek>0){
					$this->session->mhs_lulus = true;
				}
				redirect('mhs/ver/home');
			}
		}
	}

	public function cekVerifikasi(){
		$this->db->where('NPM',$this->session->usern);
		$this->db->where('STATUSMHS',1);
		$this->db->from('mahasiswa');
		return $this->db->count_all_results();
	}

}

/* End of file statusModel.php */
/* Location: ./application/views/mhs/statusModel.php */