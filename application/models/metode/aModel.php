<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AModel extends CI_Model {

	public function getMhsGeometry(){
		$tahun = $this->mfungsi->tahun()->kode;
		$afile = './jarak/'.$tahun.'.json';
		$npmd = array();
		if($this->mfile->exists($afile)==true){
			$file = json_decode($this->mfile->read($afile));
			foreach ($file as $key => $value) {
				if(!in_array($value->NPM, $npmd))
					array_push($npmd, $value->NPM);
			}
		}
		$this->db->where('mahasiswa.STATUSMHS',0);
		if(count($npmd)>0)
			$this->db->where_not_in('mahasiswa.NPM', $npmd);
		//$this->db->where("mahasiswa.NPM NOT IN (SELECT NPM FROM jarak_kelompok)");
		$this->db->order_by('KDPRODI', 'RANDOM');
		return $this->db->get('mahasiswa')->result();
	}

	public function getKelompok(){
		$this->db->where('kelompok.KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1)');
		$this->db->join('dosen','dosen.KDDPL = kelompok.KDDPL');
		$data = $this->db->get('kelompok');
		return $data->result();
	}

	public function getProdi(){
		return $this->db->get('prodi')->result();
	}

	public function jarak_kelompok(){
		$tahun = $this->mfungsi->tahun()->kode;
		$afile = './jarak/'.$tahun.'.json';
		$data = array();
		if($this->mfile->exists($afile)==true){
			$data = json_decode($this->mfile->read($afile));
		}
		return $data;
	}

	public function delKapasitasKkn(){
		$this->db->query('DELETE FROM `kapasitas_prodi` WHERE KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1))');
		$this->db->query('UPDATE kelompok SET KAPASITASKEL = 0 WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1)');
	}

	public function setKapasitesKel($a,$b){
		$this->db->where('KDKEL',$a);
		$this->db->set('KAPASITASKEL',$b);
		$this->db->update('kelompok');
	}

	public function setKapasitasKkn($data){
		$datakap = array();
		$mulai=0;
		foreach ($data as $kel => $value) {
			$vkp = 0;
			foreach ($value as $prodi => $value1) {
				array_push($datakap, array(
					'KDPRODI'=>$prodi,
					'KDKEL'=>$kel,
					'KAPRODI'=>$value1
				));
				$vkp += $value1;
			}
			$this->setKapasitesKel($kel,$vkp);
		}
		$this->db->insert_batch('kapasitas_prodi', $datakap);
	}

	public function verifikasi($npm){
		$this->db->set('STATUSMHS',1);
		$this->db->where('NPM',$npm);
		$this->db->where('STATUSMHS',0);
		$this->db->update('mahasiswa');
	}

	public function kelompok_peserta($kel,$npm){
		$this->db->insert('kelompok_peserta',array(
			'KDKEL'=>$kel,
			'NPM'=>$npm,
			'NILAIMHS'=>NULL
		));
	}

	public function mhsNokordinat(){ //digunakan
		$this->db->select('NPM,NAMAMHS,ALAMATMHS');
  	$this->db->where('KORDXMHS',NULL);
  	$this->db->or_where('KORDXMHS','');
  	$this->db->where('KORDYMHS',NULL);
  	$this->db->or_where('KORDXMHS','');
  	return $this->db->get('mahasiswa')->result();
  }

  public function setKordinat($data){
		$this->db->set('KORDXMHS',$data['lat']);
		$this->db->set('KORDYMHS',$data['lng']);
		$this->db->where('NPM',$data['npm']);
		$this->db->update('mahasiswa');
	}

	public function delJarakKelompok(){
		$tahun = $this->mfungsi->tahun()->kode;
		$afile = './jarak/'.$tahun.'.json';
		if($this->mfile->exists($afile)==true){
			$this->mfile->del_file($afile);
		}
	}

	public function delKelompokPeserta(){
		$datafile = $this->db->query('SELECT KDKEL FROM kelompok_peserta WHERE KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1)) GROUP BY KDKEL')->result();
		foreach ($datafile as $key => $value) {
			$this->mfile->del_dir('dokumen/'.$value->KDKEL);
		}

		$this->db->query('DELETE FROM kelompok_peserta WHERE KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1))');
	}

	public function setStatusMhsNol(){
		$this->db->query('UPDATE mahasiswa SET STATUSMHS = 0 WHERE NPM NOT IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1)))');
	}

}

/* End of file aModel.php */
/* Location: ./application/models/metode/aModel.php */