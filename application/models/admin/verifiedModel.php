<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VerifiedModel extends CI_Model {

	public function getMhs($npm=null){  //digunakan
		$tahun = $this->mfungsi->tahun()->kode;
		$afile = './jarak/'.$tahun.'.json';
		$npmd = array();
		if($this->mfile->exists($afile)==true){
			$file = json_decode($this->mfile->read($afile));
			foreach ($file as $key => $value) {
				if(!in_array($value->NPM, $npmd))
					array_push($npmd, $value->NPM);
			}
		}
		if($npm!=null)
			$this->db->where('mahasiswa.NPM',$npm);	
		$this->db->where('mahasiswa.STATUSMHS',0);
		if(count($npmd)>0)
			$this->db->where_not_in('mahasiswa.NPM', $npmd);
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI','LEFT');
		$data = $this->db->get('mahasiswa');
		$mahasiswa = array();
		$i=0;
		foreach ($data->result() as $key => $value) {
			$mahasiswa[$i] = new stdClass();
			$mahasiswa[$i]->NPM = $value->NPM;
			$mahasiswa[$i]->NAMA = $value->NAMAMHS;
			$mahasiswa[$i]->KODE = $value->KDPRODI;
			$mahasiswa[$i]->ALAMAT = $value->ALAMATMHS;
			$mahasiswa[$i]->PRODI = $value->FAKPRODI.' / '.$value->NAMAPRODI;
			$mahasiswa[$i]->STATUS = $value->STATUSMHS;
			$i++;
		}
		return $mahasiswa;
	}

	public function getProdi(){ //digunakan
		return $this->db->get('prodi')->result();
	}

	public function getKelompok(){  //digunakan
		$this->db->where('kelompok.KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1)');
		$this->db->join('dosen','dosen.KDDPL = kelompok.KDDPL');
		$data = $this->db->get('kelompok');
		return $data->result();
	}

	public function mhsNokordinat(){ //digunakan
		$this->db->select('NPM,NAMAMHS,ALAMATMHS');
  	$this->db->where('KORDXMHS',NULL);
  	$this->db->or_where('KORDXMHS','');
  	$this->db->where('KORDYMHS',NULL);
  	$this->db->or_where('KORDXMHS','');
  	return $this->db->get('mahasiswa')->result();
  }

	public function cek_pembentukan(){ //digunakan
		// $a = $this->db->query('SELECT COUNT(*)AS JML FROM `kelompok_peserta` WHERE KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1))')->row();
		// $b = $this->db->query('SELECT SUM(KAPRODI)AS JML FROM `kapasitas_prodi` WHERE KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1))')->row();
		$b = $this->db->query('SELECT * FROM `mahasiswa` WHERE STATUSMHS =1')->result();
		if(count($b)>0){
			return true;
		}else{
			return false;
		}
	}

}

/* End of file verifiedModel.php */
/* Location: ./application/models/admin/verifiedModel.php */