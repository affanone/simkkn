<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MapModel extends CI_Model {

	public function get($cari=null,$mode=true){
		$cek = intval($this->mfungsi->setting('halaman_peserta'));
		if($cari!=null){
			$this->db->like('mahasiswa.NPM',$cari);
			$this->db->or_like('mahasiswa.NAMAMHS',$cari);
			$this->db->or_like('kelompok.NAMAKEL',$cari);
			$this->db->or_like('mahasiswa.ALAMATMHS',$cari);
			$this->db->or_like('kelompok.ALAMATKEL',$cari);
			$this->db->or_like('dosen.NAMADPL',$cari);
			$this->db->or_like('dosen.ALAMATDPL',$cari);
		}
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->join('mahasiswa','mahasiswa.NPM = kelompok_peserta.NPM');
		$this->db->join('dosen','dosen.KDDPL = kelompok.KDDPL');
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI');
		if($mode==false){
			if($cek==1){
				$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN AND tahun.STATUSTAHUN != 1');
			}else{
				$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN');
			}
			return $this->db->get('kelompok_peserta')->result();
		}else{
			$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN AND tahun.STATUSTAHUN = 1');
			return $this->db->get('kelompok_peserta')->result();
		}
	}

}

/* End of file mapModel.php */
/* Location: ./application/models/admin/mapModel.php */