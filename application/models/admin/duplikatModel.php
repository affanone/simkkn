<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DuplikatModel extends CI_Model {

	public function upl_peserta(){
		$this->db->join('kelompok_peserta','upload.NPM = kelompok_peserta.NPM');
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN');
		$h = $this->db->get('upload')->result();

		$this->db->select('upload.KDUPL,upload.HASHUPL,COUNT(upload.HASHUPL) AS TOTAL,upload.TGLUPL,upload.AJUKANUPL');
		$this->db->join('kelompok_peserta','upload.NPM = kelompok_peserta.NPM');
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN');
		$this->db->group_by('upload.HASHUPL');
		$this->db->order_by('TOTAL DESC, upload.TGLUPL DESC, upload.AJUKANUPL ASC');
		$d = $this->db->get('upload')->result();
		$pilih = [];
		foreach ($d as $key => $value) {
			if($value->TOTAL>1)
				array_push($pilih, array(
					'kode'=>$value->KDUPL,
					'hash'=>$value->HASHUPL
				));
		}
		$del = array();
		$ukuran = 0;
		foreach ($pilih as $key => $value) {
			foreach ($h as $k => $v) {
				if($v->HASHUPL==$value['hash'] && $v->KDUPL != $value['kode']){
					array_push($del, $v);
					$ukuran += $v->SIZEUPL;
				}
			}
		}

		return array(
			'hapus'=>$del,
			'hemat'=>$ukuran
		);
	}

	public function upl_diterima(){
		$this->db->join('kelompok_peserta','upload.NPM = kelompok_peserta.NPM');
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN');
		$this->db->where('AJUKANUPL',4);
		return $this->db->get('upload')->result();
	}

	public function upl_diajukan(){
		$this->db->join('kelompok_peserta','upload.NPM = kelompok_peserta.NPM');
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN');
		$this->db->where('AJUKANUPL',1);
		return $this->db->get('upload')->result();
	}

	public function upl_belum(){
		$this->db->join('kelompok_peserta','upload.NPM = kelompok_peserta.NPM');
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN');
		$this->db->where('AJUKANUPL',0);
		$this->db->or_where('AJUKANUPL',2);
		$this->db->or_where('AJUKANUPL',3);
		return $this->db->get('upload')->result();
	}

	public function upl_monitoring(){
		$this->db->join('kelompok','kelompok.KDKEL = lembar_monitoring.KDKEL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN');
		return $this->db->get('lembar_monitoring')->result();
	}

	public function hapus_file($id,$path){
		$this->db->where('KDUPL',$id);
		$this->db->delete('upload');
		$this->mfile->del_file($path);
	}

	public function hapus_file_monitoring($id,$path){
		$this->db->where('KDLM',$id);
		$this->db->delete('lembar_monitoring');
		$this->mfile->del_file($path);
	}

}

/* End of file duplikatModel.php */
/* Location: ./application/models/admin/duplikatModel.php */