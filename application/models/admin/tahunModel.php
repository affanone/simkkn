<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TahunModel extends CI_Model {

	public function view(){
		$this->db->order_by('STATUSTAHUN desc , KDTAHUN desc');
		$data = $this->db->get('tahun');
		return $data->result();
	}

	public function simpan($n){
		$data['NAMATAHUN'] = $n['label'];
		$data['STATUSTAHUN'] = 0;
		$this->db->insert('tahun',$data);
	}

	public function update($n,$k){
		$this->db->set('NAMATAHUN', $n);
		$this->db->where('KDTAHUN', $k);
		$this->db->update('tahun');
	}

	public function load($kode){
		$this->db->where('KDTAHUN', $kode);
		$data = $this->db->get('tahun');
		return $data->result()[0];
	}

	public function nonaktifAll(){
		$this->db->set('STATUSTAHUN', 0);
		$this->db->where('STATUSTAHUN', 1);
		$this->db->update('tahun');
	}

	public function status($n){
		$this->nonaktifAll();
		$this->db->set('STATUSTAHUN', $n['status']);
		$this->db->where('KDTAHUN', $n['kode']);
		$this->db->update('tahun');
	}

	public function hapus($kode){
		$this->db->where('KDTAHUN', $kode);
		$this->db->delete('tahun');
	}

}

/* End of file tahunModel.php */
/* Location: ./application/models/admin/tahunModel.php */