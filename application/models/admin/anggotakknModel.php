<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnggotakknModel extends CI_Model {

	public function mahasiswa($npm,$s=0){
		if($s=='cari')
			$this->db->where('mahasiswa.STATUSMHS !=',0);
		else
			$this->db->where('mahasiswa.STATUSMHS',$s);
		$this->db->like('mahasiswa.NPM',$npm);
		$this->db->or_like('mahasiswa.NAMAMHS',$npm);
		$this->db->or_like('mahasiswa.ALAMATMHS',$npm);
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI');
		$this->db->order_by('rand()');
		return $this->db->get('mahasiswa', 1)->result();
	}

	public function get_kodekel($npm){
		$this->db->select('kelompok_peserta.KDKEL');
		$this->db->where('kelompok_peserta.NPM',$npm);
		$this->db->join('kelompok','kelompok_peserta.KDKEL = kelompok.KDKEL');
		$this->db->join('tahun','kelompok.KDTAHUN = tahun.KDTAHUN AND tahun.STATUSTAHUN = 1');
		return $this->db->get('kelompok_peserta')->result();
	}

	public function allmahasiswa($kode){
		$this->db->where("(mahasiswa.NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kode'))");
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI');
		return $this->db->get('mahasiswa')->result();
	}

	public function tambah($npm,$kel){
		$this->db->insert('kelompok_peserta',array(
			'KDKEL'=>$kel,
			'NPM'=>$npm
		));
		$this->statusmhs($npm);
	}
	
	public function kelompok(){
		$thn = $this->mfungsi->tahun()->kode;
		$this->db->join('(select a.KDKEL , COUNT(a.KDKEL) AS KAPASITAS from kelompok_peserta a GROUP BY a.KDKEL)a','a.KDKEL = kelompok.KDKEL');
		$this->db->join('dosen','dosen.KDDPL = kelompok.KDDPL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN AND tahun.KDTAHUN = '.$thn);
		$this->db->order_by('kelompok.NAMAKEL');
		return $this->db->get('kelompok')->result();
	}

	public function pindah($npm,$kel){
		$this->db->set('KDKEL',$kel);
		$this->db->where('NPM',$npm);
		$this->db->update('kelompok_peserta');
	}

	private function statusmhs($npm){
		$this->db->set('STATUSMHS',1);
		$this->db->where('NPM',$npm);
		$this->db->where('STATUSMHS',0);
		$this->db->update('mahasiswa');
	}

	public function kelompok_tersedia($prodi,$kel = ''){
		$thn = $this->mfungsi->tahun()->kode;
		if($kel!='')
			$this->db->where('kapasitas_prodi.KDKEL',$kel);
		$this->db->select("*,(SELECT COUNT(*) FROM kelompok_peserta WHERE kelompok_peserta.KDKEL = kapasitas_prodi.KDKEL AND NPM IN (SELECT NPM from mahasiswa WHERE KDPRODI = '$prodi')) AS ISI");
		$this->db->join('kelompok',"kelompok.KDKEL = kapasitas_prodi.KDKEL AND kelompok.KDTAHUN = '$thn'");
		$this->db->join('dosen',"dosen.KDDPL = kelompok.KDDPL");
		$this->db->where('kapasitas_prodi.KDPRODI',$prodi);
		$data = $this->db->get('kapasitas_prodi');
		$kel = array();
		foreach ($data->result() as $key => $value) {
			$row = array();
			if($value->ISI < $value->KAPRODI){
				foreach ($value as $k => $v) {
					$row[$k] = $v;
				}
				$obj = (object) $row;
				array_push($kel,$obj);
			}
		}
		return $kel;

	}

	public function setKapasitesKel($a,$c=0){
		$this->db->where('KDKEL',$a);
		if($c==0)
			$this->db->set('KAPASITASKEL', 'KAPASITASKEL-1', FALSE);
		else
			$this->db->set('KAPASITASKEL', 'KAPASITASKEL+1', FALSE);
		$this->db->update('kelompok');
	}

	public function kelompokMhs($npm){
		$this->db->select('KDKEL AS kode');
		$this->db->where('NPM',$npm);
		$data = $this->db->get('kelompok_peserta')->result();
		return $data[0];
	}

	public function atribut_mhs($nim){
		$this->db->select('mahasiswa.KDPRODI AS kode, mahasiswa.ALAMATMHS AS alamat, kelompok.*');
		$this->db->join('kelompok_peserta','kelompok_peserta.NPM = mahasiswa.NPM','LEFT');
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL','LEFT');
		$this->db->where('mahasiswa.NPM',$nim);
		$data = $this->db->get('mahasiswa')->result();
		//$data = $this->db->query("SELECT KDPRODI AS kode, ALAMATMHS AS alamat FROM mahasiswa  WHERE NPM = '$nim'")->result();
		return $data[0];
	}


	public function tukar($npm,$kel){
		$this->db->set('KDKEL',$kel);
		$this->db->where('NPM',$npm);
		$this->db->update('kelompok_peserta');
	}

	public function update_kapasitas($kel,$prd,$o){ // o=0 min o=1 plus
		if($o==0)
			$this->db->set('KAPRODI', 'KAPRODI-1', FALSE);
		else
			$this->db->set('KAPRODI', 'KAPRODI+1', FALSE);
		$this->db->where('KDKEL',$kel);
		$this->db->where('KDPRODI',$prd);
		$this->db->update('kapasitas_prodi');
	}


}

/* End of file anggotakkModel.php */
/* Location: ./application/models/admin/anggotakkModel.php */