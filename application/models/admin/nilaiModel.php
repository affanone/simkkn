<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NilaiModel extends CI_Model {

	public function prodi(){
		return $this->db->get('prodi')->result();
	}

	public function get($prodi){
		$this->db->where('kelompok_peserta.NPM IN (SELECT NPM FROM mahasiswa WHERE KDPRODI = '.$prodi.')');
		$this->db->where('kelompok_peserta.KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1))');
		$this->db->join('mahasiswa','mahasiswa.NPM = kelompok_peserta.NPM');
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->order_by('kelompok_peserta.NPM');
		return $this->db->get('kelompok_peserta')->result();
	}

}

/* End of file nilaiModel.php */
/* Location: ./application/models/admin/nilaiModel.php */