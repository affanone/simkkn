<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UniraModel extends CI_Model {

	
  public function get_Header(){
  	return array(
     'Content-Type:application/x-www-form-urlencoded; charset=utf-8',
     'Authorization:Bearer '.$_SESSION['tokenUnira']
    );
  }

  public function get_Token($data = []){
		$url = 'https://api.unira.ac.id/v1/token';
		$api = $this->mapi->request(array(
      'url'=>$url,
      'method'=>'POST',
      'params'=>$data
    ));
    $ret = new stdClass();
    if(isset($api->errors)){
      $ret->status = false;
      $ret->msg = $api->errors[0]->detail;
      $this->session->unset_userdata(['tokenUnira','refTokenUnira']);
    }else if($api!=null){
      $ret->status = true;
      $ret->token = new stdClass();
      $ret->token->access = $api->data->attributes->access;
      $ret->token->refresh = $api->data->attributes->refresh;
      $newdata = array(
        'tokenUnira' => $api->data->attributes->access,
        'refTokenUnira' => $api->data->attributes->refresh
      );
      $this->session->set_userdata($newdata);
    }else{
      $ret->status = false;
      $ret->msg = 'Kesalahan tidak ditemukan';
      $this->session->unset_userdata(['tokenUnira','refTokenUnira']);
    }
    return $ret;
  }

  public function refresh_token(){
    if(isset($_SESSION['refTokenUnira'])){
      $url = 'https://api.unira.ac.id/v1/token';
      $api = $this->mapi->request(array(
        'url'=>$url,
        'method'=>'PUT',
        'params'=>array('refresh'=>$_SESSION['refTokenUnira'])
      ));
      $_SESSION['tokenUnira'] = $api->data->attributes->access;
    }
  }

  public function saya(){
    $api = $this->mapi->request(array(
      'url'=>'https://api.unira.ac.id/v1/saya',
      'headers'=>$this->get_Header()
    ));
    if(!isset($api->errors) && $api!=null){
      return $api;
    }else{
      $this->refresh_token();
      $this->saya();
    }
  }

  public function prodi($n){
    $api = $this->mapi->request(array(
      'url'=>'https://api.unira.ac.id/v1/prodi'
    ));
    foreach ($api->data as $key => $value) {
      if($value->id==$n){
        return $api->data[$key];
      }
    }
  }

  public function get_krs_kkn($nim){
    $akd = $this->kode_akademik();
    $api = $this->mapi->request(array(
      'url'=>'https://api.unira.ac.id/v1/krs?filter[tahunAkademik]='.$akd.'&filter[nim]='.$nim
    ));
    if(isset($api->data)){
      foreach ($api->data as $key => $value) {
        $mks = array('kuliah kerja nyata','kkn','pkl');
        if(in_array(strtolower($value->attributes->namaMk),$mks)){
          return true;
        }
      }
    }
    return false;
  }

  public function kode_akademik(){
    $api = $this->mapi->request(array(
      'url'=>'https://api.unira.ac.id/v1/thajaran?sort=-nama,-semester'
    ));
    foreach ($api->data as $key => $value) {
      if($value->attributes->status==1)
        return $value->id;
    }
    return $api->data[0]->id;
  }

}

/* End of file sayaModel.php */
/* Location: ./application/models/admin/sayaModel.php */