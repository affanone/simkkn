<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VerifiedaModel extends CI_Model {

	public function jarak_kelompok(){
		// $this->db->join('mahasiswa','mahasiswa.NPM = jarak_kelompok.NPM AND mahasiswa.STATUSMHS = 0');
		// return $this->db->get('jarak_kelompok')->result();
		$tahun = $this->mfungsi->tahun()->kode;
		$afile = './jarak/'.$tahun.'.json';
		$data = array();
		if($this->mfile->exists($afile)==true){
			$data = json_decode($this->mfile->read($afile));
		}
		return $data;
	}

	public function getProdi(){
		return $this->db->get('prodi')->result();
	}

	public function getKelompok(){
		$this->db->where('kelompok.KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1)');
		$this->db->join('dosen','dosen.KDDPL = kelompok.KDDPL');
		$data = $this->db->get('kelompok');
		return $data->result();
	}

	public function delJarakKelompok(){
		//$this->db->query('DELETE FROM jarak_kelompok WHERE KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1))');

		$tahun = $this->mfungsi->tahun()->kode;
		$afile = './jarak/'.$tahun.'.json';
		if($this->mfile->exists($afile)==true){
			$this->mfile->del_file($afile);
		}
	}


	public function delKelompokPeserta(){
		$this->db->query('DELETE FROM kelompok_peserta WHERE KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1))');
	}

	public function delKapasitasKkn(){
		$this->db->query('DELETE FROM `kapasitas_prodi` WHERE KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1))');
	}

	public function setStatusMhsNol(){
		$this->db->query('UPDATE mahasiswa SET STATUSMHS = 0 WHERE NPM NOT IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1)))');
	}

	public function setKapasitasKkn($data){
		$datakap = array();
		$mulai=0;
		foreach ($data as $kel => $value) {
			foreach ($value as $prodi => $value1) {
					array_push($datakap, array(
						'KDPRODI'=>$prodi,
						'KDKEL'=>$kel,
						'KAPRODI'=>$value1
					));
			}
		}
		$this->db->insert_batch('kapasitas_prodi', $datakap);
	}

	public function verifikasi($npm){
		$this->db->set('STATUSMHS',1);
		$this->db->where('NPM',$npm);
		$this->db->where('STATUSMHS',0);
		$this->db->update('mahasiswa');
	}

	public function kelompok_peserta($kel,$npm){
		$this->db->insert('kelompok_peserta',array(
			'KDKEL'=>$kel,
			'NPM'=>$npm,
			'NILAIMHS'=>NULL
		));
	}

	public function kosongkan(){
		//$this->db->where('')
	}

}

/* End of file verifiedModel.php */
/* Location: ./application/models/admin/verifiedModel.php */