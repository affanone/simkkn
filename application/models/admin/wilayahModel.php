<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WilayahModel extends CI_Model {
	public function find($nal,$o='auto'){ // auto, google, database
		if(!in_array($o, array('auto','google','database')))
			$o = 'auto';
		$n = $this->regex(urldecode($nal));
		$this->keyword = $n;
		$res = new stdClass();
		$res->keyword = $n;
		$res->address = $nal;
		$all = $this->getalamat($n,$o);
		$all1 = $all->allresult;
		unset($all->allresult);
		$res->from = $all->from;
		unset($all->from);
		$res->result = $all;
		$res->status = $all->status;
		unset($all->status);
		$res->resultAll = $all1;
		return $res;
	}

	public function getalamat($n,$o){
		error_reporting(0);
		if($o=='auto' || $o=='database'){
			$keltext = '';
			$kectext = ', ';
			$kabtext = ', ';
			$like1 = $this->getwherelike($n,'a');
			$like2 = $this->getwherelike($n,'kecamatan');
			$like3 = $this->getwherelike($n,'kabupaten');
			$like4 = $this->getwherelike($n,'provinsi');
			$resultData = new stdClass();
			$q = "SELECT `a`.`name` as `kel`, `b`.`kec`,`c`.`kab`,`d`.`prov` FROM kelurahan a
						LEFT JOIN (select `b`.*,`b`.`name` as `kec` from `kecamatan` `b`) `b` ON `a`.`kecamatan_id` = `b`.`id`
						LEFT JOIN (select `c`.*,`c`.`name` as `kab` from `kabupaten` `c`) `c` ON `b`.`kabupaten_id` = `c`.`id`
						LEFT JOIN (select `d`.*,`d`.`name` as `prov` from `provinsi` `d`) `d` ON `c`.`provinsi_id` = `d`.`id`
						WHERE $like1 and `a`.`kecamatan_id` in (select `id` from `kecamatan` where $like2 and `kecamatan`.`kabupaten_id` in (select `id` from `kabupaten` where $like3 and `kabupaten`.`provinsi_id` in (select `id` from `provinsi` where $like4)))";
			$data = $this->db->query($q)->result();
			if(count($data)>1){
				$str = '';
				$res = [];
				$i = 0;
				foreach ($data as $key => $value) {
					$str = strtolower($value->kel.' '.$value->kec.' '.$value->kab.' '.$value->prov);
					$skor = 0;
					foreach ($n as $xy => $xv) {
						$status = 'tidak ada';
						if(preg_match("/$xv/i", $str)){
							$skor++;
							$status = 'ada';
						}
					}
					$res[$i] = new stdClass();
					$res[$i]->skor = $skor;
					$res[$i]->data = $value;
					$i++;
				}
				for ($i=0; $i < count($res)-1; $i++) { 
					$tmp = '';
					for ($j=$i+1; $j < count($res); $j++) { 
						if($res[$j]->skor>$res[$i]->skor){
							$tmp = $res[$i];
							$res[$i] = $res[$j];
							$res[$j] = $tmp;
						}
					}
				}
				$res[0] = $res[0]->data;
				$result = 'keltext '.$res[0]->kel.', kectext.'.$res[0]->kec.', kabtext. '.$res[0]->kab.' '.$res[0]->prov.' Indonesia';
				$result = implode(' ', $this->regex($result));
				$resultData->text = ucwords(str_replace(array('keltext','kectext','kabtext'), array($keltext,$kectext,$kabtext), $result));
				$resultData->detail = new stdClass();
				$resultData->detail->kel = ucwords($res[0]->kel);
				$resultData->detail->kec = ucwords($res[0]->kec);
				$resultData->detail->kab = ucwords($res[0]->kab);
				$resultData->detail->prov = ucwords($res[0]->prov);
				$resultData->from = 'database';
				$resultData->status = true;
			}else if(count($data)==1){
				$result = 'keltext '.$data[0]->kel.', kectext.'.$data[0]->kec.', kabtext. '.$data[0]->kab.' '.$data[0]->prov.' Indonesia';
				$result = implode(' ', $this->regex($result));
				$resultData->text = ucwords(str_replace(array('keltext','kectext','kabtext'), array($keltext,$kectext,$kabtext), $result));
				$resultData->detail = new stdClass();
				$resultData->detail->kel = ucwords($data[0]->kel);
				$resultData->detail->kec = ucwords($redatas[0]->kec);
				$resultData->detail->kab = ucwords($data[0]->kab);
				$resultData->detail->prov = ucwords($data[0]->prov);
				$resultData->from = 'database';
				$resultData->status = true;
			}else{
				if($o=='auto'){
					$res = $this->mfungsi->jarakTempuh(implode(' ', $n),'pamekasan');
					if($res->asal!='' ){
						$resultData->text = $res->asal;
						$resultData->from = 'google';
						$resultData->detail = new stdClass();
						$resultData->detail->kel = null;
						$resultData->detail->kec = null;
						$resultData->detail->kab = null;
						$resultData->detail->prov = null;
						$resultData->status = true;
					}else{
						$resultData->text = null;
						$resultData->from = 'notfound';
						$resultData->detail = new stdClass();
						$resultData->detail->kel = null;
						$resultData->detail->kec = null;
						$resultData->detail->kab = null;
						$resultData->detail->prov = null;
						$resultData->status = false;
					}
				}else{
					$resultData->text = null;
					$resultData->from = 'notfound';
					$resultData->detail = new stdClass();
					$resultData->detail->kel = null;
					$resultData->detail->kec = null;
					$resultData->detail->kab = null;
					$resultData->detail->prov = null;
					$resultData->status = false;
				}
			}
			$resultData->allresult = $data;
			return $resultData;
		}else if($o=='google'){
			$res = $this->mfungsi->jarakTempuh(implode(' ', $n),'pamekasan jawa timur');
			if($res->asal!=''){
				$resultData->text = $res->asal;
				$resultData->from = 'google';
				$resultData->status = true;
				return $resultData;
			}else{
				$resultData->text = null;
				$resultData->from = 'notfound';
				$resultData->status = false;
			}
			$resultData->allresult = null;
			return $resultData;
		}
	}

	public function getwherelike($n,$t){
		$i = 1;
		$query = '';
		foreach ($n as $key => $value) {
			if($value!=''){
				if($query==''){
					$query .= "(`$t`.`name` like '%$value%'";
				}else{
					$query .= " or `$t`.`name` like '%$value%'";
				}
				if($i==count($n))
					$query .= ")";
			}
			$i++;
		}
		return $query;
	}

	public function regex($n){
		return $this->mfungsi->filterKata($n);
	}
}

/* End of file wilayahModel.php */
/* Location: ./application/models/admin/wilayahModel.php */