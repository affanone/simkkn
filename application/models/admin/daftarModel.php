<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DaftarModel extends CI_Model {

	public function view($p = 0,$c = ''){
		if($c!=''){
			$this->db->like('(mahasiswa_mendaftar.NPM', $c);
			$this->db->or_like('mahasiswa_mendaftar.NAMAMHS', $c);
			$this->db->or_like('mahasiswa_mendaftar.KONTAKMHS', $c);
			$this->db->or_where("mahasiswa_mendaftar.ALAMATMHS LIKE '%$c%' ESCAPE '!')");
		}
		$this->db->join('prodi','mahasiswa_mendaftar.KDPRODI = prodi.KDPRODI','LEFT');
		$this->db->order_by('prodi.NAMAPRODI,mahasiswa_mendaftar.NAMAMHS');
		$this->db->limit(5,$p);
		$data = $this->db->get('mahasiswa_mendaftar');
		return $data->result();
	}

	public function totaldata($c){
		if(count($c)>0){
			if($c!=''){
				$this->db->like('(NPM', $c);
				$this->db->or_like('NAMAMHS', $c);
				$this->db->or_like('KONTAKMHS', $c);
				$this->db->or_where("ALAMATMHS LIKE '%$c%' ESCAPE '!')");
			}
		}
		$this->db->from('mahasiswa_mendaftar');
		return $this->db->count_all_results();
	}

	public function mhsDaftar(){
		return $this->db->get('mahasiswa_mendaftar')->result();
	}

	public function konfirm($npm){
		$this->db->query("INSERT IGNORE INTO `mahasiswa`(`NPM`, `TGLREGMHS`, `NAMAMHS`, `ALAMATMHS`, `KONTAKMHS`, `SERAGAMMHS`, `KDPRODI`, `SKSMHS`, `PEKERJAANMHS`, `STATUSMHS`, `KORDXMHS`, `KORDYMHS`) SELECT `NPM`, `TGLREGMHS`, `NAMAMHS`, `ALAMATMHS`, `KONTAKMHS`, `SERAGAMMHS`, `KDPRODI`,`SKSMHS`,`PEKERJAANMHS`,'0',`KORDXMHSD`,`KORDYMHSD` FROM mahasiswa_mendaftar WHERE `NPM` = '$npm'");
	}

	public function delete($npm){
		$this->db->where('NPM',$npm);
		$this->db->delete('mahasiswa_mendaftar');
	}

	public function deleteAll(){
		$this->db->query('delete from mahasiswa_mendaftar');
	}

}

/* End of file daftarMode.php */
/* Location: ./application/models/admin/daftarMode.php */