<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DesaModel extends CI_Model {

	public function view(){
		$this->db->order_by('desa.KDKEC,desa.NAMADESA');
		$this->db->join('kecamatan','kecamatan.KDKEC = desa.KDKEC');
		$this->db->join('kabupaten','kecamatan.KDKAB = kabupaten.KDKAB');
		$data = $this->db->get('desa');
		return $data->result();
	}

	public function simpan($n){
		$data['NAMADESA'] = $n['desa'];
		$data['KDKEC'] = $n['kecamatan'];
		$this->db->insert('desa',$data);
	}

	public function update($n,$k){
		$this->db->set('NAMADESA', $n['desa']);
		$this->db->set('KDKEC', $n['kecamatan']);
		$this->db->where('KDDESA', $k);
		$this->db->update('desa');
	}

	public function load($kode){
		$this->db->where('KDDESA', $kode);
		$data = $this->db->get('desa');
		return $data->result()[0];
	}

	public function hapus($kode){
		$this->db->where('KDKEC', $kode);
		$this->db->delete('desa');
	}

	public function kabupaten(){
		$this->db->order_by('KDKAB');
		$data = $this->db->get('kabupaten');
		return $data->result();
	}

	public function kodeKabupaten($kode){
		$data = $this->db->query('SELECT KDKAB FROM kabupaten WHERE KDKAB IN (SELECT KDKAB FROM kecamatan WHERE KDKEC = "'.$kode.'") LIMIT 1');
		return $data->result()[0]->KDKAB;
	}	

	public function kecamatan($kode){
		$this->db->order_by('KDKEC');
		$this->db->where('KDKAB',$kode);
		$data = $this->db->get('kecamatan');
		return $data->result();
	}

}

/* End of file tahunModel.php */
/* Location: ./application/models/admin/tahunModel.php */