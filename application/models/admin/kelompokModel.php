<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KelompokModel extends CI_Model {

	public function view($kode=''){
		$kd = $this->mfungsi->tahun()->kode;
		$this->db->order_by('NAMAKEL');
		$this->db->join('(select a.KDKEL , COUNT(a.KDKEL) AS KAPASITAS from kelompok_peserta a GROUP BY a.KDKEL)a','a.KDKEL = kelompok.KDKEL','LEFT');
		$this->db->join('dosen','dosen.KDDPL = kelompok.KDDPL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN');
		if($kode!='')
			$this->db->where('kelompok.KDKEL', $kode);
		$this->db->where('kelompok.KDTAHUN', $kd);
		$data = $this->db->get('kelompok');
		return $data->result();
	}

	public function simpan($n){
		if(isset($n['default'])){
			$this->updatedefault();
		}
		$data['KDKEL'] = $n['kode'];
		$data['NAMAKEL'] = $n['nama'];
		$data['KAPASITASKEL'] = $n['kapasitas'];
		$data['KDDPL'] = $n['dpl'];
		$data['KORDXKEL'] = $n['kordx'];
		$data['KORDYKEL'] = $n['kordy'];
		$data['ALAMATKEL'] = $n['alamat'];
		$data['DEFKAPPRODI'] = (isset($n['default'])) ? 1 : 0;
		$data['KDTAHUN'] = $this->mfungsi->tahun()->kode;
		$this->db->insert('kelompok',$data);
	}

	public function updatedefault(){
		$this->db->set('DEFKAPPRODI',0);
		$this->db->update('kelompok');
	}

	public function update($n,$k){
		if(isset($n['default'])){
			$this->updatedefault();
		}
		$thn = $this->mfungsi->tahun()->kode;
		$this->db->set('NAMAKEL', $n['nama']);
		//$this->db->set('KAPASITASKEL', $n['kapasitas']);
		$this->db->set('KDDPL', $n['dpl']);
		$this->db->set('KORDXKEL', $n['kordx']);
		$this->db->set('KORDYKEL', $n['kordy']);
		$this->db->set('ALAMATKEL', $n['alamat']);
		$this->db->set('DEFKAPPRODI', ((isset($n['default'])) ? 1 : 0));
		$this->db->where('KDKEL', $k);
		$this->db->where('KDTAHUN', $thn);
		$this->db->update('kelompok');
	}

	public function load($kode){
		$thn = $this->mfungsi->tahun()->kode;
		$this->db->where('KDTAHUN', $thn);
		$this->db->where('KDKEL', $kode);
		$data = $this->db->get('kelompok');
		return $data->result()[0];
	}

	public function hapus($kode){
		$this->db->where('KDKEL', $kode);
		$this->db->delete('kelompok');
	}

	public function dosen_add(){
		$this->db->order_by('NAMADPL');
		//$this->db->where('KDDPL NOT IN (SELECT KDDPL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1))');
		$data = $this->db->get('dosen');
		return $data->result();
	}

	public function dosen_edit($kd){
		$this->db->order_by('NAMADPL');
		//$this->db->where("KDDPL NOT IN (SELECT KDDPL FROM kelompok WHERE KDKEL != $kd AND  KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1))");
		$data = $this->db->get('dosen');
		return $data->result();
	}

	public function prodi(){
		return $this->db->get('prodi')->result();
	}

	public function maxkode(){
		$this->db->select_max('KDKEL');
		$kode = $this->db->get('kelompok')->result()[0];
		$kode =  ($kode->KDKEL==null) ? 0 : $kode->KDKEL;
		return $kode+1;
	}

	public function tambahkaprodi($prodi,$val,$kel){
		$this->db->insert('kapasitas_prodi',array(
			'KDPRODI'=>$prodi,
			'KDKEL'=>$kel,
			'KAPRODI'=>$val
		));
	}

	public function updatekaprodi($prodi,$val,$kel){
		$this->db->set('KAPRODI',$val);
		$this->db->where('KDPRODI',$prodi);
		$this->db->where('KDKEL',$kel);
		$this->db->update('kapasitas_prodi');
	}

	public function getdefkaprodi(){
		$data = $this->db->query('SELECT * FROM `kapasitas_prodi` WHERE `KDKEL` IN (SELECT `KDKEL` FROM `kelompok` WHERE `DEFKAPPRODI` = 1 AND KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1))');
		return $data->result();
	}

	public function getkaprodi($kd){
		$this->db->where('KDKEL',$kd);
		$data = $this->db->get('kapasitas_prodi');
		return $data->result();
	}

	public function anggota($kel){
		$this->db->where('kelompok_peserta.KDKEL',$kel);
		$this->db->join('mahasiswa','mahasiswa.NPM = kelompok_peserta.NPM','LEFT');
		$this->db->join('prodi','mahasiswa.KDPRODI = prodi.KDPRODI','LEFT');
		return $this->db->get('kelompok_peserta')->result();
	}

	public function laporan($kel){
		$this->db->select('*');
		$this->db->where_in('upload.NPM', '(SELECT NPM FROM kelompok_peserta WHERE KDKEL = "'.$kel.'")',FALSE);
		$this->db->where('upload.AJUKANUPL',4);
		$this->db->join('kelompok_peserta','kelompok_peserta.NPM = upload.NPM');
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->join('mahasiswa','mahasiswa.NPM = upload.NPM');
		return $this->db->get('upload')->result();
	}

	public function monitoring($kel){
		$this->db->join('kelompok','lembar_monitoring.KDKEL = kelompok.KDKEL AND kelompok.KDKEL = '.$kel);
		return $this->db->get('lembar_monitoring')->result();
	}


}

/* End of file tahunModel.php */
/* Location: ./application/models/admin/tahunModel.php */