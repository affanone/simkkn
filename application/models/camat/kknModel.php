<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KknModel extends CI_Model {

	public function get(){
		$thn = $this->mfungsi->tahun()->kode;
		$this->db->where('KDTAHUN',$thn);
		$this->db->order_by('NAMAKEL ASC');
		return $this->db->get('kelompok')->result();
	}

	public function cek($kode){
		$thn = $this->mfungsi->tahun()->kode;
		$this->db->where('KDTAHUN',$thn);
		$this->db->where('KDKEL',$kode);
		return $this->db->get('kelompok')->result();
	}

	public function set($kode){
		$cek = $this->cek($kode);
		if(count($cek)>0){
			$data = array(
				'dpl_kkn_aktif'=>$kode,
				'dpl_kkn_nama'=>$cek[0]->NAMAKEL
			);
			$this->session->set_userdata($data);
		}
	}

	public function aktif(){
		if(isset($this->session->dpl_kkn_aktif)){
			return $this->session->dpl_kkn_aktif;
		}else{
			$data = array(
				'dpl_kkn_aktif'=>$this->get()[0]->KDKEL,
				'dpl_kkn_nama'=>$this->get()[0]->NAMAKEL
			);
			$this->session->set_userdata($data);
			return $this->session->dpl_kkn_aktif;
		}
	}

}

/* End of file kknModel.php */
/* Location: ./application/models/dosen/kknModel.php */