<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UploadModel extends CI_Model {

	public function view(){
		$kdkel = $this->session->dpl_kkn_aktif;
		$this->db->where("(upload.NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		if(isset($_POST['cari'])){
			$this->db->like('upload.NPM', $_POST['cari']);
			$this->db->or_like('upload.FILEUPL', $_POST['cari']);
			$this->db->or_like('mahasiswa.NAMAMHS', $_POST['cari']);
		}
		$this->db->join('mahasiswa','mahasiswa.NPM = upload.NPM');
		$this->db->order_by('upload.TGLUPL','DESC');
		return $this->db->get('upload')->result();
	}

	public function file($kode){
		$this->db->where('KDUPL',$kode);
		return $this->db->get('upload')->result();
	}
}

/* End of file uploadModel.php */
/* Location: ./application/models/dosen/uploadModel.php */