<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NilaiModel extends CI_Model {

	public function get(){
		$kdkel = $this->session->dpl_kkn_aktif;
		$this->db->where("(mahasiswa.NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		$this->db->join('prodi','mahasiswa.KDPRODI = prodi.KDPRODI','LEFT');
		$this->db->join('kelompok_peserta',"mahasiswa.NPM = kelompok_peserta.NPM AND kelompok_peserta.KDKEL = $kdkel",'LEFT');
		$this->db->order_by('NAMAMHS,mahasiswa.NPM');
		return $this->db->get('mahasiswa')->result();
	}

	public function setnilai($n){
		$kdkel = $this->session->dpl_kkn_aktif;
		$this->db->where("KDKEL",$kdkel);
		$this->db->where("NPM",$n['npm']);
		$this->db->set('NILAIMHS',$n['nilai']);
		$this->db->update('kelompok_peserta');
	}

	public function kodekel(){
		$thn = $this->mfungsi->tahun()->kode;
		$dpl = $this->session->usern;
		$this->db->where('KDDPL',$dpl);
		$this->db->where('KDTAHUN',$thn);
		return $this->db->get('kelompok')->result()[0];
	}



}

/* End of file nilaiModel.php */
/* Location: ./application/models/dosen/nilaiModel.php */