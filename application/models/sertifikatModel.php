<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SertifikatModel extends CI_Model {

	public function terbaik($data){
		$tahun = $this->mfungsi->tahun();
		if($data['status']=='true'){
			$this->db->insert($data['jenis'],array(
				'KDDPL'=>$data['id'],
				'KDTAHUN'=>$tahun->kode
			));
		}else{
			$this->db->where('KDDPL',$data['id']);
			$this->db->where('KDTAHUN',$tahun->kode);
			$this->db->delete($data['jenis']);
		}
	}

	public function peserta($npm=null){
		if($npm!=null){
			$this->db->like('mahasiswa.NPM',$npm);
			$this->db->or_like('mahasiswa.NAMAMHS',$npm);
			$this->db->or_like('mahasiswa.ALAMATMHS',$npm);
		}
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI');
		$this->db->join('(select max(a.NILAIMHS) AS NILAI, a.NPM, a.KDKEL from kelompok_peserta a group by a.NPM)a','a.NPM = mahasiswa.NPM');
		$this->db->join('kelompok','a.KDKEL = kelompok.KDKEL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN AND tahun.STATUSTAHUN = 1');
		$this->db->join('dosen','dosen.KDDPL = kelompok.KDDPL');
		$this->db->order_by('rand()');
		if($npm!=null){
			return $this->db->get('mahasiswa', 1)->result();
		}else{
			return $this->db->get('mahasiswa')->result();
		}
	}

	public function dpl_kkn($kode=null){
		if($kode!=null)
			$this->db->where('dosen.KDDPL',$kode);
		$this->db->select('tahun.*,dosen.*,dpl_terbaik.KDDPL AS TERBAIK,kdpl_terbaik.KDDPL AS KTERBAIK');
		$this->db->join('kelompok','kelompok.KDDPL = dosen.KDDPL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN AND tahun.STATUSTAHUN = 1');
		$this->db->join('dpl_terbaik','dpl_terbaik.KDDPL = dosen.KDDPL AND dpl_terbaik.KDTAHUN = tahun.KDTAHUN','LEFT');
		$this->db->join('kdpl_terbaik','dosen.KDDPL = kdpl_terbaik.KDDPL AND kdpl_terbaik.KDTAHUN = tahun.KDTAHUN','LEFT');
		$this->db->order_by('kdpl_terbaik.KDDPL DESC,dpl_terbaik.KDDPL DESC,kelompok.KDTAHUN,dosen.NAMADPL,dosen.KDDPL');
		$this->db->group_by('dosen.KDDPL');
		return $this->db->get('dosen')->result();
	}

	public function dpl_kkn_terbaik(){
		$this->db->join('dosen','dosen.KDDPL = dpl_terbaik.KDDPL');
		$this->db->join('kelompok','kelompok.KDDPL = dosen.KDDPL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN AND tahun.STATUSTAHUN = 1');
		$this->db->order_by('dpl_terbaik.KDDPL DESC,kelompok.KDTAHUN,dosen.NAMADPL,dosen.KDDPL');
		$this->db->group_by('dosen.KDDPL');
		return $this->db->get('dpl_terbaik')->result();
	}

	public function kdpl_kkn_terbaik(){
		$this->db->join('dosen','dosen.KDDPL = kdpl_terbaik.KDDPL');
		$this->db->join('kelompok','kelompok.KDDPL = dosen.KDDPL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN AND tahun.STATUSTAHUN = 1');
		$this->db->order_by('kdpl_terbaik.KDDPL DESC,kelompok.KDTAHUN,dosen.NAMADPL,dosen.KDDPL');
		$this->db->group_by('dosen.KDDPL');
		return $this->db->get('kdpl_terbaik')->result();
	}

	public function nomor($npm){
		$this->db->select_max('kelompok_peserta.NILAIMHS');
		$this->db->select('kelompok_peserta.NPM');
		$this->db->select('tahun.KDTAHUN');
		$this->db->select('tahun.NAMATAHUN');
		$this->db->select('mahasiswa.STATUSMHS');
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN');
		$this->db->join('mahasiswa','mahasiswa.NPM = kelompok_peserta.NPM');
		$this->db->where('kelompok_peserta.NPM',$npm);
		$data1 = $this->db->get('kelompok_peserta')->result();
		if($data1[0]->NPM != NULL){
			if($this->session->level=='admin'){
				$data1[0]->STATUSMHS = 2; 
		  	$data1[0]->NILAIMHS = 100;
			}
			if($data1[0]->STATUSMHS!=2){
				$res = new stdClass();
				$res->STATUS = false;
				$res->PESAN = 'Peserta harus berstatus lulus';
				return $res;
			}else if(floatval($data1[0]->NILAIMHS)>65){ // nilai mhs yang dapat sertifikat
				$this->db->select('YEAR(mahasiswa.TGLREGMHS) as TAHUN,mahasiswa.*');
				$this->db->join('kelompok_peserta','kelompok_peserta.NPM = mahasiswa.NPM');
				$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL AND kelompok.KDTAHUN = '.$data1[0]->KDTAHUN);
				$this->db->order_by('mahasiswa.TGLREGMHS ASC,mahasiswa.NPM ASC');
				$data = $this->db->get('mahasiswa')->result();
				$i = 1;
				foreach ($data as $key => $value) {
					if($value->NPM==$npm){
						$res = new stdClass();
						$res->STATUS = true;
						$res->TAHUN = $value->TAHUN;
						$res->AKADEMIK = $data1[0]->NAMATAHUN;
						$res->NOMOR = $this->tambahNol($i);
						return $res;
					}
					$i++;
				}
			}else if($data1[0]->NILAIMHS==null){
				$res = new stdClass();
				$res->STATUS = false;
				$res->PESAN = 'Nilai belum diinput';
				return $res;
			}else{
				$res = new stdClass();
				$res->STATUS = false;
				$res->PESAN = 'Nilai kurang dari standart untuk mendapatkan sertifikat.';
				return $res;
			}
		}else{
			$res = new stdClass();
			$res->STATUS = false;
			$res->PESAN = 'Data tidak ditemukan';
			return $res;
		}
	}

	public function tambahNol($n){
		$n =  intval($n);
		if($n<10)
			return '00'.$n;
		else if($n<100)
			return '0'.$n;
		else
			return $n;
	}

	public function ambilalfabeta($n){
		$filter = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$n = strtoupper($n);
		$gbg = '';
		for ($i=0; $i < strlen($n); $i++) { 
			if(in_array($n[$i], $filter)){
				$gbg .= $n[$i];
			}
		}
		return $gbg;
	}

}

/* End of file sertifikatModel.php */
/* Location: ./application/models/admin/sertifikatModel.php */