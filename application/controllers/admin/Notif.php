<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notif extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
	}

	public function index()
	{
		set_time_limit(0);
		$this->mremaining->start();
		$data = array();
		$new = 0;
		foreach ($this->mnotif->get(100) as $key => $value) {
			array_push($data, array(
				'keterangan'=>str_replace(array('-','_'), ' ',$value->KETNOTIF),
				'id'=>$value->KDNOTIF,
				'status'=>$value->STATUSNOTIF,
				'url'=>base_url($value->URLNOTIF),
				'tgl'=>$this->mfungsi->waktu_lalu($value->TGLNOTIF)
			));
			if($value->STATUSNOTIF==1)
				$new++;
		}
		$this->mremaining->running(array(
			'data'=>$data,
			'baru'=>$new
		));
	}

	public function all(){
		$this->load->view('admin/header');
		$data['data'] = $this->mnotif->get(true);
		$this->load->view('admin/notifikasi',$data);
		$this->load->view('admin/footer');
	}

	public function delete($kode){
		$this->mnotif->batal($kode);
		redirect('admin/notif/all');
	}

	public function clear(){
		$this->mnotif->kosongkan();
		redirect('admin/notif/all');
	}

}

/* End of file Notif.php */
/* Location: ./application/controllers/admin/Notif.php */