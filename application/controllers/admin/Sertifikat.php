<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sertifikat extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$this->load->model('sertifikatModel');
	}

	public function index()
	{
		
	}

	public function topdf(){
		$array = array(
				'sertifikat_data' => $_POST['img'],
				'sertifikat_npm' =>  $_POST['npm']
			);
			
			$this->session->set_userdata( $array );
	}

	public function pdf(){
		$this->load->library('pdf');
    $this->pdf->setPaper('A4', 'landscape');
    $this->pdf->filename = $this->session->sertifikat_npm.".pdf";
    $this->pdf->load_view('sertifikat');
	}

	public function terbaik(){
		$this->sertifikatModel->terbaik($_POST);
	}

	public function peserta(){
		$this->load->view('admin/header');
		$this->load->view('admin/sertifikat');
		$this->load->view('admin/footer');
	}

	public function dpl($n=null,$kode=null){
		switch ($n) {
			case 'generate':
				$mhs = count($this->sertifikatModel->peserta());
				$dpl = $this->sertifikatModel->dpl_kkn();
				if(isset($_POST['dpl'])){
					$kode = $_POST['dpl'];
					$jenis = $_POST['jenis'];
					if($_POST['jenis']=='biasa') {
						$nomor = $mhs+1;
						$dosen = null;
						foreach ($dpl as $key => $value) {
							if($value->KDDPL == $kode){
								$dosen = $value;
								break;
							}
							$nomor++;
						}
						$nomor = $this->sertifikatModel->tambahNol($nomor);
						$data[0] = $dosen;
						$key = 'JEIBHCGDFA';
						$this->load->model('encyDesc');
						$data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
						$a = $this->encyDesc->enc($data[0]->KDDPL);
						$data[0]->KODE = $this->encyDesc->encrypt($key,$a);
						$b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMADPL);
						$c = $this->encyDesc->encrypt($b,$data[0]->KODE);
						$a = 0;
						$data[0]->KUNCIKODEO = $c;
						$data[0]->KUNCIKODE = '';
						for ($i=0; $i <strlen($c) ; $i++) { 
							$data[0]->KUNCIKODE .= '<span style="font-size:'.(12+$a).'pt;">'.$c[$i].'</span>';
							$a +=3;
						}
						$tahun = explode('/', $data[0]->NAMATAHUN);
						$x = str_replace(array('%nomor%','%tahun%'), array($nomor,$tahun[1]), $this->mfungsi->setting('nosertifikat'));
						$data[0]->NOMOR = 'No. '.$x;
						$data[0]->AKADEMIK = $data[0]->NAMATAHUN;
						$data[0]->SEBAGAI = 'Atas partisipasinya sebagai <strong>DOSEN PENDAMPING LAPANGAN</strong> pada kegiatan Kuliah Kerja Nyata<br>Universitas Madura Tahun Akademik '.$data[0]->NAMATAHUN;
						$x = explode('|', $this->mfungsi->setting('pimpinan_lppm'));
						$data[0]->PIMPINAN = (isset($x[0]))?$x[0]:'';
						$data[0]->NOMORPIMPINAN = (isset($x[1]))?$x[1]:'';
						echo json_encode($data);
					}else if($_POST['jenis']=='terbaik'){
						$nomor = $mhs+count($dpl)+1;
						$dosen = null;
						$dplt = $this->sertifikatModel->dpl_kkn_terbaik();
						foreach ($dplt as $key => $value) {
							if($value->KDDPL == $kode){
								$dosen = $value;
								break;
							}
							$nomor++;
						}
						$nomor = $this->sertifikatModel->tambahNol($nomor);
						$data[0] = $dosen;
						$key = 'ZYWQVARDFKL';
						$this->load->model('encyDesc');
						$data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
						$a = $this->encyDesc->enc($data[0]->KDDPL);
						$data[0]->KODE = $this->encyDesc->encrypt($key,$a);
						$b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMADPL);
						$c = $this->encyDesc->encrypt($b,$data[0]->KODE);
						$a = 0;
						$data[0]->KUNCIKODEO = $c;
						$data[0]->KUNCIKODE = '';
						for ($i=0; $i <strlen($c) ; $i++) { 
							$data[0]->KUNCIKODE .= '<span style="font-size:'.(12+$a).'pt;">'.$c[$i].'</span>';
							$a +=3;
						}
						$tahun = explode('/', $data[0]->NAMATAHUN);
						$x = str_replace(array('%nomor%','%tahun%'), array($nomor,$tahun[1]), $this->mfungsi->setting('nosertifikat'));
						$data[0]->NOMOR = 'No. '.$x;
						$data[0]->AKADEMIK = $data[0]->NAMATAHUN;
						$data[0]->SEBAGAI = 'Sebagai <strong>DOSEN PENDAMPING LAPANGAN TERBAIK</strong> pada kegiatan Kuliah Kerja Nyata<br>Universitas Madura Tahun Akademik '.$data[0]->NAMATAHUN;
						$x = explode('|', $this->mfungsi->setting('pimpinan_lppm'));
						$data[0]->PIMPINAN = (isset($x[0]))?$x[0]:'';
						$data[0]->NOMORPIMPINAN = (isset($x[1]))?$x[1]:'';
						echo json_encode($data);
					}else if($_POST['jenis']=='kterbaik'){
						$nomor = $mhs+count($dpl)+1;
						$dosen = null;
						$nomor += count($this->sertifikatModel->dpl_kkn_terbaik());
						$dplt = $this->sertifikatModel->kdpl_kkn_terbaik();
						foreach ($dplt as $key => $value) {
							if($value->KDDPL == $kode){
								$dosen = $value;
								break;
							}
							$nomor++;
						}
						$nomor = $this->sertifikatModel->tambahNol($nomor);
						$data[0] = $dosen;
						$key = 'DCBAKJIHNPS';
						$this->load->model('encyDesc');
						$data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
						$a = $this->encyDesc->enc($data[0]->KDDPL);
						$data[0]->KODE = $this->encyDesc->encrypt($key,$a);
						$b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMADPL);
						$c = $this->encyDesc->encrypt($b,$data[0]->KODE);
						$a = 0;
						$data[0]->KUNCIKODEO = $c;
						$data[0]->KUNCIKODE = '';
						for ($i=0; $i <strlen($c) ; $i++) { 
							$data[0]->KUNCIKODE .= '<span style="font-size:'.(12+$a).'pt;">'.$c[$i].'</span>';
							$a +=3;
						}
						$tahun = explode('/', $data[0]->NAMATAHUN);
						$x = str_replace(array('%nomor%','%tahun%'), array($nomor,$tahun[1]), $this->mfungsi->setting('nosertifikat'));
						$data[0]->NOMOR = 'No. '.$x;
						$data[0]->AKADEMIK = $data[0]->NAMATAHUN;
						$data[0]->SEBAGAI = 'Sebagai <strong>KEPALA DOSEN PENDAMPING LAPANGAN TERBAIK</strong> pada kegiatan Kuliah Kerja Nyata<br>Universitas Madura Tahun Akademik '.$data[0]->NAMATAHUN;
						$x = explode('|', $this->mfungsi->setting('pimpinan_lppm'));
						$data[0]->PIMPINAN = (isset($x[0]))?$x[0]:'';
						$data[0]->NOMORPIMPINAN = (isset($x[1]))?$x[1]:'';
						echo json_encode($data);
					}
					exit;
				}
				$nomor = $mhs+1;
				$dosen = null;
				foreach ($dpl as $key => $value) {
					if($value->KDDPL == $kode){
						$dosen = $value;
						break;
					}
					$nomor++;
				}
				$nomor = $this->sertifikatModel->tambahNol($nomor);
				$data[0] = $dosen;
				$key = 'JEIBHCGDFA';
				$this->load->model('encyDesc');
				$data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
				$a = $this->encyDesc->enc($data[0]->KDDPL);
				$data[0]->KODE = $this->encyDesc->encrypt($key,$a);
				$b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMADPL);
				$c = $this->encyDesc->encrypt($b,$data[0]->KODE);
				$a = 0;
				$data[0]->KUNCIKODEO = $c;
				$data[0]->KUNCIKODE = '';
				for ($i=0; $i <strlen($c) ; $i++) { 
					$data[0]->KUNCIKODE .= '<span style="font-size:'.(12+$a).'pt;">'.$c[$i].'</span>';
					$a +=3;
				}
				$tahun = explode('/', $data[0]->NAMATAHUN);
				$x = str_replace(array('%nomor%','%tahun%'), array($nomor,$tahun[1]), $this->mfungsi->setting('nosertifikat'));
				$data[0]->NOMOR = 'No. '.$x;
				$data[0]->AKADEMIK = $data[0]->NAMATAHUN;
				$data[0]->SEBAGAI = 'Atas partisipasinya sebagai <strong>DOSEN PENDAMPING LAPANGAN</strong> pada kegiatan Kuliah Kerja Nyata<br>Universitas Madura Tahun Akademik '.$data[0]->NAMATAHUN;
				$x = explode('|', $this->mfungsi->setting('pimpinan_lppm'));
				$data[0]->PIMPINAN = (isset($x[0]))?$x[0]:'';
				$data[0]->NOMORPIMPINAN = (isset($x[1]))?$x[1]:'';
				$terbaik = $this->sertifikatModel->dpl_kkn($kode)[0]->TERBAIK;
				$kterbaik = $this->sertifikatModel->dpl_kkn($kode)[0]->KTERBAIK;
				$this->load->view('admin/sertifikat-dpl-print',array('data'=>$data,'dpl'=>$data[0]->KDDPL,'terbaik'=>$terbaik,'kterbaik'=>$kterbaik));
				break;
			default:
				$data = $this->sertifikatModel->dpl_kkn(null);
				$this->load->view('admin/header');
				$this->load->view('admin/sertifikat-dpl',array('data'=>$data));
				$this->load->view('admin/footer');
				break;
		}
		
	}

	public function create(){
		$nmr = $this->sertifikatModel->nomor($_POST['npm']);
		if($nmr->STATUS==false){
			echo json_encode(array('status'=>false,'data'=>$nmr->PESAN));
			exit;
		}
		$data = $this->sertifikatModel->peserta($_POST['npm']);
		if(count($data)>0){
			$key = 'JEIBHCGDFA';
			$this->load->model('encyDesc');
			$data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
			$a = $this->encyDesc->enc($data[0]->NPM);
			$data[0]->KODE = $this->encyDesc->encrypt($key,$a);
			$b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMAMHS);
			$data[0]->KUNCIKODE = $this->encyDesc->encrypt($b,$data[0]->KODE);
			$x = str_replace(array('%nomor%','%tahun%'), array($nmr->NOMOR,$nmr->TAHUN), $this->mfungsi->setting('nosertifikat'));
			$data[0]->NOMOR = 'No. '.$x;
			$data[0]->AKADEMIK = $nmr->AKADEMIK;
			$data[0]->TGLREGMHS = $this->mfungsi->tgl($data[0]->TGLREGMHS,true);
			echo json_encode(array('status'=>true,'data'=>$data));
		}
		else{
			echo json_encode(array('status'=>false,'data'=>'Anggota tidak ditemukan'));
		}
	}

	public function cek(){
		$this->load->view('admin/header');
		$this->load->view('admin/sertifikat-cek');
		$this->load->view('admin/footer');
	}

	public function periksa(){
		$key = 'JEIBHCGDFA'; // jangan mengganti atau menghapu kode pada baris ini
		$this->load->model('encyDesc');
		$mhs = count($this->sertifikatModel->peserta());
		$dpl = $this->sertifikatModel->dpl_kkn();
		$a = $this->encyDesc->decrypt($key,$_POST['npm']);
		$kode = $this->encyDesc->dec($a);
		$nomor = $mhs+1;
		$dosen = null;
		foreach ($dpl as $key => $value) {
			if($value->KDDPL == $kode){
				$dosen = $value;
				break;
			}
			$nomor++;
		}
		if($dosen!=null){
			$key = 'JEIBHCGDFA'; // jangan mengganti atau menghapu kode pada baris ini
			$nomor = $this->sertifikatModel->tambahNol($nomor);
			$data[0] = $dosen;
			$data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
			$a = $this->encyDesc->enc($data[0]->KDDPL);
			$data[0]->KODE = $this->encyDesc->encrypt($key,$a);
			$b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMADPL);
			$data[0]->KUNCIKODE = $this->encyDesc->encrypt($b,$data[0]->KODE);
			$tahun = explode('/', $data[0]->NAMATAHUN);
			$x = str_replace(array('%nomor%','%tahun%'), array($nomor,$tahun[1]), $this->mfungsi->setting('nosertifikat'));
			$data[0]->NOMOR = 'No. '.$x;
			$x = explode('|', $this->mfungsi->setting('pimpinan_lppm'));
			$data[0]->PIMPINAN = (isset($x[0]))?$x[0]:'';
			$data[0]->NOMORPIMPINAN = (isset($x[1]))?$x[1]:'';
			$data[0]->LEVEL = 'dosen';
			$data[0]->AKADEMIK = $data[0]->NAMATAHUN;
			$data[0]->SEBAGAI = 'Atas partisipasinya sebagai <strong>DOSEN PENDAMPING LAPANGAN</strong> pada kegiatan Kuliah Kerja Nyata<br>Universitas Madura Tahun Akademik '.$data[0]->NAMATAHUN;
			echo json_encode(array('status'=>true,'data'=>$data));
		}else{
			$key = 'JEIBHCGDFA';
			$a = $this->encyDesc->decrypt($key,$_POST['npm']);
			$npm = $this->encyDesc->dec($a);
			$nmr = $this->sertifikatModel->nomor($npm);
			if($nmr->STATUS==false){
				$key = 'ZYWQVARDFKL';
				$nomor = $mhs+count($dpl)+1;
				$dosen = null;
				$dplt = $this->sertifikatModel->dpl_kkn_terbaik();

				$a = $this->encyDesc->decrypt($key,$_POST['npm']);
				$kode = $this->encyDesc->dec($a);

				foreach ($dplt as $key => $value) {
					if($value->KDDPL == $kode){
						$dosen = $value;
						break;
					}
					$nomor++;
				}
				if($dosen==null){
					$key = 'DCBAKJIHNPS';
					$nomor = $mhs+count($dpl)+1;
					$dosen = null;
					$a = $this->encyDesc->decrypt($key,$_POST['npm']);
					$kode = $this->encyDesc->dec($a);
					$nomor += count($this->sertifikatModel->dpl_kkn_terbaik());
					$dplt = $this->sertifikatModel->kdpl_kkn_terbaik();
					foreach ($dplt as $key => $value) {
						if($value->KDDPL == $kode){
							$dosen = $value;
							break;
						}
						$nomor++;
					}
					if($dosen==null){
						echo json_encode(array('status'=>false,'data'=>'Sertifikat tidak tersedia'));
						exit;
					}
					$nomor = $this->sertifikatModel->tambahNol($nomor);
					$data[0] = $dosen;
					$data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
					$a = $this->encyDesc->enc($data[0]->KDDPL);
					$key = 'DCBAKJIHNPS';
					$data[0]->KODE = $this->encyDesc->encrypt($key,$a);
					$b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMADPL);
					$data[0]->KUNCIKODE = $this->encyDesc->encrypt($b,$data[0]->KODE);
					$tahun = explode('/', $data[0]->NAMATAHUN);
					$x = str_replace(array('%nomor%','%tahun%'), array($nomor,$tahun[1]), $this->mfungsi->setting('nosertifikat'));
					$data[0]->NOMOR = 'No. '.$x;
					$x = explode('|', $this->mfungsi->setting('pimpinan_lppm'));
					$data[0]->PIMPINAN = (isset($x[0]))?$x[0]:'';
					$data[0]->NOMORPIMPINAN = (isset($x[1]))?$x[1]:'';
					$data[0]->LEVEL = 'dosen';
					$data[0]->AKADEMIK = $data[0]->NAMATAHUN;
					$data[0]->SEBAGAI = 'Sebagai <strong>KEPALA DOSEN PENDAMPING LAPANGAN TERBAIK</strong> pada kegiatan Kuliah Kerja Nyata<br>Universitas Madura Tahun Akademik '.$data[0]->NAMATAHUN;
					echo json_encode(array('status'=>true,'data'=>$data));
					exit;
				}
				$key = 'ZYWQVARDFKL';
				$nomor = $this->sertifikatModel->tambahNol($nomor);
				$data[0] = $dosen;
				$data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
				$a = $this->encyDesc->enc($data[0]->KDDPL);
				$data[0]->KODE = $this->encyDesc->encrypt($key,$a);
				$b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMADPL);
				$data[0]->KUNCIKODE = $this->encyDesc->encrypt($b,$data[0]->KODE);
				$a = 0;
				$tahun = explode('/', $data[0]->NAMATAHUN);
				$x = str_replace(array('%nomor%','%tahun%'), array($nomor,$tahun[1]), $this->mfungsi->setting('nosertifikat'));
				$data[0]->NOMOR = 'No. '.$x;
				$data[0]->AKADEMIK = $data[0]->NAMATAHUN;
				$data[0]->SEBAGAI = 'Sebagai <strong>DOSEN PENDAMPING LAPANGAN TERBAIK</strong> pada kegiatan Kuliah Kerja Nyata<br>Universitas Madura Tahun Akademik '.$data[0]->NAMATAHUN;
				$x = explode('|', $this->mfungsi->setting('pimpinan_lppm'));
				$data[0]->PIMPINAN = (isset($x[0]))?$x[0]:'';
				$data[0]->NOMORPIMPINAN = (isset($x[1]))?$x[1]:'';
				echo json_encode(array('status'=>true,'data'=>$data));
				exit;
			}
			$data = $this->sertifikatModel->peserta($npm);
			if(count($data)>0){
				$data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
				$data[0]->KODE = $_POST['npm'];

				$b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMAMHS);
				$data[0]->KUNCIKODE = $this->encyDesc->encrypt($b,$data[0]->KODE);
				$x = str_replace(array('%nomor%','%tahun%'), array($nmr->NOMOR,$nmr->TAHUN), $this->mfungsi->setting('nosertifikat'));
				$data[0]->NOMOR = 'No. '.$x;
				$data[0]->AKADEMIK = $nmr->AKADEMIK;
				$data[0]->TGLREGMHS = $this->mfungsi->tgl($data[0]->TGLREGMHS,true);
				$data[0]->LEVEL = 'mahasiswa';
				$data[0]->SEBAGAI = 'Sebagai peserta pada kegiatan Kuliah Kerja Nyata (KKN) <br>Universitas Madura Tahun Akademik '.$data[0]->NAMATAHUN;
				echo json_encode(array('status'=>true,'data'=>$data));
			}else{
				echo json_encode(array('status'=>false,'data'=>'Sertifikat tidak tersedia'));
			}
		}
	}

	public function cetak($npm=''){
		$nmr = $this->sertifikatModel->nomor($npm);
		if($nmr->STATUS==false){
			echo $nmr->PESAN;
			exit;
		}
		$data = $this->sertifikatModel->peserta($npm);
		if(count($data)>0){
			$key = 'JEIBHCGDFA';
			$this->load->model('encyDesc');
			$data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
			$a = $this->encyDesc->enc($data[0]->NPM);
			$data[0]->KODE = $this->encyDesc->encrypt($key,$a);
			$b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMAMHS);
			$data[0]->KUNCIKODE = $this->encyDesc->encrypt($b,$data[0]->KODE);
			$x = str_replace(array('%nomor%','%tahun%'), array($nmr->NOMOR,$nmr->TAHUN), $this->mfungsi->setting('nosertifikat'));
			$data[0]->NOMOR = 'No. '.$x;
			$data[0]->AKADEMIK = $nmr->AKADEMIK;
			$x = explode('|', $this->mfungsi->setting('pimpinan_lppm'));
			$data[0]->PIMPINAN = (isset($x[0]))?$x[0]:'';
			$data[0]->NOMORPIMPINAN = (isset($x[1]))?$x[1]:'';
			$data[0]->TGLREGMHS = $this->mfungsi->tgl($data[0]->TGLREGMHS,true);
			$this->load->view('admin/print/sertifikat',array('data'=>$data));
		}
		else{
			echo 'Anggota tidak ditemukan';
		}
	}

}

/* End of file sertifikat.php */
/* Location: ./application/controllers/admin/sertifikat.php */