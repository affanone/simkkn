<?php

// Model B
// jarak terdekat mahasiswa ke kelompok
// tanpa batas kapasitas per prodi

defined('BASEPATH') OR exit('No direct script access allowed');

class Gen extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/adminModel');
		$this->mlogin->cek();
	}
	
	public function ulang(){
		$this->load->model('metode/aModel');
		$this->aModel->delJarakKelompok();
		$this->aModel->delKelompokPeserta();
		$this->aModel->delKapasitasKkn();
		$this->aModel->setStatusMhsNol();
	}

	public function getkordinat(){
		$this->load->model('metode/aModel');
		$npm = $_POST['npm'];
		$alamat = $_POST['alamat'];
		$cek = $this->mfungsi->kordinat($alamat);
		if($cek->status==true){
			$this->aModel->setKordinat(array(
				'alamat'=>$cek->alamat,
				'lat'=>$cek->lat,
				'lng'=>$cek->lng,
				'npm'=>$npm
			));
		}
		echo json_encode($cek);
	}

	public function getkordinatAll(){
		$this->load->model('metode/aModel');
		set_time_limit(0);
		$this->mremaining->start();
		$noKordinat = $this->aModel->mhsNokordinat();
		foreach ($noKordinat as $key => $value) {
			$cek = $this->mfungsi->kordinat($value->ALAMATMHS);
			if($cek->status==true){
				$this->aModel->setKordinat(array(
					'alamat'=>$cek->alamat,
					'lat'=>$cek->lat,
					'lng'=>$cek->lng,
					'npm'=>$value->NPM
				));
			}
			$this->mremaining->running(array(
				'data'=>$cek,
				'npm'=>$value->NPM
			));
		}
		$this->mremaining->finish();
	}

	public function find_distance_geometry()
	{
		$this->load->model('metode/aModel');
		set_time_limit(0);
		$this->mremaining->start();
		$mahasiswa = $this->aModel->getMhsGeometry();
		$kelompok = $this->aModel->getKelompok();
		$total = count($mahasiswa)*count($kelompok);
		$persen = 100 / $total;
		$i = 0;
		$data = array();
		$thn = null;
		foreach ($mahasiswa as $m => $mhs) {
			$j = 1;
			foreach ($kelompok as $k => $kel) {
				$thn = $kel->KDTAHUN;
				//√((x_(1-) x_2 )^2+(y_(1-) y_2 )^2 )
				// $a = pow($mhs->KORDXMHS - $kel->KORDXKEL,2);
				// $b = pow($mhs->KORDYMHS - $kel->KORDYKEL,2);
				// $c = sqrt($a + $b);
				array_push($data,array(
					'NPM'=>$mhs->NPM,
					'KDKEL'=>$kel->KDKEL,
					'KDPRODI'=>$mhs->KDPRODI,
					'NILAIJARAK'=>$this->mfungsi->getDistance($mhs->KORDXMHS,$mhs->KORDYMHS,$kel->KORDXKEL,$kel->KORDYKEL)
				));
				$this->mremaining->running(floor($persen*$i));
				$j++;
				$i++;
			}
		}
		$this->mfile->write(array(
			'file'=>'./jarak/'.$thn.'.json',
			'data'=>json_encode($data, JSON_PRETTY_PRINT)
		));
		$this->mremaining->finish();
	}

	public function bentuk_anggota($data,$kelompok,$npms=array()){
		$persen = 0;
		$persen1 = count($data)-count($npms);
		$persen2 = floor(100/$persen1);
		$persen3 = 0;
		$perkelompok = floor(count($data)/count($kelompok));
		$anggotakel = array();
		$maksimalkel = array();
		foreach ($data as $npm => $v1) {
			if(!in_array($npm, $npms)){
				$tmp = 10000000;
				$kelpilih = null;
				foreach ($v1 as $kel => $v2) {
					if(array_key_exists($kel, $maksimalkel)){
						if($maksimalkel[$kel]<$perkelompok){
							if($v2<$tmp){
								$tmp = $v2;
								$kelpilih = $kel;
							}
						}
					}else{
						if($v2<$tmp){
							$tmp = $v2;
							$kelpilih = $kel;
						}
					}					
				}
				if(!array_key_exists($kelpilih, $maksimalkel))
					$maksimalkel[$kelpilih] = 0;	
				$maksimalkel[$kelpilih]++;
				if(!array_key_exists($kelpilih, $anggotakel))
					if($kelpilih!=null)
						$anggotakel[$kelpilih] = array();
				if($kelpilih!=null){
					array_push($anggotakel[$kelpilih], $npm);
					array_push($npms, $npm);
					$this->mremaining->running(array('persen'=>floor($persen2*$persen3),'pesan'=>'Pembentukan kelompok KKN'));
					$persen3++;
				}
			}
		}
			return array(
				'anggotakel'=>$anggotakel,
				'npms'=>$npms
			);
	}

	public function pemetaan(){
		$this->load->model('metode/aModel');
		set_time_limit(0);
		$this->mremaining->start();
		$jarak = $this->aModel->jarak_kelompok();
		$prodi = $this->aModel->getProdi();
		$kelompok = $this->aModel->getKelompok();
		$persen = count($jarak)+count($prodi)+count($kelompok);
		$persen1 = 100/$persen;
		$persen2 = 0;
		$p32c = array();
		$data = array();
		$prodimhs = array();
		foreach ($jarak as $key => $value) {
			//prodi mahasiswa
			if(!array_key_exists($value->NPM, $prodimhs))
				$prodimhs[$value->NPM] = $value->KDPRODI;
			if(!array_key_exists($value->NPM, $data)){
				$data[$value->NPM] = array();
			}
			$data[$value->NPM][$value->KDKEL] = $value->NILAIJARAK;
			
			$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Mengambil nilai jarak antara mahasiswa dan kelompok pada setiap peserta'));
			$persen2++;
		}

		$datakelompok = array();
		$btk = $this->bentuk_anggota($data,$kelompok);
		$datakelompok = $btk['anggotakel'];
		$bentuk = $this->bentuk_anggota($data,$kelompok,$btk['npms']);
		foreach ($bentuk['anggotakel'] as $kel => $value) {
			foreach ($value as $key => $npm) {
				if(!array_key_exists($kel, $datakelompok))
					$datakelompok[$kel] = array();
				array_push($datakelompok[$kel], $npm);
			}
		}
		$kodeprodi = array();
		foreach ($prodi as $key => $value) {
			$kodeprodi[$value->KDPRODI] = 0;
		}
		$kapasitas = array();
		$mulai = 0;
		$total = 0;
		$this->mremaining->running(array('persen'=>0,'pesan'=>'Tentukan kapasitas pada masing-masing kelompok KKN'));

		$datakap = array();
		foreach ($datakelompok as $kel => $value) {
			$kaprodi = $kodeprodi;
			foreach ($value as $key => $npm) {
				$kaprodi[$prodimhs[$npm]]++;
			}
			foreach ($kaprodi as $prd => $value) {
				array_push($datakap, array(
					'KDPRODI'=>$prd,
					'KDKEL'=>$kel,
					'KAPRODI'=>$value
				));
			}
		}
		$this->mremaining->running(array('persen'=>0,'pesan'=>'Simpang kapasitas kelompok KKN'));
		$this->aModel->delKapasitasKkn();
		$this->db->insert_batch('kapasitas_prodi', $datakap);

		$persen = 0;
		foreach ($datakelompok as $kel => $value) {
			$persen += count($value);
		}
		$persen1 = 100/$persen;
		$persen2 = 0;

		foreach ($datakelompok as $kel => $value) {
			foreach ($value as $key => $npm) {
				$this->aModel->verifikasi($npm);
				$this->aModel->kelompok_peserta($kel,$npm);
				$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Simpan kelompok KKN yang sudah dibentuk'));
				$persen2++;
			}
		}
		$this->mremaining->finish();
	}

	public function order_asc($datax){
		for ($i=0; $i < count($datax)-1 ; $i++) { 
			$tmp = null;
			for ($j=$i+1; $j < count($datax) ; $j++) { 
				$a = $datax[$i]['jarak'];
				$b = $datax[$j]['jarak'];
				if($b<$a){
					$tmp = $datax[$j];
					$datax[$j] = $datax[$i];
					$datax[$i] = $tmp;
				}
			}
		}
		return $datax;
	}

	public function group_select($datax,$no_kel){
		$skor = array();
		foreach ($datax as $kel => $value) {
			if(!in_array($kel, $no_kel)){
				$total = 0;
				foreach ($value as $key => $value_a) {
					$total = $total + $value_a['jarak'];
				}
				array_push($skor, array(
					'kelompok'=>$kel,
					'jarak'=>$total
				));
			}
		}

		for ($i=0; $i < count($skor)-1 ; $i++) { 
			$tmp = null;
			for ($j=$i+1; $j < count($skor) ; $j++) { 
				$a = $skor[$i]['jarak'];
				$b = $skor[$j]['jarak'];
				// perbandingan pada nilai penjumlahan semua jarak di satu kelompok
				if($b<$a){
					$tmp = $skor[$j];
					$skor[$j] = $skor[$i];
					$skor[$i] = $tmp;
				}
			}
		}

		return array(
			'anggota'=>$datax[$skor[0]['kelompok']],
			'rincian'=>$skor[0]
		);
	}

}

/* End of file Metode_a.php */
/* Location: ./application/controllers/admin/metode/Metode_a.php */