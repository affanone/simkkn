<?php
// Model A
// prodi dibagi rata
// jarak terkecil mahasiswa
// ambil jumlah terkecil dari kelompok yang telah dipilih

defined('BASEPATH') OR exit('No direct script access allowed');

class Gen extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/adminModel');
		$this->mlogin->cek();
	}
	
	public function ulang(){
		$this->load->model('metode/aModel');
		$this->aModel->delJarakKelompok();
		$this->aModel->delKelompokPeserta();
		$this->aModel->delKapasitasKkn();
		$this->aModel->setStatusMhsNol();
	}

	public function getkordinat(){
		$this->load->model('metode/aModel');
		$npm = $_POST['npm'];
		$alamat = $_POST['alamat'];
		$cek = $this->mfungsi->kordinat($alamat);
		if($cek->status==true){
			$this->aModel->setKordinat(array(
				'alamat'=>$cek->alamat,
				'lat'=>$cek->lat,
				'lng'=>$cek->lng,
				'npm'=>$npm
			));
		}
		echo json_encode($cek);
	}

	public function getkordinatAll(){
		$this->load->model('metode/aModel');
		set_time_limit(0);
		$this->mremaining->start();
		$noKordinat = $this->aModel->mhsNokordinat();
		foreach ($noKordinat as $key => $value) {
			$cek = $this->mfungsi->kordinat($value->ALAMATMHS);
			if($cek->status==true){
				$this->aModel->setKordinat(array(
					'alamat'=>$cek->alamat,
					'lat'=>$cek->lat,
					'lng'=>$cek->lng,
					'npm'=>$value->NPM
				));
			}
			$this->mremaining->running(array(
				'data'=>$cek,
				'npm'=>$value->NPM
			));
		}
		$this->mremaining->finish();
	}

	public function find_distance_geometry()
	{
		$this->load->model('metode/aModel');
		set_time_limit(0);
		$this->mremaining->start();
		$mahasiswa = $this->aModel->getMhsGeometry();
		$kelompok = $this->aModel->getKelompok();
		$total = count($mahasiswa)*count($kelompok);
		$persen = 100 / $total;
		$i = 0;
		$data = array();
		$thn = null;
		foreach ($mahasiswa as $m => $mhs) {
			$j = 1;
			foreach ($kelompok as $k => $kel) {
				$thn = $kel->KDTAHUN;
				//√((x_(1-) x_2 )^2+(y_(1-) y_2 )^2 )
				// $a = pow($mhs->KORDXMHS - $kel->KORDXKEL,2);
				// $b = pow($mhs->KORDYMHS - $kel->KORDYKEL,2);
				// $c = sqrt($a + $b);
				array_push($data,array(
					'NPM'=>$mhs->NPM,
					'KDKEL'=>$kel->KDKEL,
					'KDPRODI'=>$mhs->KDPRODI,
					'NILAIJARAK'=>$this->mfungsi->getDistance($mhs->KORDXMHS,$mhs->KORDYMHS,$kel->KORDXKEL,$kel->KORDYKEL)
				));
				$this->mremaining->running(floor($persen*$i));
				$j++;
				$i++;
			}
		}
		$this->mfile->write(array(
			'file'=>'./jarak/'.$thn.'.json',
			'data'=>json_encode($data, JSON_PRETTY_PRINT)
		));
		$this->mremaining->finish();
	}

	public function pemetaan(){
		$this->load->model('metode/aModel');
		set_time_limit(0);
		$this->mremaining->start();
		$jarak = $this->aModel->jarak_kelompok();
		$prodi = $this->aModel->getProdi();
		$kelompok = $this->aModel->getKelompok();
		$data = array();
		$order1 = array();
		$order = array();
		$persen = count($jarak)+count($prodi)+count($kelompok);
		$persen1 = 100/$persen;
		$persen2 = 0;
		$p32c = array();
		foreach ($jarak as $key => $value) {
			if(!in_array($value->KDPRODI, $p32c)){
				array_push($p32c, $value->KDPRODI);
				$order1[$value->KDPRODI] = array(
					'prodi'=>$value->KDPRODI,
					'value'=>$value->NILAIJARAK
				);
			}else{
				$order1[$value->KDPRODI]['value'] += $value->NILAIJARAK;
			}
			if(!array_key_exists($value->KDPRODI, $data)){
				$data[$value->KDPRODI] = array();
			}
			if(!array_key_exists($value->NPM, $data[$value->KDPRODI])){
				$data[$value->KDPRODI][$value->NPM] = array();
			}
			if(!array_key_exists($value->KDKEL, $data[$value->KDPRODI][$value->NPM])){
				$data[$value->KDPRODI][$value->NPM][$value->KDKEL] = array();
			}
			$data[$value->KDPRODI][$value->NPM][$value->KDKEL] = $value->NILAIJARAK;
			$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Mengambil nilai jarak antara mahasiswa dan kelompok pada setiap peserta'));
			$persen2++;
		}

		$order3 = array();
		foreach ($data as $key => $value) {
			$order3[$key] = array(
				'prodi'=>$key,
				'value'=>count($value)
			);
		}

		//normal index array order
		foreach ($order1 as $key => $value) {
			array_push($order, $value);
		}

		//order 
		for ($i=0; $i < count($order)-1 ; $i++) { 
			$tmp = null;
			for ($j=$i+1; $j < count($order) ; $j++) { 
				$a = $order[$i]['value'];
				$b = $order[$j]['value'];
				$ap = $order[$i]['prodi'];
				$bp = $order[$j]['prodi'];
				if($b<$a){
					$tmp1 = $order[$j];
					$order[$j] = $order[$i];
					$order[$i] = $tmp1;

					$tmp2 = $order3[$bp];
					$order3[$bp] = $order3[$ap];
					$order3[$ap] = $tmp2;
				}
			}
		}

		//normal index array order3
		$order = array();
		foreach ($order3 as $key => $value) {
			array_push($order, $value);
		}

		$orderkel = array();

		foreach ($kelompok as $key => $value) {
			array_push($orderkel, $value->KDKEL);
		}

		$kapasitas = array();
		$mulai = 0;
		$total = 0;


		$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Tentukan kapasitas pada masing-masing kelompok KKN'));

		foreach ($order as $key => $value) {
			for ($i=0; $i < $value['value']; $i++) { 
				if(!array_key_exists($orderkel[$mulai], $kapasitas)){
					$kapasitas[$orderkel[$mulai]] = array();
				}

				if(!array_key_exists($value['prodi'], $kapasitas[$orderkel[$mulai]])){
					$kapasitas[$orderkel[$mulai]][$value['prodi']] = 0;
				}
				$kapasitas[$orderkel[$mulai]][$value['prodi']]++;

				$mulai++;
				$total++;
				if($mulai>count($orderkel)-1)
					$mulai = 0;
			}
		}

		$perkel = floor($total/count($kelompok));

		foreach ($kapasitas as $key => $value) {
			foreach ($prodi as $prd => $vprd) {
				if(!array_key_exists($vprd->KDPRODI, $value))
					$kapasitas[$key][$vprd->KDPRODI] = 0;
			}
		}

		$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Simpang kapasitas kelompok KKN'));

		$this->aModel->delKapasitasKkn();
		$this->aModel->setKapasitasKkn($kapasitas);

		
		$npms = array();
		$kels = array();
		$selects = array();

		$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Kalkulasi...'));
		$persen = count($kelompok)*count($kapasitas);
		$persen1 = 100/$persen;
		$persen2 = 0;

		for ($k=0; $k<count($kelompok); $k++) { 
			$data_a = array();
			foreach ($kapasitas as $kel => $value) {
				if(!in_array($kel, $kels)){
					foreach ($value as $prodi => $vprodi) {
						$n1 = array();
						foreach ($data[$prodi] as $npm => $vnpm) {
							if(!in_array($npm, $npms)){
								array_push($n1, array(
									'npm'=>$npm,
									'jarak'=>$vnpm[$kel]
								));
							}
						}
						$n1 = $this->order_asc($n1);
						for ($i=0; $i<$vprodi; $i++) {
							if(!array_key_exists($kel, $data_a))
								$data_a[$kel] = array();
							array_push($data_a[$kel], array(
								'npm'=>$n1[$i]['npm'],
								'prodi'=>$prodi,
								'jarak'=>$n1[$i]['jarak']
							));
						}
					}
				}
				$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Pembentukan kelompok KKN'));
					$persen2++;
			}

			$pilih = $this->group_select($data_a,$kels);
			foreach ($pilih['anggota'] as $key => $value) {
				$kapasitas[$pilih['rincian']['kelompok']][$value['prodi']]--;
				array_push($npms, $value['npm']);
			}
			array_push($kels, $pilih['rincian']['kelompok']);
			array_push($selects, $pilih);
		}

		$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Kalkulasi...'));
		$persen = 0;
		foreach ($selects as $k0 => $value) {
			$persen += count($value['anggota']);
		}
		$persen1 = 100/$persen;
		$persen2 = 0;
		foreach ($selects as $k0 => $value) {
			foreach ($value['anggota'] as $k1 => $k2) {
				$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Simpan kelompok KKN yang sudah dibentuk'));
					$persen2++;
				$this->aModel->verifikasi($k2['npm']);
				$this->aModel->kelompok_peserta($value['rincian']['kelompok'],$k2['npm']);
			}
		}		
		$this->mremaining->finish();
	}

	public function order_asc($datax){
		for ($i=0; $i < count($datax)-1 ; $i++) { 
			$tmp = null;
			for ($j=$i+1; $j < count($datax) ; $j++) { 
				$a = $datax[$i]['jarak'];
				$b = $datax[$j]['jarak'];
				if($b<$a){
					$tmp = $datax[$j];
					$datax[$j] = $datax[$i];
					$datax[$i] = $tmp;
				}
			}
		}
		return $datax;
	}

	public function group_select($datax,$no_kel){
		$skor = array();
		foreach ($datax as $kel => $value) {
			if(!in_array($kel, $no_kel)){
				$total = 0;
				foreach ($value as $key => $value_a) {
					$total = $total + $value_a['jarak'];
				}
				array_push($skor, array(
					'kelompok'=>$kel,
					'jarak'=>$total
				));
			}
		}

		for ($i=0; $i < count($skor)-1 ; $i++) { 
			$tmp = null;
			for ($j=$i+1; $j < count($skor) ; $j++) { 
				$a = $skor[$i]['jarak'];
				$b = $skor[$j]['jarak'];
				if($b<$a){
					$tmp = $skor[$j];
					$skor[$j] = $skor[$i];
					$skor[$i] = $tmp;
				}
			}
		}

		return array(
			'anggota'=>$datax[$skor[0]['kelompok']],
			'rincian'=>$skor[0]
		);
	}

}

/* End of file Metode_a.php */
/* Location: ./application/controllers/admin/metode/Metode_a.php */