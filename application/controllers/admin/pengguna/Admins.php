<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admins extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/adminModel');
		$this->mlogin->cek();
	}

	public function index()
	{
		$this->mhistory->go();
		$this->load->view('admin/header');
		$data['data'] = $this->adminModel->view();
		$this->load->view('admin/admin',$data);
		$this->load->view('admin/footer');
	}

	public function tambah(){
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'tambah';
		$data['username'] = '';
		$data['nama'] = '';
		$data['pass'] = '';
		$data['rpass'] = '';
		$this->load->view('admin/formAdmin',$data);
		$this->load->view('admin/footer');
	}

	public function edit($kode){
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'edit';
		$data['kode'] = $kode;
		$load = $this->adminModel->load($kode);
		$data['username'] = $load->USERN;
		$data['nama'] = $load->NAMA;
		$data['pass'] = '';
		$data['rpass'] = '';
		$this->load->view('admin/formAdmin',$data);
		$this->load->view('admin/footer');
	}

	public function simpan(){
		$this->form_validation->set_rules('username', 's', 'trim|required|callback_cek_nama|alpha_numeric',array(
			'required'=>'Username tidak boleh kosong',
			'cek_nama'=>'Username "'.$_POST['username'].'" sudah digunakan',
			'alpha_numeric'=>'Format username tidak dibolehkan'
		));

		$this->form_validation->set_rules('nama', 's', 'trim|required|regex_match[/^[a-z][a-z -]*$/i]',array(
			'required'=>'Nama tidak boleh kosong',
			'regex_match'=>'Format nama tidak dibolehkan'
		));

		$this->form_validation->set_rules('pass', 's', 'trim|required',array(
			'required'=>'Password tidak boleh kosong',
		));

		$this->form_validation->set_rules('rpass', 's', 'trim|matches[pass]',array(
			'matches'=>'Pengulangan password tidak sama'
		));



		if ($this->form_validation->run() == FALSE)
    {
      $this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'tambah';
			$data['username'] = set_value('username');
			$data['nama'] = set_value('nama');
			$data['pass'] = set_value('pass');
			$data['rpass'] = set_value('rpass');
			$this->load->view('admin/formAdmin',$data);
			$this->load->view('admin/footer');
    }
    else
    {
     	$this->adminModel->simpan($_POST);
      redirect($this->mhistory->back(),'refresh');
    }
	}

	public function update($kode){

		$this->form_validation->set_rules('username', 's', 'trim|required|callback_cek_nama_update|alpha_numeric',array(
			'required'=>'Username tidak boleh kosong',
			'cek_nama'=>'Username "'.$_POST['username'].'" sudah digunakan',
			'alpha_numeric'=>'Format username tidak dibolehkan'
		));

		$this->form_validation->set_rules('nama', 's', 'trim|required|regex_match[/^[a-z][a-z -]*$/i]',array(
			'required'=>'Nama tidak boleh kosong',
			'regex_match'=>'Format nama tidak dibolehkan'
		));

		if($_POST['pass']!=''){

			$this->form_validation->set_rules('pass', 's', 'trim|required',array(
				'required'=>'Password tidak boleh kosong',
			));

			$this->form_validation->set_rules('rpass', 's', 'trim|matches[pass]',array(
				'matches'=>'Pengulangan password tidak sama'
			));

		}



		if ($this->form_validation->run() == FALSE)
    {
      $this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'edit';
			$data['kode'] = $kode;
			$data['username'] = set_value('username');
			$data['nama'] = set_value('nama');
			$data['pass'] = set_value('pass');
			$data['rpass'] = set_value('rpass');
			$this->load->view('admin/formAdmin',$data);
			$this->load->view('admin/footer');
    }
    else
    {
     	$this->adminModel->update($_POST,$kode);
      redirect($this->mhistory->back(),'refresh');
    }
	}


	public function hapus($kode){
		$this->adminModel->hapus($kode);
    redirect($this->mhistory->back(),'refresh');
	}

	public function cek_nama(){
		$this->db->where('USERN', $_POST['username']);
    $query = $this->db->get('users');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

	public function cek_nama_update(){
		$this->db->where('USERN', $_POST['username']);
		$this->db->where('USERN!=', $this->uri->segment(5));
    $query = $this->db->get('users');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

}

/* End of file tahun.php */
/* Location: ./application/controllers/admin/master/tahun.php */