<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reg extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$this->load->model('admin/daftarModel');
	}
	public function index()
	{
		$this->data();
	}

	public function data($page = 1){
		$this->mhistory->go();
		$cari = '';
		if(isset($_POST['cari'])){
			$cari = $_POST['cari'];
		}
		$page = (5*($page-1));
		$nomor = $page+1;
		$total = $this->daftarModel->totaldata($cari);
		$this->mhistory->go();
		if(!isset($_POST['ajax']))
			$this->load->view('admin/header');
		$data['data'] = $this->daftarModel->view($page,$cari);
		$data['nomor'] = $nomor;
		$data['total'] = $total;
		$data['cari'] = $cari;
		$this->load->view('admin/mendaftar',$data);
		if(!isset($_POST['ajax']))
			$this->load->view('admin/footer');
	}

	public function konfirm($npm){
		$this->daftarModel->konfirm($npm);
		$this->daftarModel->delete($npm);
		redirect($this->mhistory->back());
	}

	public function konfirmall(){
		$mhs = $this->daftarModel->mhsDaftar();
		foreach ($mhs as $key => $value) {
			$this->daftarModel->konfirm($value->NPM);
		}
		$this->daftarModel->deleteAll();
		redirect($this->mhistory->back());
	}

	public function hapus($npm){
		$this->daftarModel->delete($npm);
		redirect($this->mhistory->back());
	}

	public function hapusall($npm){
		$this->daftarModel->deleteAll();
		redirect($this->mhistory->back());
	}

}

/* End of file daftar.php */
/* Location: ./application/controllers/admin/daftar.php */