<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/kecamatanModel');
		$this->mlogin->cek();
	}

	public function index()
	{
		$this->mhistory->go();
		$this->load->view('admin/header');
		$data['data'] = $this->kecamatanModel->view();
		$this->load->view('admin/kecamatan',$data);
		$this->load->view('admin/footer');
	}

	public function tambah(){
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'tambah';
		$data['kecamatan'] = '';
		$data['dataKab'] = $this->kecamatanModel->kabupaten();
		$data['kabupaten'] = '';
		$this->load->view('admin/formKecamatan',$data);
		$this->load->view('admin/footer');
	}

	public function edit($kode){
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'edit';
		$data['kode'] = $kode;
		$load = $this->kecamatanModel->load($kode);
		$data['kecamatan'] = $load->NAMAKEC;
		$data['dataKab'] = $this->kecamatanModel->kabupaten();
		$data['kabupaten'] = $load->KDKAB;
		$this->load->view('admin/formKecamatan',$data);
		$this->load->view('admin/footer');
	}

	public function simpan(){
		$this->form_validation->set_rules('kecamatan', 's', 'trim|required|callback_cek_nama',array(
			'required'=>'Nama kecamatan harus diisi',
			'cek_nama'=>'Nama kecamatan "'.$_POST['kecamatan'].'" sudah digunakan'
		));

		$this->form_validation->set_rules('kabupaten', 's', 'trim|required',array(
			'required'=>'Kabupaten harus dipilih'
		));



		if ($this->form_validation->run() == FALSE)
    {
      $this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'tambah';
			$data['kecamatan'] = set_value('kecamatan');
			$data['dataKab'] = $this->kecamatanModel->kabupaten();
			$data['kabupaten'] = set_value('kabupaten');
			$this->load->view('admin/formKecamatan',$data);
			$this->load->view('admin/footer');
    }
    else
    {
     	$this->kecamatanModel->simpan($_POST);
      redirect($this->mhistory->back(),'refresh');
    }
	}

	public function update($kode){

		$this->form_validation->set_rules('kecamatan', 's', 'trim|required|callback_cek_nama_update',array(
			'required'=>'Nama kecamatan harus diisi',
			'cek_nama_update'=>'Nama kecamatan "'.$_POST['kecamatan'].'" sudah digunakan'
		));

		$this->form_validation->set_rules('kabupaten', 's', 'trim|required',array(
			'required'=>'Kabupaten harus dipilih'
		));



		if ($this->form_validation->run() == FALSE)
    {
      $this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'edit';
			$data['kode'] = $kode;
			$data['kecamatan'] = set_value('kecamatan');
			$data['dataKab'] = $this->kecamatanModel->kabupaten();
			$data['kabupaten'] = set_value('kabupaten');
			$this->load->view('admin/formKecamatan',$data);
			$this->load->view('admin/footer');
    }
    else
    {
     	$this->kecamatanModel->update($_POST,$kode);
      redirect($this->mhistory->back(),'refresh');
    }
	}


	public function hapus($kode){
		$this->kecamatanModel->hapus($kode);
    redirect($this->mhistory->back(),'refresh');
	}

	public function cek_nama(){
		$this->db->where('NAMAKEC', $_POST['kecamatan']);
		$this->db->where('KDKAB', $_POST['kabupaten']);
    $query = $this->db->get('kecamatan');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

	public function cek_nama_update(){
		$this->db->where('NAMAKEC', $_POST['kecamatan']);
		$this->db->where('KDKEC!=', $this->uri->segment(5));
		$this->db->where('KDKAB', $_POST['kabupaten']);
    $query = $this->db->get('kecamatan');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

}

/* End of file tahun.php */
/* Location: ./application/controllers/admin/master/tahun.php */