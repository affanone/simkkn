<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$this->load->model('admin/nilaiModel');
	}
	public function index()
	{
		$this->view();
	}

	public function view($prd=null){
		$this->mhistory->go();
		$prodi = $this->nilaiModel->prodi();
		$prodata = array();
		foreach ($prodi as $key => $value) {
			$prodata[$value->KDPRODI] = $value->FAKPRODI.' / '.$value->NAMAPRODI;
		}
		if($prd!=null){
			$proaktif['kode'] = $prd;
			$proaktif['nama'] = $prodata[$prd];
			$data = $this->nilaiModel->get($prd);
		}
		else{
			$proaktif['kode'] = $prodi[0]->KDPRODI;
			$proaktif['nama'] = $prodi[0]->NAMAPRODI;
			$data = $this->nilaiModel->get($proaktif['kode']);
		}
		$this->load->view('admin/header');
		$this->load->view('admin/statistika-nilai',array(
			'data'=>$data,
			'prodi'=>$prodi,
			'proaktif'=>$proaktif
		));
		$this->load->view('admin/footer');
	}

	public function cetak($kode){
		$prodata = array();
		$prodi = $this->nilaiModel->prodi();
		$data['data'] = $this->nilaiModel->get($kode);
		foreach ($prodi as $key => $value) {
			$prodata[$value->KDPRODI] = $value->FAKPRODI.' / '.$value->NAMAPRODI;
		}
		$data['nama'] = $prodata[$kode];
		$this->load->view('admin/print/header');
		$this->load->view('admin/print/nilaiprodi',$data);
		$this->load->view('admin/print/footer');
	}

}

/* End of file Nilai.php */
/* Location: ./application/controllers/admin/statistika/Nilai.php */