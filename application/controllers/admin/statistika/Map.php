<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$this->load->model('admin/mapModel');
	}
	public function index()
	{
		$this->view();
	}

	public function view(){
		$this->mhistory->go();
		$key = $this->mfungsi->setting('api_key');
        $data['key'] = ($key==null) ? '' : '&key='.$key;
        $dataa = $this->mapModel->get();
        $datab = array();
        foreach ($dataa as $key => $value) {
        	if(!array_key_exists($value->KDKEL, $datab)){
        		$datab[$value->KDKEL] = new stdClass();
        		$datab[$value->KDKEL]->kode = $value->KDKP;
                $datab[$value->KDKEL]->dosen = $value->NAMADPL;
        		$datab[$value->KDKEL]->nama = $value->NAMAKEL;
        		$datab[$value->KDKEL]->alamat = $value->ALAMATKEL;
        		$datab[$value->KDKEL]->lat = floatval($value->KORDXKEL);
        		$datab[$value->KDKEL]->lng = floatval($value->KORDYKEL);
        		$datab[$value->KDKEL]->mahasiswa = array();
        	}
        	array_push($datab[$value->KDKEL]->mahasiswa, array(
        		'npm'=>$value->NPM,
        		'nama'=>$value->NAMAMHS,
        		'prodi'=>$value->FAKPRODI.' / '.$value->NAMAPRODI,
        		'alamat'=>$value->ALAMATMHS,
        		'lat'=>floatval($value->KORDXMHS),
        		'lng'=>floatval($value->KORDYMHS)
        	));
        }
        $data['data'] = $datab;
		$this->load->view('admin/header');
		$this->load->view('admin/map-kkn',$data);
		$this->load->view('admin/footer');
	}

    public function cari(){
        $dataa = $this->mapModel->get($_POST['cari']);
        $datab = array();
        foreach ($dataa as $key => $value) {
            if(!array_key_exists($value->KDKEL, $datab)){
                $datab[$value->KDKEL] = new stdClass();
                $datab[$value->KDKEL]->kode = $value->KDKP;
                $datab[$value->KDKEL]->dosen = $value->NAMADPL;
                $datab[$value->KDKEL]->nama = $value->NAMAKEL;
                $datab[$value->KDKEL]->alamat = $value->ALAMATKEL;
                $datab[$value->KDKEL]->lat = floatval($value->KORDXKEL);
                $datab[$value->KDKEL]->lng = floatval($value->KORDYKEL);
                $datab[$value->KDKEL]->mahasiswa = array();
            }
            array_push($datab[$value->KDKEL]->mahasiswa, array(
                'npm'=>$value->NPM,
                'nama'=>$value->NAMAMHS,
                'prodi'=>$value->FAKPRODI.' / '.$value->NAMAPRODI,
                'alamat'=>$value->ALAMATMHS,
                'lat'=>floatval($value->KORDXMHS),
                'lng'=>floatval($value->KORDYMHS)
            ));
        }
        echo json_encode($datab);
    }

}

/* End of file Map.php */
/* Location: ./application/controllers/admin/Map.php */