<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller {

	public function edit($npm)
	{
		$this->db->join('tahun','tahun.STATUSTAHUN = 1');
		$this->db->join('kelompok','kelompok.KDTAHUN = tahun.KDTAHUN');
		$this->db->join('kelompok_peserta','kelompok_peserta.KDKEL = kelompok.KDKEL AND kelompok_peserta.NPM = "'.$npm.'"');
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI');
		$this->db->where('mahasiswa.NPM',$npm);
		$mhs = $this->db->get('mahasiswa')->result()[0];
		$this->load->view('admin/header');
		$data['npm'] = $mhs->NPM;
		$data['nama'] = $mhs->NAMAMHS;
		$data['prodi'] = $mhs->NAMAPRODI;
		$data['fakultas'] = $mhs->FAKPRODI;
		$data['kkn'] = $mhs->NAMAKEL;
		$data['nilai'] = $mhs->NILAIMHS;
		$data['kembali'] = $this->mhistory->back();
		$this->load->view('admin/formNilai',$data);
		$this->load->view('admin/footer');
	}

	public function arsip(){
		$this->load->model('admin/mahasiswaModel');
		$this->mahasiswaModel->arsipkan();
		redirect('admin/statistika/nilai');
	}

	public function simpan($npm){
		$this->db->join('tahun','tahun.STATUSTAHUN = 1');
		$this->db->join('kelompok','kelompok.KDTAHUN = tahun.KDTAHUN');
		$this->db->join('kelompok_peserta','kelompok_peserta.KDKEL = kelompok.KDKEL AND kelompok_peserta.NPM = "'.$npm.'"');
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI');
		$this->db->where('mahasiswa.NPM',$npm);
		$mhs = $this->db->get('mahasiswa')->result()[0];

		$this->form_validation->set_rules('nilai', 's', 'trim|required|integer|regex_match[/^(?:100|\d{1,2})(?:\.\d{1,2})?$/]',array(
			'required'=>'Nilai harus diisi',
			'integer'=>'Format harus angka',
			'regex_match'=>'Nilai harus 0-100'
		));

		if ($this->form_validation->run() == FALSE)
    {
    	$this->load->view('admin/header');
			$data['npm'] = $mhs->NPM;
			$data['nama'] = $mhs->NAMAMHS;
			$data['prodi'] = $mhs->NAMAPRODI;
			$data['fakultas'] = $mhs->FAKPRODI;
			$data['kkn'] = $mhs->NAMAKEL;
			$data['nilai'] = $mhs->NILAIMHS;
			$data['kembali'] = $this->mhistory->back();
			$this->load->view('admin/formNilai',$data);
			$this->load->view('admin/footer');
    }else{
    	$this->db->set('NILAIMHS',$_POST['nilai']);
    	$this->db->where('KDKEL',$mhs->KDKEL);
    	$this->db->where('NPM',$mhs->NPM);
    	$this->db->update('kelompok_peserta');
    	redirect($this->mhistory->back());
    }
	}

}

/* End of file Nilai.php */
/* Location: ./application/controllers/admin/Nilai.php */