<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf extends CI_Controller {

	public function view($kel,$fil)
	{
		$this->load->model('admin/pdfModel');
		$file = $this->pdfModel->file($fil);
		$path1 = './dokumen';
		$path2 = $kel;
		$namafile = $file[0]->FILEUPL;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$ext = explode('.', $path);
		if(end($ext)=='pdf'){
			$this->mpdf->view($path);
		}else{
			$this->mio->download($path);
		}
	}

	public function download($kel,$fil){
		$this->load->model('admin/pdfModel');
		$file = $this->pdfModel->file($fil);
		$path1 = './dokumen';
		$path2 = $kel;
		$namafile = $file[0]->FILEUPL;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$this->mio->download($path);
	}

	public function monitoring($mode,$kel,$kode){
		$this->load->model('admin/pdfModel');
		$path1 = './monitoring';
		$path2 = $kel;
		$file = $this->pdfModel->file2($kode);
		$namafile = $file[0]->FILELM;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$ext = explode('.', $path);
		switch ($mode) {
			case 'view':
				if(end($ext)=='pdf'){
					$this->mpdf->view($path);
				}else{
					$this->mio->download($path);
				}
				break;
			case 'download':
					$this->mio->download($path);
				break;
		}
	}

}

/* End of file pdf.php */
/* Location: ./application/controllers/admin/pdf.php */