<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        if(count($_POST)==0){
            $this->mlogin->inLogin();
        }
    }

    public function index(){
        // konten
        if($this->session->time_lock_status==true){
            $now = new DateTime(date('Y-m-d H:i:s'));
            $now = $now->getTimestamp();
            $end = new DateTime($this->session->end_time_lock);
            $end = $end->getTimestamp();
            $st = $end-$now;
            if($st>0){
                $this->load->view('locked',array(
                    'detik'=>$st
                ));
            }else{
                $this->mlogin->remove_lock_system();
            }
        }else{
            $this->load->view('login',array(
                "aut"=>false,
                "msg"=>null
            ));
        }
    }

    public function masuk(){
        if($this->session->time_lock_status==true){
            $now = new DateTime(date('Y-m-d H:i:s'));
            $now = $now->getTimestamp();
            $end = new DateTime($this->session->end_time_lock);
            $end = $end->getTimestamp();
            $st = $end-$now;
            if($st>0){
                $this->load->view('locked',array(
                    'detik'=>$st
                ));
            }else{
                $this->mlogin->remove_lock_system();
            }
        }else{
            $this->mlogin->autentikasi(array(
                "username"=>$this->input->post('uname'),
                "password"=>$this->input->post('passwd'),
                "level"=>$this->input->post('level')
            ));
        }
    }


    public function sertifikat(){
        $this->load->model('sertifikatModel');
        $this->load->model('sertifikatBiro');
        $key = 'JEIBHCGDFA'; // jangan mengganti atau menghapu kode pada baris ini
        $this->load->model('encyDesc');
        $a = $this->encyDesc->decrypt($key,$_POST['kode']);
        $npm = $this->encyDesc->dec($a);
        $nmr = $this->sertifikatModel->nomor($npm);
        if($nmr->STATUS==false){
            $key = 'JEIBHCGDFA'; // jangan mengganti atau menghapu kode pada baris ini
            $dpl = $this->sertifikatModel->dpl_kkn();
            $a = $this->encyDesc->decrypt($key,$_POST['kode']);
            $kode = $this->encyDesc->dec($a);
            $dosen = null;
            foreach ($dpl as $key => $value) {
                if($value->KDDPL == $kode){
                    $dosen = $value;
                    break;
                }
            }
            if($dosen!=null){
                $key = 'JEIBHCGDFA'; // jangan mengganti atau menghapu kode pada baris ini
                $res = new stdClass();
                $res->nama = $dosen->NAMADPL;
                $res->induk = $dosen->KDDPL;
                $res->jenis = 'SERTIFIKAT DPL';
                $a = $this->encyDesc->enc($dosen->KDDPL);
                $a = $this->encyDesc->encrypt($key,$a);
                $b = $this->sertifikatModel->ambilalfabeta($dosen->NAMADPL);
                $res->kode = $this->encyDesc->encrypt($b,$a);
                echo json_encode(array('status'=>true,'data'=>$res));
            }else{
                $key = 'ZYWQVARDFKL';
                $dosen = null;
                $dplt = $this->sertifikatModel->dpl_kkn_terbaik();

                $a = $this->encyDesc->decrypt($key,$_POST['kode']);
                $kode = $this->encyDesc->dec($a);

                foreach ($dplt as $key => $value) {
                    if($value->KDDPL == $kode){
                        $dosen = $value;
                        break;
                    }
                }
                if($dosen==null){
                    $key = 'DCBAKJIHNPS';
                    $this->load->model('encyDesc');
                    $dosen = null;
                    $dplt = $this->sertifikatModel->kdpl_kkn_terbaik();
                    $a = $this->encyDesc->decrypt($key,$_POST['kode']);
                    $kode = $this->encyDesc->dec($a);
                    foreach ($dplt as $key => $value) {
                        if($value->KDDPL == $kode){
                            $dosen = $value;
                            break;
                        }
                    }
                    if($dosen==null){
                        $key = 'ORDIKUNEWA';
                        $this->load->model('encyDesc');
                        $a = $this->encyDesc->decrypt($key,$_POST['kode']);
                        $npm = $this->encyDesc->dec($a);
                        $mhs = $this->sertifikatBiro->mahasiswa_api($npm);
                        if($mhs->status==false){
                            echo json_encode(array('status'=>false,'data'=>'Kode sertifikat tidak ada yang cocok pada sistem'));
                            exit;
                        }
                        $this->load->model('encyDesc');
                        $res = new stdClass();
                        $res->nama = $mhs->data->attributes->nama;
                        $res->induk = $mhs->data->id;
                        $res->jenis = 'SERTIFIKAT ORDIK';
                        $a = $this->encyDesc->enc($mhs->data->id);
                        $a = $this->encyDesc->encrypt($key,$a);
                        $b = $this->sertifikatBiro->ambilalfabeta($mhs->data->attributes->nama);
                        $res->kode = $this->encyDesc->encrypt($b,$a);
                        echo json_encode(array('status'=>true,'data'=>$res));
                        exit;
                    }
                    $data[0] = $dosen;
                    $data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
                    $a = $this->encyDesc->enc($data[0]->KDDPL);
                    $key = 'DCBAKJIHNPS';
                    $data[0]->KODE = $this->encyDesc->encrypt($key,$a);
                    $b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMADPL);
                    $c = $this->encyDesc->encrypt($b,$data[0]->KODE);
                    $res = new stdClass();
                    $res->nama = $data[0]->NAMADPL;
                    $res->induk = $data[0]->KDDPL;
                    $res->jenis = 'SERTIFIKAT KDPL TERBAIK';
                    $res->kode = $c;
                    echo json_encode(array('status'=>true,'data'=>$res));
                    exit;                    
                }
                $key = 'ZYWQVARDFKL';
                $data[0] = $dosen;
                $data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
                $a = $this->encyDesc->enc($data[0]->KDDPL);
                $data[0]->KODE = $this->encyDesc->encrypt($key,$a);
                $b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMADPL);
                $c = $this->encyDesc->encrypt($b,$data[0]->KODE);
                $res = new stdClass();
                $res->nama = $data[0]->NAMADPL;
                $res->induk = $data[0]->KDDPL;
                $res->jenis = 'SERTIFIKAT DPL TERBAIK';
                $res->kode = $c;
                echo json_encode(array('status'=>true,'data'=>$res));
            }
            exit;
        }
        $data = $this->sertifikatModel->peserta($npm);
        if(count($data)>0){
            $value = $data[0];
            $res = new stdClass();
            $res->nama = $value->NAMAMHS;
            $res->induk = $value->NPM;
            $res->jenis = 'SERTIFIKAT KKN';
            $b = $this->sertifikatBiro->ambilalfabeta($value->NAMAMHS);
            $res->kode = $this->encyDesc->encrypt($b,$_POST['kode']);
            echo json_encode(array('status'=>true,'data'=>$res));
        }else{
            echo json_encode(array('status'=>false,'data'=>'Kode sertifikat tidak ada yang cocok pada sistems'));
        }
    }


}

/* End of file login.php */
/* Location: ./application/controllers/login.php */