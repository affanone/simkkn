<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_camat'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('camat/kknModel');
		$this->kknModel->aktif();
	}
	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->mhistory->go();
		$this->load->view('camat/header');
		$this->load->view('camat/beranda');
		$this->load->view('camat/footer');
	}

}

/* End of file beranda.php */
/* Location: ./application/controllers/camat/beranda.php */