<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bio extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_camat'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('camat/bioModel');
		$this->load->model('camat/kknModel');
		$this->kkn = $this->kknModel->get();
		$this->kknaktif = $this->kknModel->aktif();
	}

	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$data = $this->bioModel->get()->result();
		$this->load->view('camat/header');
		$this->load->view('camat/bio',array(
			'usern'=>$data[0]->USERN,
			'nama'=>$data[0]->NAMA,
			'pass'=>'',
			'rpass'=>''
		));
		$this->load->view('camat/footer');
	}

	public function update(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}

		$this->form_validation->set_rules('nama', 'ini', 'trim|required',array(
			'required'=>'Nama tidak boleh kosong',
		));
		if($_POST['pass']!=''){
			$this->form_validation->set_rules('pass', 'ini', 'trim|required');
			$this->form_validation->set_rules('rpass', 'ini', 'trim|required|matches[pass]',array('matches'=>'Ulangi password tidak sama'));
		}

		if ($this->form_validation->run() == FALSE)
    {
			$data = $this->bioModel->get()->result();
			$data['nama'] = set_value('nama');
			$this->load->view('camat/header');
			$this->load->view('camat/bio',array(
				'usern'=>$this->session->usern,
				'nama'=>set_value('nama'),
				'pass'=>set_value('pass'),
				'rpass'=>set_value('rpass')
			));
			$this->load->view('camat/footer');
    }
    else
    {
     	$this->bioModel->update($_POST);
      redirect('camat/bio');
    }

	}

}

/* End of file Bio.php */
/* Location: ./application/controllers/camat/Bio.php */