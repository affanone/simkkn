<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bio extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_biro'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('biro/bioModel');
		$this->load->model('biro/kknModel');
		$this->kkn = $this->kknModel->get();
		$this->kknaktif = $this->kknModel->aktif();
	}

	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$data = $this->bioModel->get()->result();
		$this->load->view('biro/header',array('kkn'=>$this->kkn,'kknaktif'=>$this->kknaktif));
		$this->load->view('biro/bio',array(
			'kontak'=>$data[0]->KONTAKDPL,
			'alamat'=>$data[0]->ALAMATDPL,
			'nama'=>$data[0]->NAMADPL,
			'data'=>$data
		));
		$this->load->view('biro/footer');
	}

	public function update(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->form_validation->set_rules('kontak', 'ini', 'trim|required',array(
			'required'=>'Kontak tidak boleh kosong',
		));

		$this->form_validation->set_rules('nama', 'ini', 'trim|required',array(
			'required'=>'Nama tidak boleh kosong',
		));

		$this->form_validation->set_rules('alamat', 'ini', 'trim|required',array(
			'required'=>'Alamat tidak boleh kosong'
		));

		if ($this->form_validation->run() == FALSE)
    {
			$data = $this->bioModel->get()->result();
			$data['kontak'] = set_value('kontak');
			$data['alamat'] = set_value('alamat');
			$data['nama'] = set_value('nama');
			$this->load->view('biro/header',array('kkn'=>$this->kkn,'kknaktif'=>$this->kknaktif));
			$this->load->view('biro/bio',array(
				'kontak'=>set_value('kontak'),
				'alamat'=>set_value('alamat'),
				'nama'=>set_value('nama'),
				'data'=>$data
			));
			$this->load->view('biro/footer');
    }
    else
    {
     	$this->bioModel->update($_POST);
      redirect('biro/bio');
    }

	}

}

/* End of file Bio.php */
/* Location: ./application/controllers/biro/Bio.php */