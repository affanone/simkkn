<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tunggu extends CI_Controller {

	public function index()
	{
		if($this->session->tunggu==true)
			if(intval($this->mfungsi->setting('edit_biodata_mhs'))==1)
				redirect('mhs/beranda','refresh');
			else
				$this->load->view('mhs/tunggu');	
		else	
			redirect('keluar');
	}

}

/* End of file tunggu.php */
/* Location: ./application/controllers/mhs/tunggu.php */