<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mhs/daftarModel');
		$this->mlogin->cek();
		$this->load->model('mhs/statusModel');
		if($this->session->mhs_baru_daftar!=true)
			$this->statusModel->cetStatus();
	}

	public function alamat(){
		//$_POST['alamat'] = 'pangarengan sampang jawa timur indonesia';
		// $this->load->model('admin/wilayahModel');
		// $csv = $this->wilayahModel->find($_POST['alamat'],'google');
		$data = $this->mfungsi->kordinat($_POST['alamat']);
		// if($data->status==true){
		// 	$x = $data->lat;
		// 	$y = $data->lng;
		// 	$this->daftarModel->setKordinat($x,$y);
		// }else{

		// }
		echo json_encode($data);
	}

	public function alamat2(){
		$this->load->model('admin/wilayahModel');
		$csv = $this->wilayahModel->find($_POST['alamat'],'google');
		$this->alamattext = $csv;
		if($csv->status==true){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function sksMax(){
		if(intval($_POST['sks'])>100){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function simpan(){
		$this->form_validation->set_rules('sks', 'sks', 'required|numeric');
		$this->form_validation->set_rules('telp', 'telp', 'required');
		$this->form_validation->set_rules('kordx', 'lat', 'required',array('required'=>'Lokasi atau alamat tidak benar'));
		$this->form_validation->set_rules('kordy', 'lng', 'required',array('required'=>'Lokasi atau alamat tidak benar'));
		$this->form_validation->set_rules('seragam', 'seragam', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		if ($this->form_validation->run() == FALSE)
    {
      $this->load->view('mhs/daftar',array(
				'alamat'=>set_value('alamat'),
				'telp'=>set_value('telp'),
				'sks'=>set_value('sks'),
				'kordx'=>set_value('kordx'),
				'kordy'=>set_value('kordy'),
				'seragam'=>set_value('seragam'),
				'kerja'=>set_value('kerja')
			));
    }
    else
    {
    	//$_POST['alamat'] = $this->alamattext->result->text; 
     	$this->daftarModel->simpan($_POST);
      redirect('mhs/beranda','refresh');
    }
	}
}

/* End of file daftar.php */
/* Location: ./application/controllers/mhs/daftar.php */