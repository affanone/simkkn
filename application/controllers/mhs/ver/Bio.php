<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bio extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_peserta'));
		if($cek==1){
			$this->kunci = true;
		}
		if($this->session->verified==false)
			redirect(base_url('keluar'));
		$this->load->model('mhs/v/bioModel');
	}

	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$n = $this->bioModel->get()[0];
		$this->load->view('mhs/v/header');
		$this->load->view('mhs/v/biodata',array(
			'telp'=>$n->KONTAKMHS,
			'kerja'=>$n->PEKERJAANMHS,
			'seragam'=>$n->SERAGAMMHS,
			'alamat'=>$n->ALAMATMHS,
			'sks'=>$n->SKSMHS
		));
		$this->load->view('mhs/v/footer');
	}

	public function update(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$cekdis = intval($this->mfungsi->setting('edit_biodata_mhs'));
		if($cekdis==0){
			redirect('mhs/ver/bio');
		}else{
			$this->form_validation->set_rules('sks', 'sks', 'required|numeric|callback_sksMax',array('sksMax'=>'Sks yang ditempuh kurang dari target'));
			$this->form_validation->set_rules('telp', 'telp', 'required');
			$this->form_validation->set_rules('seragam', 'seragam', 'required');
			if ($this->form_validation->run() == FALSE)
	    {
	    	$n = $this->bioModel->get()[0];
	    	$this->load->view('mhs/v/header');
	      $this->load->view('mhs/v/biodata',array(
					'telp'=>set_value('telp'),
					'kerja'=>set_value('kerja'),
					'seragam'=>set_value('seragam'),
					'alamat'=>$n->ALAMATMHS,
					'sks'=>set_value('sks')
				));
				$this->load->view('mhs/v/footer');
	    }
	    else
	    {
	     	$this->bioModel->update($_POST);
	      redirect('mhs/ver/bio');
	    }
	  }
	}

	public function sksMax(){
		if(intval($_POST['sks'])>100){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function foto($tipe){
		switch ($tipe) {
			case 'ganti':
				$cek = $this->mio->upload(array(
					'path'=>'assets/images/peserta',
					'format'=>'jpg',
					'filename'=>$this->session->usern,
					'name'=>'foto',
					'openerr'=>'',
					'closeerr'=>'',
					'size'=>5000,
					'width'=>5000,
					'height'=>5000,
					'overwrite'=>true
				));
				if($cek['status']==true){
					$foto = 'assets/images/peserta/'.$cek['data']['file_name'];
					$this->compress_gambar($foto,15);
					$this->session->foto = base_url($foto);
					$this->bioModel->updatefoto($foto);
					echo json_encode(array(
						'status'=>true,
						'foto'=>base_url($foto).'?'.rand(111,999)
					));
				}else{
					echo json_encode(array(
						'status'=>false,
						'foto'=>$cek['data']
					));
				}
				break;
			case 'default':
				//$ft = $this->bioModel->ambilfoto();
				$this->session->foto = $this->session->fotoorigin;
				$this->mfile->del_file('./assets/images/peserta/'.$this->session->usern.'.jpg');
				$this->bioModel->defaultfoto();
				echo json_encode(array(
					'status'=>true,
					'foto'=>$this->session->fotoorigin.'?'.rand(111,999)
				));
				break;
			default:
				# code...
				break;
		}
	}

	public function compress_gambar($source_url, $quality) {
    $info = getimagesize($source_url);
  
    if ($info['mime'] == 'image/jpeg') $gambar = imagecreatefromjpeg($source_url);
    elseif ($info['mime'] == 'image/gif') $gambar = imagecreatefromgif($source_url);
    elseif ($info['mime'] == 'image/png') $gambar = imagecreatefrompng($source_url);
  
    imagejpeg($gambar, $source_url, $quality);
    return $source_url;
	}

}

/* End of file Bio.php */
/* Location: ./application/controllers/mhs/ver/Bio.php */