<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_peserta'));
		if($cek==1){
			$this->kunci = true;
		}
		if($this->session->verified==false)
			redirect(base_url('keluar'));
		$this->periode = $this->mfungsi->periodeMHS($this->session->usern);
		$this->load->model('mhs/v/uploadModel');
	}

	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->find();
	}

	public function view($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$file = $this->uploadModel->file($kode);
		$kode = $this->uploadModel->kodekel();
		$path1 = './dokumen';
		$path2 = $kode[0]->KDKEL;
		$namafile = $file[0]->FILEUPL;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$ext = explode('.', $path);
		if(end($ext)=='pdf'){
			$this->mpdf->view($path);
		}else{
			$this->mio->download($path);
		}
	}

	public function download($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$file = $this->uploadModel->file($kode);
		$kode = $this->uploadModel->kodekel();
		$path1 = './dokumen';
		$path2 = $kode[0]->KDKEL;
		$namafile = $file[0]->FILEUPL;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$this->mio->download($path);
	}

	public function find(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$upload = intval($this->mfungsi->setting('halaman_uploadp'));
		$this->mhistory->go();
		$cari =  (isset($_POST['cari'])) ? $_POST['cari'] : '';
		$data = $this->uploadModel->view();
		$this->load->view('mhs/v/header');
		$this->load->view('mhs/v/upload',array(
			'cari'=>$cari,
			'data'=>$data,
			'periode'=>$this->periode,
			'upload'=>$upload
		));
		$this->load->view('mhs/v/footer');
	}

	public function tambah(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		if($this->session->tidak_lulus!=true && $this->session->mhs_lulus!=true){
			$this->uploadModel->view();
			$this->load->view('mhs/v/header');
			$this->load->view('mhs/v/upload-tambah',array(
				'cari'=>''
			));
			$this->load->view('mhs/v/footer');
		}
	}

	public function delete($kode){
		if($this->periode==true){
			if(intval($this->mfungsi->setting('halaman_uploadp'))==0){
				$file = $this->uploadModel->file($kode);
				$this->uploadModel->delete($kode);
				$cek = $this->uploadModel->cek($kode);
				if($cek==0){
					$kode = $this->uploadModel->kodekel();
					$path = './dokumen';
					$path1 = $kode[0]->KDKEL;
					$namafile = $file[0]->FILEUPL;
					$this->mfile->del_file($path.'/'.$path1.'/'.$namafile);
				}
			}
		}
		redirect($this->mhistory->back());
	}

	public function proses(){
		if($this->periode==true){

			$this->form_validation->set_rules('nama', 's', 'trim|required|max_length[15]|alpha_numeric_spaces',array(
				'required'=>'Nama file harus diisi',
				'max_length'=>'Nama file maksimal 15 karakter',
				'alpha_numeric_spaces'=>'Nama file harus huruf / angka / spasi'
			));

			if($this->form_validation->run() == FALSE){
				echo json_encode(array(
					'data'=>'<td colspan="8">'.form_error('nama','<p class="text-danger text-center"><i>','</i></p>').'</td>',
					'status'=>false
				));
			}else{
				if(isset($_FILES['file'])){
					$kode = $this->uploadModel->kodekel();
					$path1 = 'dokumen';
					$path2 = $kode[0]->KDKEL;
					$fullpath = "./$path1/$path2";
					$tgl = $this->mfungsi->timeNow();
					$tglnotime = explode(' ', $tgl);
					$ukuran_ori = $_FILES['file']['size'];
					$has_ori = hash_file('md5', $_FILES['file']['tmp_name']);
					$ext = explode('.',  $_FILES['file']['name']);
	    		$ext = end($ext);

	    		$valid = $this->uploadModel->validasiUpload(array(
	    			'hash'=>$has_ori,
	    			'tanggal'=>$tglnotime[0],
	    			'file'=>$_POST['nama'].'.'.$ext,
	    			'kelompok'=>$kode[0]->KDKEL
	    		));

	    		if($valid['status']==false){
	    			echo json_encode(array(
							'data'=>'<td colspan="8"><p class="text-danger text-center"><i>'.$valid['pesan'].'</i></p></td>',
							'status'=>false
						));
	    		}else{
	    			if (!is_dir("$path1/$path2")) {
						 	mkdir($fullpath, 0777, TRUE);
						}	
						$cek = $this->mio->upload(array(
							'name'=>'file',
							'path'=>$fullpath,
							'format'=>'pdf',
							'size'=>5000,
							'openerr'=>'<p class="text-danger text-center"><i>',
							'closeerr'=>'</i></p>',
							'filename'=>$_POST['nama'].'.'.$ext
						));
						$data = '';
						$status = false;
						if($cek['status']==false){
							$data = '<td colspan="8">'.$cek['data'].'</td>';
						}else{
							$id = $this->uploadModel->maxId();
							$direktori = FCPATH.'./'.$path1.'/'.$path2.'/'.$cek['data']['file_name'];
							$has = hash_file('md5', $direktori);
							$this->uploadModel->simpan(array(
								'file'=>$_POST['nama'].$cek['data']['file_ext'],
								'ukuran'=>$ukuran_ori,
								'tgl'=>$tgl,
								'npm'=>$this->session->usern,
								'id'=>$id,
								'ajukan'=>0,
								'jenis'=>$_POST['jenis'],
								'deskripsi'=>$_POST['deskripsi'],
								'hash'=>$has
							));
							$status = true;
							$data = '<tr><td width="60px"><img src="'.base_url('assets/images/icons/'.$ext.'.png').'" class="img-fluid rounded"></td>
				    			<td><a '.(($ext=='pdf')? 'onclick="_cetak(this)"' : '' ).' href="'.base_url('mhs/ver/upload/view/').$id.'" class="btn-link" title="'.$_POST['nama'].$cek['data']['file_ext'].'">'.$this->mfungsi->minimalText($_POST['nama'].$cek['data']['file_ext'],15).'</a>&nbsp;.&nbsp;<a href="'.base_url('mhs/ver/upload/download/').$id.'">Download</a></td>
				    			<td><span title="'.$this->session->usern.'">'.$this->session->nama.'</span></td>
				    			<td>'.$this->mfungsi->koversiSize($ukuran_ori).'</td>
				    			<td>'.$this->mfungsi->tgl($tgl,true).'</td>
				    			<td>'.ucwords($_POST['jenis']).'</td>
				    			<td>'.$_POST['deskripsi'].'</td>
				    			<td><span class="badge badge-pill badge-info"><i>Evaluasi DPL</i></span></td>
				    			<td><a href="'.base_url('mhs/ver/upload/delete/').$id.'" onclick="return hapus(this);" class="btn-link text-danger"><i class="fa fa-trash"></i></a></td>
				    			</tr>';

						}
						echo json_encode(array(
							'data'=>$data,
							'status'=>$status
						));
	    		}
				}else{
					echo json_encode(array(
						'data'=>'<td colspan="8"><p class="text-danger text-center"><i>File tidak dipilih</i></p></td>',
						'status'=>false
					));
				}
			}

			
		}else{
			redirect($this->mhistory->back());
		}
	}

}

/* End of file upload.php */
/* Location: ./application/controllers/mhs/ver/upload.php */