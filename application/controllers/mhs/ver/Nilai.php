<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_peserta'));
		if($cek==1){
			$this->kunci = true;
		}
		if($this->session->verified==false)
			redirect(base_url('keluar'));
		$this->load->model('mhs/v/nilaiModel');
	}

	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$n = $this->nilaiModel->get();
		$this->load->view('mhs/v/header');
		$this->load->view('mhs/v/nilai',array(
			'nilai'=>($n->NILAIMHS==null) ? 0 : $n->NILAIMHS,
			'status'=>$n->STATUSMHS
		));
		$this->load->view('mhs/v/footer');
	}

	public function sertifikat($n=null){
		if($n=='topdf'){
			$array = array(
				'sertifikat_data' => $_POST['img']
			);
			$this->session->set_userdata( $array );
		}else if($n=='pdf'){
			$this->load->library('pdf');
	    $this->pdf->setPaper('A4', 'landscape');
	    $this->pdf->filename = $this->session->usern.".pdf";
	    $this->pdf->load_view('sertifikat');
		}else if($n!='cetak'){
			$this->load->view('mhs/v/header');
			$this->load->view('mhs/v/sertifikat');
			$this->load->view('mhs/v/footer');
		}else{
			$this->load->model('sertifikatModel');
			$npm = $this->session->usern;
			$nmr = $this->sertifikatModel->nomor($npm);
			if($nmr->STATUS==false){
				echo $nmr->PESAN;
				exit;
			}
			$data = $this->sertifikatModel->peserta($npm);
			if(count($data)>0){
				$key = 'JEIBHCGDFA';
				$this->load->model('encyDesc');
				$data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
				$a = $this->encyDesc->enc($data[0]->NPM);
				$data[0]->KODE = $this->encyDesc->encrypt($key,$a);
				$b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMAMHS);
				$data[0]->KUNCIKODE = $this->encyDesc->encrypt($b,$data[0]->KODE);
				$x = str_replace(array('%nomor%','%tahun%'), array($nmr->NOMOR,$nmr->TAHUN), $this->mfungsi->setting('nosertifikat'));
				$data[0]->NOMOR = 'No. '.$x;
				$data[0]->AKADEMIK = $nmr->AKADEMIK;
				$x = explode('|', $this->mfungsi->setting('pimpinan_lppm'));
				$data[0]->PIMPINAN = (isset($x[0]))?$x[0]:'';
				$data[0]->NOMORPIMPINAN = (isset($x[1]))?$x[1]:'';
				$data[0]->TGLREGMHS = $this->mfungsi->tgl($data[0]->TGLREGMHS,true);
				$this->load->view('mhs/v/sertifikat-cetak',array('data'=>$data));
			}
			else{
				echo 'Anggota tidak ditemukan';
			}
		}
	}

	public function ajax_sertifikat(){
		$this->load->model('sertifikatModel');
		$npm = $this->session->usern;
		$nmr = $this->sertifikatModel->nomor($npm);
		if($nmr->STATUS==false){
			echo json_encode(array('status'=>false,'data'=>$nmr->PESAN));
			exit;
		}
		$data = $this->sertifikatModel->peserta($npm);
		if(count($data)>0){
			$key = 'JEIBHCGDFA';
			$this->load->model('encyDesc');
			$data[0]->TANGGAL = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
			$a = $this->encyDesc->enc($data[0]->NPM);
			$data[0]->KODE = $this->encyDesc->encrypt($key,$a);
			$b = $this->sertifikatModel->ambilalfabeta($data[0]->NAMAMHS);
			$data[0]->KUNCIKODE = $this->encyDesc->encrypt($b,$data[0]->KODE);
			$x = str_replace(array('%nomor%','%tahun%'), array($nmr->NOMOR,$nmr->TAHUN), $this->mfungsi->setting('nosertifikat'));
			$data[0]->NOMOR = 'No. '.$x;
			$data[0]->AKADEMIK = $nmr->AKADEMIK;
			$x = explode('|', $this->mfungsi->setting('pimpinan_lppm'));
			$data[0]->PIMPINAN = (isset($x[0]))?$x[0]:'';
			$data[0]->NOMORPIMPINAN = (isset($x[1]))?$x[1]:'';
			$data[0]->TGLREGMHS = $this->mfungsi->tgl($data[0]->TGLREGMHS,true);
			echo json_encode(array('status'=>true,'data'=>$data));
		}
		else{
			echo json_encode(array('status'=>false,'data'=>'Anggota tidak ditemukan'));
		}
	}

}

/* End of file nilai.php */
/* Location: ./application/controllers/mhs/ver/nilai.php */