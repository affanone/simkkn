<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$this->load->model('mhs/statusModel');
		if($this->session->mhs_baru_daftar!=true)
			$this->statusModel->cetStatus();
	}
	public function biodata()
	{
		$this->load->model('mhs/biodataModel');
		$data = $this->biodataModel->mahasiswa();
		$this->load->view('mhs/print/header');
		$this->load->view('mhs/print/biodata',array(
			'alamat'=>$data[0]->ALAMATMHS,
			'telp'=>$data[0]->KONTAKMHS,
			'kerja'=>$data[0]->PEKERJAANMHS,
			'sks'=>$data[0]->SKSMHS,
			'seragam'=>$data[0]->SERAGAMMHS,
			'prodi'=>$data[0]->FAKPRODI.' / '.$data[0]->NAMAPRODI,
			'npm'=>$data[0]->NPM,
			'nama'=>$data[0]->NAMAMHS
		));
		$this->load->view('mhs/print/footer');
	}

}

/* End of file cetak.php */
/* Location: ./application/controllers/mhs/cetak.php */