<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mhs/berandaModel');
		$this->mlogin->cek();
		$this->load->model('mhs/statusModel');
		if($this->session->mhs_baru_daftar!=true)
			$this->statusModel->cetStatus();
	}
	public function index()
	{
			$cek = $this->berandaModel->mahasiswa();
			if(count($cek)==0){
				$cek = $this->berandaModel->mahasiswa_diterima();
				if($cek>0){	
					if($this->session->enable_edit_biodata==true)
						$this->view();
					else
						redirect('mhs/tunggu','refresh');	
				}else{
					$this->daftar();
				}
			}else{
				$this->view();
			}
	}

	public function daftar(){
		$cek = $this->berandaModel->ordik();
		if($cek>0){
			$psn = $this->mfungsi->setting('biro_pesan');
			$this->load->view('mhs/tidak-ordik',array('pesan'=>$psn));
		}else{
			$this->load->view('mhs/daftar',array(
				'kordx'=>'',
				'kordy'=>'',
				'alamat'=>$this->session->alamat,
				'telp'=>$this->session->telepon,
				'kerja'=>'-',
				'sks'=>'0',
				'seragam'=>''
			));
		}
	}

	public function view(){
		$this->load->view('mhs/header');
		$this->load->view('mhs/beranda');
		$this->load->view('mhs/footer');
	}


}

/* End of file beranda.php */
/* Location: ./application/controllers/mhs/beranda.php */