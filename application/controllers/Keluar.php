<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keluar extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->mlogin->logout();
	}

}

/* End of file keluar.php */
/* Location: ./application/controllers/keluar.php */