<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_dpl'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('dosen/kknModel');
		$this->kkn = $this->kknModel->get();
		$this->kknaktif = $this->kknModel->aktif();
		$this->load->model('dosen/uploadModel');
	}
	public function index()
	{
		$this->find();
	}

	public function find(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->mhistory->go();
		$cekupl = intval($this->mfungsi->setting('halaman_uploadd'));
		$cekarsip = $this->uploadModel->cek_arsip();
		$data = $this->uploadModel->view();
		$monitor = $this->uploadModel->lmonit();
		$this->load->view('dosen/header',array('kkn'=>$this->kkn,'kknaktif'=>$this->kknaktif));
		$this->load->view('dosen/upload',array(
			'data'=>$data,
			'arsip'=>$cekupl,
			'cari'=>$cari =  (isset($_POST['cari'])) ? $_POST['cari'] : '',
			'monitoring'=>$monitor
		));
		$this->load->view('dosen/footer');
	}

	public function view($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$file = $this->uploadModel->file($kode);
		$path1 = './dokumen';
		$path2 = $this->kknaktif;
		$namafile = $file[0]->FILEUPL;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$ext = explode('.', $path);
		if(end($ext)=='pdf'){
			$this->mpdf->view($path);
		}else{
			$this->mio->download($path);
		}
	}

	public function vmonit($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$file = $this->uploadModel->file2($kode);
		$kode = $this->uploadModel->kodekel();
		$path1 = './monitoring';
		$path2 = $this->kknaktif;
		$namafile = $file[0]->FILELM;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$ext = explode('.', $path);
		if(end($ext)=='pdf'){
			$this->mpdf->view($path);
		}else{
			$this->mio->download($path);
		}
	}

	public function download($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$file = $this->uploadModel->file($kode);
		$path1 = './dokumen';
		$path2 = $this->kknaktif;
		$namafile = $file[0]->FILEUPL;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$this->mio->download($path);
	}

	public function vdownload($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$file = $this->uploadModel->file2($kode);
		$path1 = './monitoring';
		$path2 = $this->kknaktif;
		$namafile = $file[0]->FILELM;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$this->mio->download($path);
	}

	public function ajukan($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->uploadModel->ajukan($kode);
		$info = $this->uploadModel->infofile($kode);
		$id = $this->mfungsi->idTime();
		$this->mnotif->tambah(array(
			'id'=>$id,
			'keterangan'=>'Kelompok '.$info->NAMAKEL.' mengajukan file "'.$info->FILEUPL.'"',
			'url'=>'admin/statistika/kinerja/detail/'.$info->KDKEL.'?notif='.$id.'#ajuan',
			'tgl'=>$this->mfungsi->timeNow()
		));
		redirect($this->mhistory->back());
	}

	public function batalajukan($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->uploadModel->batalajukan($kode);
		redirect($this->mhistory->back());
	}

	public function batalajukanulang($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->uploadModel->batalajukanulang($kode);
		redirect($this->mhistory->back());
	}

	public function lembar(){
		$cek = intval($this->mfungsi->setting('halaman_uploadd'));
		if($cek==1){
			echo json_encode(array(
				'data'=>'<td colspan="5"><p class="text-danger text-center"><i>Uploda lembar monitoring tidak dapat dilakukan! silahkan untuk menghubungi LPPM</i></p></td>',
				'status'=>false
			));
			exit;
		}
		if(isset($_FILES['file'])){
			//$kode = $this->uploadModel->kodekel();
			$path1 = 'monitoring';
			$path2 = $this->kknaktif;
			$fullpath = "./$path1/$path2";
			$ukuran_ori = $_FILES['file']['size'];
			if (!is_dir("$path1/$path2")) {
			    mkdir($fullpath, 0777, TRUE);
			}	
			$cek = $this->mio->upload(array(
				'name'=>'file',
				'path'=>$fullpath,
				'format'=>'pdf',
				'size'=>5000,
				'openerr'=>'<p class="text-danger text-center"><i>',
				'closeerr'=>'</i></p>',
				'filename'=>$_FILES['file']['name']
			));
			$data = '';
			$status = false;
			if($cek['status']==false){
				$data = '<td colspan="5">'.$cek['data'].'</td>';
			}else{
				$id = $this->uploadModel->maxId();
				$tgl = $this->mfungsi->timeNow();
				$direktori = FCPATH.'./'.$path1.'/'.$path2.'/'.$cek['data']['file_name'];
				$has = hash_file('md5', $direktori);
				$this->uploadModel->simpan(array(
					'file'=>$cek['data']['file_name'],
					'ukuran'=>$ukuran_ori,
					'tgl'=>$tgl,
					'kel'=>$this->kknaktif,
					'id'=>$id,
					'hash'=>$has
				));
				$info = $this->uploadModel->infofile2($id);
				$idNotif = $this->mfungsi->idTime();
				$this->mnotif->tambah(array(
					'id'=>$idNotif,
					'keterangan'=>'Kelompok '.$info->NAMAKEL.' mengunggah file lembar monitoring "'.$info->FILELM.'"',
					'url'=>'admin/statistika/kinerja/detail/'.$this->session->dpl_kkn_aktif.'?notif='.$idNotif.'#monitor',
					'tgl'=>$this->mfungsi->timeNow()
				));
				$status = true;
				$ext = explode('.', $cek['data']['file_name']);
    		$ext = end($ext);
				$data = '<tr><td width="60px"><img src="'.base_url('assets/images/icons/'.$ext.'.png').'" class="img-fluid rounded"></td>
	    			<td><a '.(($ext=='pdf')? 'onclick="_cetak(this)"' : '' ).' href="'.base_url('dosen/upload/vmonit/').$id.'" class="btn-link" title="'.$cek['data']['file_name'].'">'.$this->mfungsi->minimalText($cek['data']['file_name'],15).'</a>&nbsp;.&nbsp;<a href="'.base_url('dosen/upload/vdownload/').$id.'">Download</a></td>
	    			<td>'.$cek['data']['file_size'].'Kb</td>
	    			<td>'.$this->mfungsi->tgl($tgl,true).'</td>
	    			<td><a href="'.base_url('dosen/upload/delete/').$id.'" onclick="return hapus(this);" class="btn-link text-danger">Hapus</a></td>
	    			</tr>';

			}
			echo json_encode(array(
				'data'=>$data,
				'status'=>$status
			));
		}else{
			echo json_encode(array(
				'data'=>'<td colspan="5"><p class="text-danger text-center"><i>File tidak dipilih</i></p></td>',
				'status'=>false
			));
		}
	}

	public function delete($kode){
		$file = $this->uploadModel->file2($kode);
		$this->uploadModel->delete($kode);
		$cek = $this->uploadModel->cek2($kode);
		if($cek==0){
			$kode = $this->uploadModel->kodekel();
			$path = './monitoring';
			$path1 = $this->kknaktif;
			$namafile = $file[0]->FILELM;
			$this->mfile->del_file($path.'/'.$path1.'/'.$namafile);
		}
		redirect('dosen/upload#lembar-monitoring');
	}

}

/* End of file upload.php */
/* Location: ./application/controllers/dosen/upload.php */