<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bio extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_dpl'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('dosen/bioModel');
		$this->load->model('dosen/kknModel');
		$this->kkn = $this->kknModel->get();
		$this->kknaktif = $this->kknModel->aktif();
	}

	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$data = $this->bioModel->get()->result();
		$this->load->view('dosen/header',array('kkn'=>$this->kkn,'kknaktif'=>$this->kknaktif));
		$this->load->view('dosen/bio',array(
			'kontak'=>$data[0]->KONTAKDPL,
			'alamat'=>$data[0]->ALAMATDPL,
			'nama'=>$data[0]->NAMADPL,
			'data'=>$data
		));
		$this->load->view('dosen/footer');
	}

	public function update(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->form_validation->set_rules('kontak', 'ini', 'trim|required',array(
			'required'=>'Kontak tidak boleh kosong',
		));

		$this->form_validation->set_rules('nama', 'ini', 'trim|required',array(
			'required'=>'Nama tidak boleh kosong',
		));

		$this->form_validation->set_rules('alamat', 'ini', 'trim|required',array(
			'required'=>'Alamat tidak boleh kosong'
		));

		if ($this->form_validation->run() == FALSE)
    {
			$data = $this->bioModel->get()->result();
			$data['kontak'] = set_value('kontak');
			$data['alamat'] = set_value('alamat');
			$data['nama'] = set_value('nama');
			$this->load->view('dosen/header',array('kkn'=>$this->kkn,'kknaktif'=>$this->kknaktif));
			$this->load->view('dosen/bio',array(
				'kontak'=>set_value('kontak'),
				'alamat'=>set_value('alamat'),
				'nama'=>set_value('nama'),
				'data'=>$data
			));
			$this->load->view('dosen/footer');
    }
    else
    {
     	$this->bioModel->update($_POST);
      redirect('dosen/bio');
    }

	}

	public function foto($tipe){
		switch ($tipe) {
			case 'ganti':
				$cek = $this->mio->upload(array(
					'path'=>'assets/images/dosen',
					'format'=>'jpg',
					'filename'=>$this->session->usern,
					'name'=>'foto',
					'openerr'=>'',
					'closeerr'=>'',
					'size'=>5000,
					'width'=>5000,
					'height'=>5000,
					'overwrite'=>true
				));
				if($cek['status']==true){
					$foto = 'assets/images/dosen/'.$cek['data']['file_name'];
					$this->compress_gambar($foto,15);
					$this->session->foto = base_url($foto);
					$this->bioModel->updatefoto($foto);
					echo json_encode(array(
						'status'=>true,
						'foto'=>base_url($foto).'?'.rand(111,999)
					));
				}else{
					echo json_encode(array(
						'status'=>false,
						'foto'=>$cek['data']
					));
				}
				break;
			case 'default':
				//$ft = $this->bioModel->ambilfoto();
				$this->session->foto = $this->session->fotoorigin;
				$this->mfile->del_file('./assets/images/dosen/'.$this->session->usern.'.jpg');
				$this->bioModel->defaultfoto();
				echo json_encode(array(
					'status'=>true,
					'foto'=>$this->session->fotoorigin.'?'.rand(111,999)
				));
				break;
			default:
				# code...
				break;
		}
	}

	public function compress_gambar($source_url, $quality) {
    $info = getimagesize($source_url);
  
    if ($info['mime'] == 'image/jpeg') $gambar = imagecreatefromjpeg($source_url);
    elseif ($info['mime'] == 'image/gif') $gambar = imagecreatefromgif($source_url);
    elseif ($info['mime'] == 'image/png') $gambar = imagecreatefrompng($source_url);
  
    imagejpeg($gambar, $source_url, $quality);
    return $source_url;
	}

}

/* End of file Bio.php */
/* Location: ./application/controllers/dosen/Bio.php */