<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Set extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_dpl'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('dosen/kknModel');
	}

	public function kkn($kode)
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->kknModel->set($kode);
		redirect($this->mhistory->back());
	}

}

/* End of file setkkn.php */
/* Location: ./application/controllers/dosen/setkkn.php */