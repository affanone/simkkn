<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_dpl'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('dosen/kknModel');
		$this->kkn = $this->kknModel->get();
		$this->kknaktif = $this->kknModel->aktif();
	}
	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		//echo $this->mfungsi->viewJson($_SESSION);
		$this->load->view('dosen/header',array('kkn'=>$this->kkn,'kknaktif'=>$this->kknaktif));
		$this->load->view('dosen/beranda');
		$this->load->view('dosen/footer');
	}

}

/* End of file beranda.php */
/* Location: ./application/controllers/dosen/beranda.php */